using System.Collections.Generic;
using AppDocument.SystemSettings;
using DALDocument.EF.Context;
using LayerTransmitter.Wrapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NUnit.Framework;
using Org.BouncyCastle.Utilities;
using PLDocument;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using AppDocument.General;

namespace TestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task Test1Async()
        {
            // Arrange
            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            //var personDB = dbContext.People.FirstOrDefault(x =>
            //    x.firstName == "Jon" && x.lastName == "Doe" && x.userName == "Jon_user");

            // Act

            HttpResponseMessage response = await httpClient.GetAsync("api/s_DocumentTemplateController/GetAllTemplate");

            // Assert

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var contentResp = await response.Content.ReadAsStringAsync();
            var docs = JsonConvert.DeserializeObject<DtoWrapper<List<s_DocumentTemplateDto>>>(contentResp);
            ;
            //docs.Item.Count > 0;
        }
    }
}