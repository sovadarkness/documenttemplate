﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALDocument.Abstraction
{
    public interface IEntityWithIdParent<TKeyType, TNullableKeyType>: IEntityWithId<TKeyType>
    {
        TNullableKeyType idParent { get; set; }
    }
}
