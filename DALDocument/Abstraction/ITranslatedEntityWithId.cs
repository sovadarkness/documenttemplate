﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALDocument.Abstraction
{
    public interface ITranslatedEntityWithId<KeyType>: IEntityWithId<KeyType>
    {
        int idLanguage { get; set; }
    }
}
