﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALDocument.Abstraction
{
    public interface ILogEntityWithId<KeyType> : IEntityWithId<KeyType>
    {
        DateTime? lastUpdatedTime { get; set; }
        string lastUpdatedUser { get; set; }
    }
}
