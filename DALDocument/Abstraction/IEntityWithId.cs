﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALDocument.Abstraction
{
    public interface IEntityWithId<KeyType>
    {
        KeyType id { get; set; }
    }
}
