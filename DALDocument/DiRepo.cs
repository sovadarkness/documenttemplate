﻿using AppDocument;
using AppDocument.DocTemplate;
using AppDocument.General;
using AppDocument.SystemSettings;
using DALDocument.DocTemplate;
using DALDocument.General;
using DALDocument.Logic.System;
using DALDocument.Logic.System.CommonSettings;
//using AppDocument.DictionaryTranslation.DictionaryTranslationCrudService;
using DALDocument.SystemSettings;
using Microsoft.Extensions.DependencyInjection;

//using AppDocument.SystemSettings;
//using DALDocument.SystemSettings;

namespace DALDocument
{
    public static class DiRepo
    {
        public static void AddRepos(IServiceCollection services)
        {
            services.AddScoped<ICommonSettingsRepository, CommonSettingsRepository>();
            services.AddScoped<ISystemSettingRepository, SystemSettingRepository>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<Is_DocumentTemplateTranslationRepository, s_DocumentTemplateTranslationRepository>();
            services.AddScoped<Is_QueriesDocumentTemplateRepository, s_QueriesDocumentTemplateRepository>();
            services.AddScoped<Is_TemplateDocumentPlaceholderRepository, s_TemplateDocumentPlaceholderRepository>();
            services.AddScoped<Is_DocumentTemplateRepository, s_DocumentTemplateRepository>();
            services.AddScoped<Is_PlaceHolderTemplateRepository, s_PlaceHolderTemplateRepository>();
            services.AddScoped<Is_PlaceHolderGroupsMtmRepository, s_PlaceHolderGroupsMtmRepository>();
            services.AddScoped<Is_PlaceholderGroupRepository, s_PlaceholderGroupRepository>();
            services.AddScoped<IroDocumentTemplateCategoryRepository, roDocumentTemplateCategoryRepository>();
            services.AddScoped<ICustomSvgIconRepository, CustomSvgIconRepository>();
            services.AddScoped<Is_QueryRepository, s_QueryRepository>();
            services.AddScoped<ISystemSettingProvider, SystemSettingProvider>();
        }
    }
}
