﻿using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using AppDocument.SystemSettings;
using System;
using System.Linq.Expressions;
using System.Linq;
using DALDocument.ExceptionConcerter;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using DALDocument.EF.Entities;

namespace DALDocument.Logic.System
{
    public class SystemSettingRepository : BaseEfCrudRepository<
            SystemSettingDto,
            int,
            SystemSetting
        >, ISystemSettingRepository
    {
        protected override Func<SystemSettingDto, SystemSetting> entitySelector => (x) => x.ToSystemSetting();
        protected override Expression<Func<SystemSettingDto, SystemSetting>> entitySelectorExpr => (x) => x.ToSystemSetting();
        protected override Func<SystemSetting, SystemSettingDto> dtoSelector => (x) => x.ToSystemSettingDto();
        protected override Expression<Func<SystemSetting, SystemSettingDto>> dtoSelectorExpr => (x) => x.ToSystemSettingDto();

        protected override Expression<Func<SystemSetting, bool>> filterSameItemsExpr(SystemSettingDto dto)
        {
            return x => x.ro_code != null && x.ro_code == dto.Code || x.ro_name == dto.Name;
        }

        private string clinicLogoCode = "ClinicLogo";
        private string BonusDoctorProcent = "BonusDoctorProcent";
        private string MainDoctorProcent = "MainDoctorProcent";

        public void SetLogo(string filePath)
        {
            var clinicLogo = TheWholeEntities.FirstOrDefault(x => x.ro_code == clinicLogoCode);
            if (clinicLogo == null)
            {
                base.Add(new SystemSettingDto() { Code = clinicLogoCode, Name = "Clinic Logo", Value = filePath });
            }
            else
            {
                base.Update(new SystemSettingDto() { id = clinicLogo.id, Code = clinicLogoCode, Name = "Clinic Logo", Value = filePath });
            }
        }

        public void DeleteLogo()
        {
            var logo = TheWholeEntities.FirstOrDefault(x => x.ro_code == clinicLogoCode);
            if (logo != null)
            {
                Remove(logo.id);
            }
        }

        public string GetLogo()
        {
            var logo = TheWholeEntities.FirstOrDefault(x => x.ro_code == clinicLogoCode);
            if (logo != null)
            {
                return logo.value;
            }
            return string.Empty;
        }

        public int GetBonusProcent()
        {
            var logo = TheWholeEntities.FirstOrDefault(x => x.ro_code == BonusDoctorProcent);
            if (logo != null)
            {
                int b = 0;
                int.TryParse(logo.value, out b);
                return b;
            }
            return 0;
        }

        public int GetMainProcent()
        {
            var logo = TheWholeEntities.FirstOrDefault(x => x.ro_code == MainDoctorProcent);
            if (logo != null)
            {
                int b = 0;
                int.TryParse(logo.value, out b);
                return b;
            }
            return 0;
        }

        protected override IQueryable<SystemSetting> TheWholeEntities =>
               context.SystemSettings.AsNoTracking();

        public SystemSettingRepository(ApplicationContext context)
            : base(context)
        {
        }

    }

}

