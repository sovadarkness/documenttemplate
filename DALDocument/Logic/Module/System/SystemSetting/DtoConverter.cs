﻿using AppDocument.SystemSettings;
using DALDocument.EF.Entities;

namespace DALDocument.Logic.System
{
    static partial class SystemSettingDtoConverter
    {
        public static SystemSettingDto ToSystemSettingDto(this SystemSetting SystemSetting)
        {
            var result = new SystemSettingDto
            {
                id = SystemSetting.id,
                Name = SystemSetting.ro_name,
                Code = SystemSetting.ro_code,
                Description = SystemSetting.ro_description,
                QueueNumber = SystemSetting.ro_queueNumber ?? -1,
                Value = SystemSetting.value
            };
            return result;
        }
        public static SystemSetting ToSystemSetting(this SystemSettingDto dto)
        {
            var result = new SystemSetting
            {
                id = dto.id,
                ro_name = dto.Name,
                ro_code = dto.Code,
                ro_description = dto.Description,
                ro_queueNumber = dto.QueueNumber,
                value = dto.Value
            };
            return result;
        }
    }
}
