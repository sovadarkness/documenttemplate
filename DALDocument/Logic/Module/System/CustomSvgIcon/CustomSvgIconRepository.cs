using AppDocument.General;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.Logic.System
{
    public class CustomSvgIconRepository : BaseEfCrudRepository<CustomSvgIconDto, int, EF.Entities.CustomSvgIcon>, ICustomSvgIconRepository
    {
        public CustomSvgIconRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<CustomSvgIconDto, CustomSvgIcon> entitySelector => (x) => x.ToCustomSvgIcon();

        protected override Expression<Func<CustomSvgIconDto, CustomSvgIcon>> entitySelectorExpr => (x) => x.ToCustomSvgIcon();

        protected override Func<CustomSvgIcon, CustomSvgIconDto> dtoSelector => (x) => x.ToCustomSvgIconDto();

        protected override Expression<Func<CustomSvgIcon, CustomSvgIconDto>> dtoSelectorExpr => (x) => x.ToCustomSvgIconDto();

        protected override Expression<Func<CustomSvgIcon, bool>> filterSameItemsExpr(CustomSvgIconDto dto)
        {
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
            return x => false;
        }

        public List<CustomSvgIconDto> GetByTableName(string tableName)
        {
            var wrappedTableName = "," + tableName + ",";
            return GetByFilter(x => x.usedTables.Contains(wrappedTableName));
        }

        protected override IQueryable<CustomSvgIcon> TheWholeEntities => context.CustomSvgIcons
            .AsNoTracking()
        ;



    }
}
