using AppDocument.General;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.Logic.System
{
    static partial class CustomSvgIconDtoConverter
    {
        public static CustomSvgIconDto ToCustomSvgIconDto(this CustomSvgIcon _CustomSvgIcon)
        {
            var result = new CustomSvgIconDto
            {
                id = _CustomSvgIcon.id,
                name = _CustomSvgIcon.name,
                svgPath = _CustomSvgIcon.svgPath,
                usedTables = _CustomSvgIcon.usedTables,


            };
            return result;
        }
        public static CustomSvgIcon ToCustomSvgIcon(this CustomSvgIconDto dto)
        {
            var result = new CustomSvgIcon
            {
                id = dto.id,
                name = dto.name,
                svgPath = dto.svgPath,
                usedTables = dto.usedTables
            };
            return result;
        }
    }
}
