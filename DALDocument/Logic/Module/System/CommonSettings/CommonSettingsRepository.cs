using AppDocument.General;
using DALDocument.EF.Context;
using DALDocument.Base;
using AppDocument;
using DALDocument.EF.Entities;
using System.Linq.Expressions;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DALDocument.Logic.System.CommonSettings
{
    public class CommonSettingsRepository : BaseEfCrudRepository<CommonSettingsDto, int, CommonSetting>, ICommonSettingsRepository
    {
        public ISystemSettingProvider SystemSettingProvider { get; }
        public CommonSettingsRepository(ApplicationContext context, ISystemSettingProvider systemSettingProvider)
            : base(context)
        {
            SystemSettingProvider = systemSettingProvider;
        }

        protected override Func<CommonSettingsDto, CommonSetting> entitySelector => (x) => x.ToCommonSettings();
        protected override Expression<Func<CommonSettingsDto, CommonSetting>> entitySelectorExpr => (x) => x.ToCommonSettings();
        protected override Func<CommonSetting, CommonSettingsDto> dtoSelector => (x) => x.ToCommonSettingsDto();
        protected override Expression<Func<CommonSetting, CommonSettingsDto>> dtoSelectorExpr => (x) => x.ToCommonSettingsDto();
        protected override Expression<Func<CommonSetting, bool>> filterSameItemsExpr(CommonSettingsDto dto)
        {
            return x => false;
        }

        public CommonSettingsDto GetOneByCode(string code)
        {
            return GetByFilter(x => x.code == code).FirstOrDefault();
        }

        protected override IQueryable<CommonSetting> TheWholeEntities => context.CommonSettings
            .AsNoTracking()

        ;


    }
}
