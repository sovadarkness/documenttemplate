using AppDocument.General;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.Logic.System.CommonSettings
{
    static partial class CommonSettingsDtoConverter
    {
        public static CommonSettingsDto ToCommonSettingsDto(this CommonSetting _CommonSettings)
        {
            var result = new CommonSettingsDto
            {
                id = _CommonSettings.id,
                name = _CommonSettings.name,
                nameky = _CommonSettings.nameky,
                value = _CommonSettings.value,
                valueky = _CommonSettings.valueky,
                code = _CommonSettings.code,
                description = _CommonSettings.description,

            };
            return result;
        }
        public static CommonSetting ToCommonSettings(this CommonSettingsDto dto)
        {
            var result = new CommonSetting
            {
                id = dto.id,
                name = dto.name,
                nameky = dto.nameky,
                value = dto.value,
                valueky = dto.valueky,
                code = dto.code,
                description = dto.description,

            };
            return result;
        }
    }
}
