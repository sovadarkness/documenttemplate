using AppDocument.SystemSettings;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.SystemSettings
{
    public class LanguageRepository : BaseEfCrudRepository<LanguageDto, int, Language>, ILanguageRepository
    {
        public LanguageRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<LanguageDto, Language> entitySelector => (LanguageDto x) => x.ToLanguage();

        protected override Expression<Func<LanguageDto, Language>> entitySelectorExpr => (LanguageDto x) => x.ToLanguage();

        protected override Func<Language, LanguageDto> dtoSelector => (Language x) => x.ToLanguageDto();

        protected override Expression<Func<Language, LanguageDto>> dtoSelectorExpr => (Language x) => x.ToLanguageDto();

        protected override Expression<Func<Language, bool>> filterSameItemsExpr(LanguageDto dto)
        {
            return x => (x.code != "" && x.code == dto.code) || x.name == dto.name;
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
        }

        protected override IQueryable<Language> TheWholeEntities => context.Languages.AsNoTracking();
		

	        

    }
}
