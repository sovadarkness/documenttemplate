using AppDocument.SystemSettings;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.SystemSettings
{
    static partial class LanguageDtoConverter
    {
        public static LanguageDto ToLanguageDto(this Language _Language)
        {
            var result = new LanguageDto
            {
		id = _Language.id,
		name = _Language.name,
		description = _Language.description,
		code = _Language.code,
		isDefault = _Language.isDefault,
                

            };
            return result;
        }
        public static Language ToLanguage(this LanguageDto dto)
        {
            var result = new Language
            {
		id = dto.id,
		name = dto.name,
		description = dto.description,
		code = dto.code,
		isDefault = dto.isDefault
            };
            return result;
        }
    }
}
