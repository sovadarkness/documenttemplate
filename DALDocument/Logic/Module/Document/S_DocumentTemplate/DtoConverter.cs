using AppDocument.General;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.General
{
    static partial class s_DocumentTemplateDtoConverter
    {
        public static s_DocumentTemplateDto Tos_DocumentTemplateDto(this s_DocumentTemplate _s_DocumentTemplate)
        {
            var result = new s_DocumentTemplateDto
            {
                id = _s_DocumentTemplate.id,
                name = _s_DocumentTemplate.name,
                description = _s_DocumentTemplate.description,
                code = _s_DocumentTemplate.code,
                iconColor = _s_DocumentTemplate.iconColor,
                idCustomSvgIcon = _s_DocumentTemplate.idCustomSvgIcon,
                idDocumentTyplateType = _s_DocumentTemplate.idDocumentTyplateType,
                idCustomSvgIconPreview = _s_DocumentTemplate.idCustomSvgIconNavigation?.svgPath,
                idDocumentTyplateTypeNavName = _s_DocumentTemplate.idDocumentTyplateTypeNavigation?.name
            };
            return result;
        }
        public static s_DocumentTemplate Tos_DocumentTemplate(this s_DocumentTemplateDto dto)
        {
            var result = new s_DocumentTemplate
            {
                id = dto.id,
                name = dto.name,
                description = dto.description,
                code = dto.code,
                idDocumentTyplateType = dto.idDocumentTyplateType,
                iconColor = dto.iconColor,
                idCustomSvgIcon = dto.idCustomSvgIcon,
            };
            return result;
        }
    }
}
