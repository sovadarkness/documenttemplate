using AppDocument.General;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.General
{
    public class s_DocumentTemplateRepository : BaseEfCrudRepository<s_DocumentTemplateDto, int, s_DocumentTemplate>, Is_DocumentTemplateRepository
    {
        public s_DocumentTemplateRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<s_DocumentTemplateDto, s_DocumentTemplate> entitySelector => (s_DocumentTemplateDto x) => x.Tos_DocumentTemplate();

        protected override Expression<Func<s_DocumentTemplateDto, s_DocumentTemplate>> entitySelectorExpr => (s_DocumentTemplateDto x) => x.Tos_DocumentTemplate();

        protected override Func<s_DocumentTemplate, s_DocumentTemplateDto> dtoSelector => (s_DocumentTemplate x) => x.Tos_DocumentTemplateDto();

        protected override Expression<Func<s_DocumentTemplate, s_DocumentTemplateDto>> dtoSelectorExpr => (s_DocumentTemplate x) => x.Tos_DocumentTemplateDto();

        protected override Expression<Func<s_DocumentTemplate, bool>> filterSameItemsExpr(s_DocumentTemplateDto dto)
        {
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;


            if (dto.code != "") // TODO TODO TODO WTFFFFFF
            {
                return x => (x.code != null && x.code == dto.code) || (x.name == dto.name);
            }
            else
            {
                return x => (x.name == dto.name);
            }
        }

        public List<s_DocumentTemplateDto> GetAllTemplate()
        {
            //Todo lang
            var result = context.s_DocumentTemplateTranslations.AsNoTracking()
                .Include(x => x.idDocumentTemplateNavigation)
                //.Include(x => x.idLanguageNavigation) //TODO WTF
                //.Where(x => x.idLanguageNavigation.name == "Ru-ru" || x.idLanguageNavigation.name == "Русский") //TODO WTF
                .Select(x => x.idDocumentTemplateNavigation.Tos_DocumentTemplateDto()).ToList();
            return result;
        }

        protected override IQueryable<s_DocumentTemplate> TheWholeEntities => context.s_DocumentTemplates
            .AsNoTracking()
            .Include(x => x.idCustomSvgIconNavigation)
            .Include(x => x.idDocumentTyplateTypeNavigation)
        ;
    }
}
