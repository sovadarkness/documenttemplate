using AppDocument.General;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.General
{
    static partial class s_QueryDtoConverter
    {
        public static s_QueryDto Tos_QueryDto(this s_Query _s_Query)
        {
            var result = new s_QueryDto
            {
                id = _s_Query.id,
                name = _s_Query.name,
                description = _s_Query.description,
                code = _s_Query.code,
                query = _s_Query.query,

                idQueryType = _s_Query.idQueryType,
                idDBMS = _s_Query.idDBMS,

            };
            return result;
        }
        public static s_Query Tos_Query(this s_QueryDto dto)
        {
            var result = new s_Query
            {
                id = dto.id,
                name = dto.name,
                description = dto.description,
                code = dto.code,
                query = dto.query,
                idQueryType = dto.idQueryType,
                idDBMS = dto.idDBMS
            };
            return result;
        }
    }
}
