using AppDocument.General;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.General
{
    public class s_QueryRepository : BaseEfCrudRepository<s_QueryDto, int, s_Query>, Is_QueryRepository
    {
        public s_QueryRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<s_QueryDto, s_Query> entitySelector => (s_QueryDto x) => x.Tos_Query();

        protected override Expression<Func<s_QueryDto, s_Query>> entitySelectorExpr => (s_QueryDto x) => x.Tos_Query();

        protected override Func<s_Query, s_QueryDto> dtoSelector => (s_Query x) => x.Tos_QueryDto();

        protected override Expression<Func<s_Query, s_QueryDto>> dtoSelectorExpr => (s_Query x) => x.Tos_QueryDto();

        protected override Expression<Func<s_Query, bool>> filterSameItemsExpr(s_QueryDto dto)
        {
            return x => (x.code != "" && x.code == dto.code) || x.name == dto.name;
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
        }

        protected override IQueryable<s_Query> TheWholeEntities => context.s_Queries
            .AsNoTracking()
        ;


        public List<s_QueryDto> GetByidQueryType(int id)
        {
            return GetByFilter(x => x.idQueryType == id);
        }
        public List<s_QueryDto> GetByidDBMS(int id)
        {
            return GetByFilter(x => x.idDBMS == id);
        }

    }
}
