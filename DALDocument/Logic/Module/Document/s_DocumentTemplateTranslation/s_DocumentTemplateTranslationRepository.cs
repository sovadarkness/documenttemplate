using AppDocument.DocTemplate;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.DocTemplate
{
    public class s_DocumentTemplateTranslationRepository : BaseEfCrudRepository<s_DocumentTemplateTranslationDto, int, s_DocumentTemplateTranslation>, Is_DocumentTemplateTranslationRepository
    {
        public s_DocumentTemplateTranslationRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<s_DocumentTemplateTranslationDto, s_DocumentTemplateTranslation> entitySelector => (s_DocumentTemplateTranslationDto x) => x.Tos_DocumentTemplateTranslation();

        protected override Expression<Func<s_DocumentTemplateTranslationDto, s_DocumentTemplateTranslation>> entitySelectorExpr => (s_DocumentTemplateTranslationDto x) => x.Tos_DocumentTemplateTranslation();

        protected override Func<s_DocumentTemplateTranslation, s_DocumentTemplateTranslationDto> dtoSelector => (s_DocumentTemplateTranslation x) => x.Tos_DocumentTemplateTranslationDto();

        protected override Expression<Func<s_DocumentTemplateTranslation, s_DocumentTemplateTranslationDto>> dtoSelectorExpr => (s_DocumentTemplateTranslation x) => x.Tos_DocumentTemplateTranslationDto();

        protected override Expression<Func<s_DocumentTemplateTranslation, bool>> filterSameItemsExpr(s_DocumentTemplateTranslationDto dto)
        {
            return x => false;
            //return x => x.idDocumentTemplate == dto.idDocumentTemplate; // && x.idLanguage == dto.idLanguage; TODO WTF
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
        }

        protected override IQueryable<s_DocumentTemplateTranslation> TheWholeEntities => context.s_DocumentTemplateTranslations
            .AsNoTracking()
		.Include(x => x.idDocumentTemplateNavigation)
        .Include(x => x.idLanguageNavigation)
        ;

	        
	public List<s_DocumentTemplateTranslationDto> GetByidDocumentTemplate(int id)
        {
            return GetByFilter(x=>x.idDocumentTemplate == id);
        }
	public s_DocumentTemplateTranslationDto GetByidLanguageAndIdTemplate(int idLanguage, int idTemplate)
        {
            //throw new NotImplementedException(); // TODO WTF
            var item = TheWholeEntities.FirstOrDefault(x => x.idDocumentTemplate == idTemplate && x.idLanguage == idLanguage);
            return item?.Tos_DocumentTemplateTranslationDto();
            //return GetByFilter(x => x.idDocumentTemplate == idTemplate && x.idLanguage == idLanguage).FirstOrDefault();
        }

        public List<s_DocumentTemplateTranslationDto> GetSubscription()
        {
            var items = TheWholeEntities
                //.Where(x => x.idDocumentTemplateNavigation.isSubscription == true) //TODO WTF
                .OrderByDescending(x => x.id)
                .Select(dtoSelectorExpr)
                .ToList();

            return items;
        }
    }
}
