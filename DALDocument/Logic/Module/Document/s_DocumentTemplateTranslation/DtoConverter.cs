using AppDocument.DocTemplate;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.DocTemplate
{
    static partial class s_DocumentTemplateTranslationDtoConverter
    {
        public static s_DocumentTemplateTranslationDto Tos_DocumentTemplateTranslationDto(this s_DocumentTemplateTranslation _s_DocumentTemplateTranslation)
        {
            var result = new s_DocumentTemplateTranslationDto
            {
                id = _s_DocumentTemplateTranslation.id,
                template = _s_DocumentTemplateTranslation.template,

                idDocumentTemplate = _s_DocumentTemplateTranslation.idDocumentTemplate,
                idDocumentTemplateNavName = _s_DocumentTemplateTranslation.idDocumentTemplateNavigation?.name,
                idLanguage = _s_DocumentTemplateTranslation.idLanguage, //TODO WT
                idLanguageNavName = _s_DocumentTemplateTranslation.idLanguageNavigation?.name,
            };
            return result;
        }
        public static s_DocumentTemplateTranslation Tos_DocumentTemplateTranslation(this s_DocumentTemplateTranslationDto dto)
        {
            var result = new s_DocumentTemplateTranslation
            {
                id = dto.id,
                template = dto.template,
                idDocumentTemplate = dto.idDocumentTemplate,
                idLanguage = dto.idLanguage
            };
            return result;
        }
    }
}
