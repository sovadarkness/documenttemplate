using AppDocument.General;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.General
{
    public class s_PlaceholderGroupRepository : BaseEfCrudRepository<s_PlaceholderGroupDto, int, s_PlaceholderGroup>, Is_PlaceholderGroupRepository
    {
        public s_PlaceholderGroupRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<s_PlaceholderGroupDto, s_PlaceholderGroup> entitySelector => (s_PlaceholderGroupDto x) => x.Tos_PlaceholderGroup();

        protected override Expression<Func<s_PlaceholderGroupDto, s_PlaceholderGroup>> entitySelectorExpr => (s_PlaceholderGroupDto x) => x.Tos_PlaceholderGroup();

        protected override Func<s_PlaceholderGroup, s_PlaceholderGroupDto> dtoSelector => (s_PlaceholderGroup x) => x.Tos_PlaceholderGroupDto();

        protected override Expression<Func<s_PlaceholderGroup, s_PlaceholderGroupDto>> dtoSelectorExpr => (s_PlaceholderGroup x) => x.Tos_PlaceholderGroupDto();

        protected override Expression<Func<s_PlaceholderGroup, bool>> filterSameItemsExpr(s_PlaceholderGroupDto dto)
        {
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
            if (dto.code != "")
            {
                return x => (x.code != null && x.code == dto.code) || x.name == dto.name;
            }
            else
            {
                return x => x.name == dto.name;
            }
        }

        protected override IQueryable<s_PlaceholderGroup> TheWholeEntities => context.s_PlaceholderGroups
           .AsNoTracking()
            //.Include(x => x.idCustomSvgIconNavigation)
        ;


    }
}
