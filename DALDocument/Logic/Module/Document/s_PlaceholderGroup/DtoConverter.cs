using AppDocument.General;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.General
{
    static partial class s_PlaceholderGroupDtoConverter
    {
        public static s_PlaceholderGroupDto Tos_PlaceholderGroupDto(this s_PlaceholderGroup _s_PlaceholderGroup)
        {
            var result = new s_PlaceholderGroupDto
            {
		id = _s_PlaceholderGroup.id,
		name = _s_PlaceholderGroup.name,
		description = _s_PlaceholderGroup.description,
		code = _s_PlaceholderGroup.code,
		queueNumber = _s_PlaceholderGroup.queueNumber,
        //iconColor = _s_PlaceholderGroup.iconColor,
        //idCustomSvgIcon = _s_PlaceholderGroup.idCustomSvgIcon,
        //idCustomSvgIconPreview = _s_PlaceholderGroup.idCustomSvgIconNavigation?.svgPath,


            };
            return result;
        }
        public static s_PlaceholderGroup Tos_PlaceholderGroup(this s_PlaceholderGroupDto dto)
        {
            var result = new s_PlaceholderGroup
            {
		id = dto.id,
		name = dto.name,
		description = dto.description,
		code = dto.code,
		queueNumber = dto.queueNumber,
        //iconColor = dto.iconColor,
        //idCustomSvgIcon = dto.idCustomSvgIcon,
            };
            return result;
        }
    }
}
