using AppDocument.General;
using DALDocument.EF.Context;
using DALDocument.Base;
using DALDocument.EF.Entities;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using Microsoft.Data.SqlClient;
using DALDocument.ExceptionConcerter;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DALDocument.General
{
    public class s_PlaceHolderGroupsMtmRepository : BaseEfCrudRepository<s_PlaceHolderGroupsMtmDto,int, s_PlaceHolderGroupsMtm>, Is_PlaceHolderGroupsMtmRepository
    {
        protected override Func<s_PlaceHolderGroupsMtmDto, s_PlaceHolderGroupsMtm> entitySelector => (s_PlaceHolderGroupsMtmDto x) => x.Tos_PlaceHolderGroupsMtm();
        protected override Expression<Func<s_PlaceHolderGroupsMtmDto, s_PlaceHolderGroupsMtm>> entitySelectorExpr => (s_PlaceHolderGroupsMtmDto x) => x.Tos_PlaceHolderGroupsMtm();
        protected override Func<s_PlaceHolderGroupsMtm, s_PlaceHolderGroupsMtmDto> dtoSelector => (s_PlaceHolderGroupsMtm x) => x.Tos_PlaceHolderGroupsMtmDto();
        protected override Expression<Func<s_PlaceHolderGroupsMtm, s_PlaceHolderGroupsMtmDto>> dtoSelectorExpr => (s_PlaceHolderGroupsMtm x) => x.Tos_PlaceHolderGroupsMtmDto();

        protected override Expression<Func<s_PlaceHolderGroupsMtm, bool>> filterSameItemsExpr(s_PlaceHolderGroupsMtmDto dto)
        {
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
            return x => x.idPlaceHolder == dto.idPlaceHolder && x.idTypePlaceholder == dto.idTypePlaceholder;
        }

        protected override IQueryable<s_PlaceHolderGroupsMtm> TheWholeEntities =>
                context.s_PlaceHolderGroupsMtms
                    .AsNoTracking()
		.Include(x => x.idPlaceHolderNavigation)
		.Include(x => x.idTypePlaceholderNavigation)
					;

        public s_PlaceHolderGroupsMtmRepository(ApplicationContext context)
            : base(context)
        {
        }

        public List<s_PlaceHolderGroupsMtmDto> GetByidPlaceHolder(int idPlaceHolder)
        {
            return GetByFilter(x => x.idPlaceHolder == idPlaceHolder);
        }
        public List<s_PlaceHolderGroupsMtmDto> GetByidTypePlaceholder(int idTypePlaceholder)
        {
            return GetByFilter(x => x.idTypePlaceholder == idTypePlaceholder);
        }

        public s_PlaceHolderGroupsMtmDto GetExact(int idPlaceHolder, int idTypePlaceholder)
        {
            return GetByFilter(x => x.idTypePlaceholder == idTypePlaceholder && x.idPlaceHolder == idPlaceHolder).FirstOrDefault();
        }
    }
}

