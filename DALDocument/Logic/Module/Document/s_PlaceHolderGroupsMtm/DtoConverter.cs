using AppDocument.General;
using DALDocument.EF.Entities;

namespace DALDocument.General
{
    static partial class s_PlaceHolderGroupsMtmDtoConverter
    {
        public static s_PlaceHolderGroupsMtmDto Tos_PlaceHolderGroupsMtmDto(this s_PlaceHolderGroupsMtm _s_PlaceHolderGroupsMtm)
        {
            var result = new s_PlaceHolderGroupsMtmDto
            {
                id = _s_PlaceHolderGroupsMtm.id,

                idPlaceHolder = _s_PlaceHolderGroupsMtm.idPlaceHolder,
                idPlaceHolderNavName = _s_PlaceHolderGroupsMtm.idPlaceHolderNavigation?.name,
                idTypePlaceholder = _s_PlaceHolderGroupsMtm.idTypePlaceholder,
                idTypePlaceholderNavName = _s_PlaceHolderGroupsMtm.idTypePlaceholderNavigation?.name

            };
            return result;
        }
        public static s_PlaceHolderGroupsMtm Tos_PlaceHolderGroupsMtm(this s_PlaceHolderGroupsMtmDto dto)
        {
            var result = new s_PlaceHolderGroupsMtm
            {
                id = dto.id,
                idPlaceHolder = dto.idPlaceHolder,
                idTypePlaceholder = dto.idTypePlaceholder
            };
            return result;
        }
    }
}
