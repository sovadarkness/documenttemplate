using AppDocument.DocTemplate;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.DocTemplate
{
    public class roDocumentTemplateCategoryRepository : BaseEfCrudRepository<roDocumentTemplateCategoryDto, int, roDocumentTemplateCategory>, IroDocumentTemplateCategoryRepository
    {
        public roDocumentTemplateCategoryRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<roDocumentTemplateCategoryDto, roDocumentTemplateCategory> entitySelector => (roDocumentTemplateCategoryDto x) => x.ToroDocumentTemplateCategory();

        protected override Expression<Func<roDocumentTemplateCategoryDto, roDocumentTemplateCategory>> entitySelectorExpr => (roDocumentTemplateCategoryDto x) => x.ToroDocumentTemplateCategory();

        protected override Func<roDocumentTemplateCategory, roDocumentTemplateCategoryDto> dtoSelector => (roDocumentTemplateCategory x) => x.ToroDocumentTemplateCategoryDto();

        protected override Expression<Func<roDocumentTemplateCategory, roDocumentTemplateCategoryDto>> dtoSelectorExpr => (roDocumentTemplateCategory x) => x.ToroDocumentTemplateCategoryDto();

        protected override Expression<Func<roDocumentTemplateCategory, bool>> filterSameItemsExpr(roDocumentTemplateCategoryDto dto)
        {
            return x => (x.code != "" && x.code == dto.code) || x.name == dto.name;
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
        }

        protected override IQueryable<roDocumentTemplateCategory> TheWholeEntities => context.roDocumentTemplateCategories
            .AsNoTracking()
		;

	        

    }
}
