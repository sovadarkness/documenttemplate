using AppDocument.DocTemplate;
using DALDocument.EF.Entities;
using System.Linq;

namespace DALDocument.DocTemplate
{
    static partial class roDocumentTemplateCategoryDtoConverter
    {
        public static roDocumentTemplateCategoryDto ToroDocumentTemplateCategoryDto(this roDocumentTemplateCategory _roDocumentTemplateCategory)
        {
            var result = new roDocumentTemplateCategoryDto
            {
		id = _roDocumentTemplateCategory.id,
		name = _roDocumentTemplateCategory.name,
		description = _roDocumentTemplateCategory.description,
		code = _roDocumentTemplateCategory.code,
		queueNumber = _roDocumentTemplateCategory.queueNumber,
                

            };
            return result;
        }
        public static roDocumentTemplateCategory ToroDocumentTemplateCategory(this roDocumentTemplateCategoryDto dto)
        {
            var result = new roDocumentTemplateCategory
            {
		id = dto.id,
		name = dto.name,
		description = dto.description,
		code = dto.code,
		queueNumber = dto.queueNumber
            };
            return result;
        }
    }
}
