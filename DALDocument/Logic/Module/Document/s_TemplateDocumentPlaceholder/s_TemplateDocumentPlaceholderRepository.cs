using AppDocument.DocTemplate;
using DALDocument.EF.Context;
using DALDocument.Base;
using DALDocument.EF.Entities;
using System.Collections.Generic;
using System;
using System.Data;
using System.Linq.Expressions;
using Microsoft.Data.SqlClient;
using DALDocument.ExceptionConcerter;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text.RegularExpressions;
using DALDocument.EF.EF.Context;

namespace DALDocument.DocTemplate
{
    public class s_TemplateDocumentPlaceholderRepository : BaseEfCrudRepository<s_TemplateDocumentPlaceholderDto, int, s_TemplateDocumentPlaceholder>, Is_TemplateDocumentPlaceholderRepository
    {
        protected override Func<s_TemplateDocumentPlaceholderDto, s_TemplateDocumentPlaceholder> entitySelector => (s_TemplateDocumentPlaceholderDto x) => x.Tos_TemplateDocumentPlaceholder();
        protected override Expression<Func<s_TemplateDocumentPlaceholderDto, s_TemplateDocumentPlaceholder>> entitySelectorExpr => (s_TemplateDocumentPlaceholderDto x) => x.Tos_TemplateDocumentPlaceholder();
        protected override Func<s_TemplateDocumentPlaceholder, s_TemplateDocumentPlaceholderDto> dtoSelector => (s_TemplateDocumentPlaceholder x) => x.Tos_TemplateDocumentPlaceholderDto();
        protected override Expression<Func<s_TemplateDocumentPlaceholder, s_TemplateDocumentPlaceholderDto>> dtoSelectorExpr => (s_TemplateDocumentPlaceholder x) => x.Tos_TemplateDocumentPlaceholderDto();

        protected override Expression<Func<s_TemplateDocumentPlaceholder, bool>> filterSameItemsExpr(s_TemplateDocumentPlaceholderDto dto)
        {
            return x => false;
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;

        }

        protected override IQueryable<s_TemplateDocumentPlaceholder> TheWholeEntities =>
                context.s_TemplateDocumentPlaceholders
                    .AsNoTracking()
        .Include(x => x.idTemplateDocumentNavigation)
        .Include(x => x.idPlaceholderNavigation)
                    ;

        public s_TemplateDocumentPlaceholderRepository(ApplicationContext context)
            : base(context)
        {
        }

        public List<s_TemplateDocumentPlaceholderDto> GetByidTemplateDocument(int idTemplateDocument)
        {
            return GetByFilter(x => x.idTemplateDocument == idTemplateDocument);
        }
        public List<s_TemplateDocumentPlaceholderDto> GetByidPlaceholder(int idPlaceholder)
        {
            return GetByFilter(x => x.idPlaceholder == idPlaceholder);
        }

        public List<s_PlaceHolderTemplateDto> GetPlaceholderByidTemplateDocument(int idDocument)
        {
            var list = context.s_TemplateDocumentPlaceholders.AsNoTracking()
                .Where(x => x.idTemplateDocument == idDocument)
                .Include(x=>x.idPlaceholderNavigation)
                .ThenInclude(x=>x.idQueryNavigation)
                .Select(x => x.idPlaceholderNavigation.Tos_PlaceHolderTemplateDto())
                .ToList();
            return list;

            //var idPlasholder = GetByFilter(x => x.idTemplateDocument == idDocument)
            //    .Select(x => x.idPlaceholder).ToList();

            //var plasholders = context.s_PlaceHolderTemplates
            //    .Where(x => idPlasholder.Any(p => p == x.id))
            //    .Select(x => x.Tos_PlaceHolderTemplateDto()).ToList();
            //return plasholders;
        }

        public string GetFilledHtml(string htmlTemplate, List<s_PlaceHolderTemplateDto> placeholders, Dictionary<string, object> sqlParameters)
        {
            var html = htmlTemplate;

            var queryIds = placeholders
                //.Select(x => x.idQuery)
                .Distinct()
                .ToList();

            var queryResults = new Dictionary<int, DataTable>();
            foreach (var queryId in queryIds)
            {
                if (queryResults.ContainsKey(queryId.idQuery)) continue;
                //var query = context.s_Queries.FirstOrDefault(x => x.id == queryId.idQuery);
                var sqlPar = new Dictionary<string, object>();
                foreach (var par in sqlParameters)
                {
                    sqlPar.Add(par.Key, par.Value);
                }
                if (string.IsNullOrWhiteSpace(queryId.idQueryCommand)) continue;
                var dataTable = context.DataTable(queryId.idQueryCommand, sqlPar);
                queryResults.Add(queryId.idQuery, dataTable);
            }

            foreach (var p in placeholders)
            {
                if (!queryResults.ContainsKey(p.idQuery)) continue;
                var dataTable = queryResults[p.idQuery];
                var rows = dataTable.Select();

                string value = "";
                switch (p.code)
                {
                    case "table_string":
                        value = ConvertDataTableToTableBorderHTML2(dataTable, p);
                        break;
                    case "table":
                        value = ConvertDataTableToTableBorderHTML(dataTable, p);
                        break;
                    case string t when t.Contains("table"):
                        string style = "";
                        if (p.code.Contains("["))
                        {
                            style = p.code.Replace("table[", "").Replace("]", "");
                            value = ConvertDataTableToTableHTML(dataTable, p, style);
                        }
                        else
                        {
                            value = ConvertDataTableToTableHTML(dataTable, p);
                        }
                        break;
                    case "image":
                        value = ConvertDataTableToImageHTML(rows, p);
                        break;
                    case "comma":
                        value = ConvertDataTableToCommaText(dataTable, p.description);
                        break;

                    //case "newlineSimple":
                    //    value = ConvertDataTableToNewlineSimpleText(dataTable, p.description);
                    //    break;
                    case "hour_minute_amb":
                        if (rows.Length == 0) break;
                        if (rows[0][p.description] == DBNull.Value) break;
                        var dateTime = Convert.ToDateTime(rows[0][p.description]);
                        value = "ч " + dateTime.Hour.ToString("D2") + " мин " + dateTime.Minute.ToString("D2");
                        break;
                    case "hour_minute":
                        if (rows.Length == 0) break;
                        if (rows[0][p.description] == DBNull.Value) break;
                        value = Convert.ToDateTime(rows[0][p.description]).ToString("HH:mm");
                        break;
                    case "hour":
                        if (rows.Length == 0) break;
                        if (rows[0][p.description] == DBNull.Value) break;
                        value = Convert.ToDateTime(rows[0][p.description]).Hour.ToString("D2");
                        break;
                    case "minute":
                        if (rows.Length == 0) break;
                        if (rows[0][p.description] == DBNull.Value) break;
                        value = Convert.ToDateTime(rows[0][p.description]).Minute.ToString("D2");
                        break;
                    default:
                        if (rows.Length == 0)
                        {
                            value = "";
                            break;
                        }
                        value = rows[0][p.description].ToString();
                        break;
                }
                html = html.Replace("[[" + p.name + "]]", value);
            }
            return html;
        }

        private string ConvertDataTableToTableHTML(DataTable dt, s_PlaceHolderTemplateDto placeholder, string style = "")
        {
            var parameters = placeholder.description.Split(",").Select(x => x.Trim()).ToList();
            string html = @"<table style=""width: 100%;border-collapse: collapse;"">";
            string tdStyle = @"style=""border-collapse:collapse;""";
            if (style != "")
            {
                tdStyle = @$"style=""{style}""";
            }
            var dtRows = dt.Select();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < parameters.Count; j++)
                    html += @$"<td {tdStyle}><span>" + dtRows[i][parameters[j]].ToString() + "</span></td>";
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        private string ConvertDataTableToTableBorderHTML2(DataTable dt, s_PlaceHolderTemplateDto placeholder)
        {
            var parameters = placeholder.description.Split(",").Select(x => x.Trim()).ToList();
            string html = @"";
            var dtRows = dt.Select();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<strong>" + dtRows[i][parameters[0]].ToString() + "</strong>: ";
                for (int j = 1; j < parameters.Count; j++)
                { 
                    if (j == parameters.Count - 1)
                        html += dtRows[i][parameters[j]].ToString();
                    else html += dtRows[i][parameters[j]].ToString() + ",";
                }
                html += "</br>";
            }
            return html;
        }

        private string ConvertDataTableToTableBorderHTML(DataTable dt, s_PlaceHolderTemplateDto placeholder)
        {
            var parameters = placeholder.description.Split(",").Select(x => x.Trim()).ToList();
            string html = @"<table style=""border-collapse:collapse; border: 1px solid black; width: 100%;"">";
            var dtRows = dt.Select();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < parameters.Count; j++)
                    html += @"<td style=""border-collapse:collapse; border: 1px solid black;"">" + dtRows[i][parameters[j]].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        private string ConvertDataTableToCommaText(DataTable dt, string columnName)
        {
            var lst = new List<string>();

            foreach (DataRow row in dt.Rows)
            {
                var s = row[columnName].ToString();
                lst.Add(s);
            }

            var result = string.Join(", ", lst);

            return result;
        }

        private string ConvertDataTableToImageHTML(DataRow[] dr, s_PlaceHolderTemplateDto placeholder)
        {
            var imagePath = dr[0][placeholder.description].ToString();
            string html = @$"<img src={imagePath}>";
            return html;
        }
    }
}

