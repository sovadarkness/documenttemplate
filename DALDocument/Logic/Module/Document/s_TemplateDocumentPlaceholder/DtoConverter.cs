using AppDocument.DocTemplate;
using DALDocument.EF.Entities;

namespace DALDocument.DocTemplate
{
    static partial class s_TemplateDocumentPlaceholderDtoConverter
    {
        public static s_TemplateDocumentPlaceholderDto Tos_TemplateDocumentPlaceholderDto(this s_TemplateDocumentPlaceholder _s_TemplateDocumentPlaceholder)
        {
            var result = new s_TemplateDocumentPlaceholderDto
            {
                id = _s_TemplateDocumentPlaceholder.id,

                idTemplateDocument = _s_TemplateDocumentPlaceholder.idTemplateDocument,
                //idTemplateDocumentNavName = _s_TemplateDocumentPlaceholder.idTemplateDocumentNavigation?.name,
                idPlaceholder = _s_TemplateDocumentPlaceholder.idPlaceholder,
                idPlaceholderNavName = _s_TemplateDocumentPlaceholder.idPlaceholderNavigation?.name

            };
            return result;
        }
        public static s_TemplateDocumentPlaceholder Tos_TemplateDocumentPlaceholder(this s_TemplateDocumentPlaceholderDto dto)
        {
            var result = new s_TemplateDocumentPlaceholder
            {
                id = dto.id,
                idTemplateDocument = dto.idTemplateDocument,
                idPlaceholder = dto.idPlaceholder
            };
            return result;
        }
    }
}
