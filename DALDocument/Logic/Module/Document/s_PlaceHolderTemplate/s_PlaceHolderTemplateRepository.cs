using AppDocument.DocTemplate;
using DALDocument.EF.Context;
using DALDocument.Base;
using Core.Abstraction;
using AppDocument;
using DALDocument.EF.Entities;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DALDocument.DocTemplate
{
    public class s_PlaceHolderTemplateRepository : BaseEfCrudRepository<s_PlaceHolderTemplateDto, int, s_PlaceHolderTemplate>, Is_PlaceHolderTemplateRepository
    {
        public s_PlaceHolderTemplateRepository(ApplicationContext context)
            : base(context)
        {
        }

        protected override Func<s_PlaceHolderTemplateDto, s_PlaceHolderTemplate> entitySelector => (s_PlaceHolderTemplateDto x) => x.Tos_PlaceHolderTemplate();

        protected override Expression<Func<s_PlaceHolderTemplateDto, s_PlaceHolderTemplate>> entitySelectorExpr => (s_PlaceHolderTemplateDto x) => x.Tos_PlaceHolderTemplate();

        protected override Func<s_PlaceHolderTemplate, s_PlaceHolderTemplateDto> dtoSelector => (s_PlaceHolderTemplate x) => x.Tos_PlaceHolderTemplateDto();

        protected override Expression<Func<s_PlaceHolderTemplate, s_PlaceHolderTemplateDto>> dtoSelectorExpr => (s_PlaceHolderTemplate x) => x.Tos_PlaceHolderTemplateDto();

        protected override Expression<Func<s_PlaceHolderTemplate, bool>> filterSameItemsExpr(s_PlaceHolderTemplateDto dto)
        {
            return x => (x.name == dto.name && x.idQuery == dto.idQuery);
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;
        }

        protected override IQueryable<s_PlaceHolderTemplate> TheWholeEntities => context.s_PlaceHolderTemplates
            .AsNoTracking()
        .Include(x => x.idQueryNavigation)
        ;


        public List<s_PlaceHolderTemplateDto> GetByidQuery(int id)
        {
            return GetByFilter(x => x.idQuery == id);
        }

        public List<s_PlaceHolderTemplateDto> GetByNames(IEnumerable<string> names)
        {
            var placeholders = GetByFilter(x => names.Contains(x.name)).ToList();
            return placeholders;
        }
    }
}
