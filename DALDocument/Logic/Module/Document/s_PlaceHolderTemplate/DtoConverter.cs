using AppDocument.DocTemplate;
using DALDocument.EF.Entities;
using System.Linq;
using AppDocument.General;

namespace DALDocument.DocTemplate
{
    static partial class s_PlaceHolderTemplateDtoConverter
    {
        public static s_PlaceHolderTemplateDto Tos_PlaceHolderTemplateDto(this s_PlaceHolderTemplate _s_PlaceHolderTemplate)
        {
            var result = new s_PlaceHolderTemplateDto
            {
                id = _s_PlaceHolderTemplate.id,
                name = _s_PlaceHolderTemplate.name,
                description = _s_PlaceHolderTemplate.description,
                code = _s_PlaceHolderTemplate.code,
                idQuery = _s_PlaceHolderTemplate.idQuery,
                idQueryNavName = _s_PlaceHolderTemplate.idQueryNavigation?.name,
                idQueryCommand = _s_PlaceHolderTemplate.idQueryNavigation?.query

            };
            return result;
        }
        public static s_PlaceHolderTemplate Tos_PlaceHolderTemplate(this s_PlaceHolderTemplateDto dto)
        {
            var result = new s_PlaceHolderTemplate
            {
                id = dto.id,
                name = dto.name,
                description = dto.description,
                code = dto.code,
                idQuery = dto.idQuery
            };
            return result;
        }
    }
}
