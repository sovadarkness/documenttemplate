using AppDocument.DocTemplate;
using DALDocument.EF.Context;
using DALDocument.Base;
using DALDocument.EF.Entities;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using Microsoft.Data.SqlClient;
using DALDocument.ExceptionConcerter;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DALDocument.DocTemplate
{
    public class s_QueriesDocumentTemplateRepository : BaseEfCrudRepository<s_QueriesDocumentTemplateDto,int,s_QueriesDocumentTemplate>, Is_QueriesDocumentTemplateRepository
    {
        protected override Func<s_QueriesDocumentTemplateDto, s_QueriesDocumentTemplate> entitySelector => (s_QueriesDocumentTemplateDto x) => x.Tos_QueriesDocumentTemplate();
        protected override Expression<Func<s_QueriesDocumentTemplateDto, s_QueriesDocumentTemplate>> entitySelectorExpr => (s_QueriesDocumentTemplateDto x) => x.Tos_QueriesDocumentTemplate();
        protected override Func<s_QueriesDocumentTemplate, s_QueriesDocumentTemplateDto> dtoSelector => (s_QueriesDocumentTemplate x) => x.Tos_QueriesDocumentTemplateDto();
        protected override Expression<Func<s_QueriesDocumentTemplate, s_QueriesDocumentTemplateDto>> dtoSelectorExpr => (s_QueriesDocumentTemplate x) => x.Tos_QueriesDocumentTemplateDto();

        protected override Expression<Func<s_QueriesDocumentTemplate, bool>> filterSameItemsExpr(s_QueriesDocumentTemplateDto dto)
        {
            return x => false;
            // Уточнить у аналитика , по каким параметрам проверяется наличие дублиатат записи в БД и реализовать 
            // sample return x => false;  если нет проверки на дублирование 
            // Или
            // sample return x => (x.code != null && x.code == dto.Code) || x.name == dto.Name;

        }

        protected override IQueryable<s_QueriesDocumentTemplate> TheWholeEntities =>
                context.s_QueriesDocumentTemplates
                    .AsNoTracking()
		.Include(x => x.idDocumentTemplateNavigation)
		.Include(x => x.idQueryNavigation)
					;

        public s_QueriesDocumentTemplateRepository(ApplicationContext context)
            : base(context)
        {
        }

        public List<s_QueriesDocumentTemplateDto> GetByidDocumentTemplate(int idDocumentTemplate)
        {
            return GetByFilter(x => x.idDocumentTemplate == idDocumentTemplate);
        }
        public List<s_QueriesDocumentTemplateDto> GetByidQuery(int idQuery)
        {
            return GetByFilter(x => x.idQuery == idQuery);
        }

        
    }
}

