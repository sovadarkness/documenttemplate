using AppDocument.DocTemplate;
using DALDocument.EF.Entities;

namespace DALDocument.DocTemplate
{
    static partial class s_QueriesDocumentTemplateDtoConverter
    {
        public static s_QueriesDocumentTemplateDto Tos_QueriesDocumentTemplateDto(this s_QueriesDocumentTemplate _s_QueriesDocumentTemplate)
        {
            var result = new s_QueriesDocumentTemplateDto
            {
		id = _s_QueriesDocumentTemplate.id,
                
                idDocumentTemplate = _s_QueriesDocumentTemplate.idDocumentTemplate,
                //idDocumentTemplateNavName = _s_QueriesDocumentTemplate.idDocumentTemplateNavigation?.name,
                idQuery = _s_QueriesDocumentTemplate.idQuery,
                idQueryNavName = _s_QueriesDocumentTemplate.idQueryNavigation?.name

            };
            return result;
        }
        public static s_QueriesDocumentTemplate Tos_QueriesDocumentTemplate(this s_QueriesDocumentTemplateDto dto)
        {
            var result = new s_QueriesDocumentTemplate
            {
		id = dto.id,
		idDocumentTemplate = dto.idDocumentTemplate,
		idQuery = dto.idQuery
            };
            return result;
        }
    }
}
