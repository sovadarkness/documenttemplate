﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class SystemSetting
    {
        public int id { get; set; }
        public string ro_name { get; set; }
        public string ro_description { get; set; }
        public int? ro_queueNumber { get; set; }
        public string ro_code { get; set; }
        public string value { get; set; }
    }
}
