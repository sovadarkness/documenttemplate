﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class CommonSetting
    {
        public int id { get; set; }
        public string name { get; set; }
        public string nameky { get; set; }
        public string value { get; set; }
        public string valueky { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }
}
