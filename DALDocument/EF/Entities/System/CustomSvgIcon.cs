﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class CustomSvgIcon
    {
        public CustomSvgIcon()
        {
            s_DocumentTemplates = new HashSet<s_DocumentTemplate>();
            s_PlaceholderGroups = new HashSet<s_PlaceholderGroup>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string svgPath { get; set; }
        public string usedTables { get; set; }

        public virtual ICollection<s_DocumentTemplate> s_DocumentTemplates { get; set; }
        public virtual ICollection<s_PlaceholderGroup> s_PlaceholderGroups { get; set; }
    }
}
