﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_TemplateDocumentPlaceholder
    {
        public int id { get; set; }
        public int idTemplateDocument { get; set; }
        public int idPlaceholder { get; set; }

        public virtual s_PlaceHolderTemplate idPlaceholderNavigation { get; set; }
        public virtual s_DocumentTemplateTranslation idTemplateDocumentNavigation { get; set; }
    }
}
