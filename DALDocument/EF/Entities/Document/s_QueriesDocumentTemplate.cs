﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_QueriesDocumentTemplate
    {
        public int id { get; set; }
        public int idDocumentTemplate { get; set; }
        public int idQuery { get; set; }

        public virtual s_DocumentTemplateTranslation idDocumentTemplateNavigation { get; set; }
        public virtual s_Query idQueryNavigation { get; set; }
    }
}
