﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class Language
    {
        public Language()
        {
            s_DocumentTemplateTranslations = new HashSet<s_DocumentTemplateTranslation>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public bool? isDefault { get; set; }
        public int? queueNumber { get; set; }

        public virtual ICollection<s_DocumentTemplateTranslation> s_DocumentTemplateTranslations { get; set; }
    }
}
