﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_PlaceHolderTemplate
    {
        public s_PlaceHolderTemplate()
        {
            s_PlaceHolderGroupsMtms = new HashSet<s_PlaceHolderGroupsMtm>();
            s_TemplateDocumentPlaceholders = new HashSet<s_TemplateDocumentPlaceholder>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int idQuery { get; set; }

        public virtual s_Query idQueryNavigation { get; set; }
        public virtual ICollection<s_PlaceHolderGroupsMtm> s_PlaceHolderGroupsMtms { get; set; }
        public virtual ICollection<s_TemplateDocumentPlaceholder> s_TemplateDocumentPlaceholders { get; set; }
    }
}
