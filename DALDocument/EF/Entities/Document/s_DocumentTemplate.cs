﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_DocumentTemplate
    {
        public s_DocumentTemplate()
        {
            s_DocumentTemplateTranslations = new HashSet<s_DocumentTemplateTranslation>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int? idCustomSvgIcon { get; set; }
        public string iconColor { get; set; }
        public int? idDocumentTyplateType { get; set; }

        public virtual CustomSvgIcon idCustomSvgIconNavigation { get; set; }
        public virtual roDocumentTemplateCategory idDocumentTyplateTypeNavigation { get; set; }
        public virtual ICollection<s_DocumentTemplateTranslation> s_DocumentTemplateTranslations { get; set; }
    }
}
