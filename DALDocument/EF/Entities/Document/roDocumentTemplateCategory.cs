﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class roDocumentTemplateCategory
    {
        public roDocumentTemplateCategory()
        {
            s_DocumentTemplates = new HashSet<s_DocumentTemplate>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int? queueNumber { get; set; }

        public virtual ICollection<s_DocumentTemplate> s_DocumentTemplates { get; set; }
    }
}
