﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_PlaceholderGroup
    {
        public s_PlaceholderGroup()
        {
            s_PlaceHolderGroupsMtms = new HashSet<s_PlaceHolderGroupsMtm>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int? queueNumber { get; set; }
        public int? idCustomSvgIcon { get; set; }
        public string iconColor { get; set; }

        public virtual CustomSvgIcon idCustomSvgIconNavigation { get; set; }
        public virtual ICollection<s_PlaceHolderGroupsMtm> s_PlaceHolderGroupsMtms { get; set; }
    }
}
