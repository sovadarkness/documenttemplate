﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_PlaceHolderGroupsMtm
    {
        public int id { get; set; }
        public int? idPlaceHolder { get; set; }
        public int? idTypePlaceholder { get; set; }

        public virtual s_PlaceHolderTemplate idPlaceHolderNavigation { get; set; }
        public virtual s_PlaceholderGroup idTypePlaceholderNavigation { get; set; }
    }
}
