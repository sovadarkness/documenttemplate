﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_DocumentTemplateTranslation
    {
        public s_DocumentTemplateTranslation()
        {
            s_QueriesDocumentTemplates = new HashSet<s_QueriesDocumentTemplate>();
            s_TemplateDocumentPlaceholders = new HashSet<s_TemplateDocumentPlaceholder>();
        }

        public int id { get; set; }
        public string template { get; set; }
        public int idDocumentTemplate { get; set; }
        public int idLanguage { get; set; }

        public virtual s_DocumentTemplate idDocumentTemplateNavigation { get; set; }
        public virtual Language idLanguageNavigation { get; set; }
        public virtual ICollection<s_QueriesDocumentTemplate> s_QueriesDocumentTemplates { get; set; }
        public virtual ICollection<s_TemplateDocumentPlaceholder> s_TemplateDocumentPlaceholders { get; set; }
    }
}
