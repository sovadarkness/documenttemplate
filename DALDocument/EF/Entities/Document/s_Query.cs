﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DALDocument.EF.Entities
{
    public partial class s_Query
    {
        public s_Query()
        {
            s_PlaceHolderTemplates = new HashSet<s_PlaceHolderTemplate>();
            s_QueriesDocumentTemplates = new HashSet<s_QueriesDocumentTemplate>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public string query { get; set; }
        public int idQueryType { get; set; }
        public int idDBMS { get; set; }

        public virtual ICollection<s_PlaceHolderTemplate> s_PlaceHolderTemplates { get; set; }
        public virtual ICollection<s_QueriesDocumentTemplate> s_QueriesDocumentTemplates { get; set; }
    }
}
