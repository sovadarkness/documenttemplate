﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DALDocument.EF.Entities;

#nullable disable

namespace DALDocument.EF.Context
{
    public partial class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CommonSetting> CommonSettings { get; set; }
        public virtual DbSet<CustomSvgIcon> CustomSvgIcons { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<SystemSetting> SystemSettings { get; set; }
        public virtual DbSet<roDocumentTemplateCategory> roDocumentTemplateCategories { get; set; }
        public virtual DbSet<s_DocumentTemplate> s_DocumentTemplates { get; set; }
        public virtual DbSet<s_DocumentTemplateTranslation> s_DocumentTemplateTranslations { get; set; }
        public virtual DbSet<s_PlaceHolderGroupsMtm> s_PlaceHolderGroupsMtms { get; set; }
        public virtual DbSet<s_PlaceHolderTemplate> s_PlaceHolderTemplates { get; set; }
        public virtual DbSet<s_PlaceholderGroup> s_PlaceholderGroups { get; set; }
        public virtual DbSet<s_QueriesDocumentTemplate> s_QueriesDocumentTemplates { get; set; }
        public virtual DbSet<s_Query> s_Queries { get; set; }
        public virtual DbSet<s_TemplateDocumentPlaceholder> s_TemplateDocumentPlaceholders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=amb_1207;Username=postgres;Password=00890");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("postgis")
                .HasAnnotation("Relational:Collation", "Russian_Russia.1251");
            

            modelBuilder.Entity<CommonSetting>(entity =>
            {
                entity.Property(e => e.id).UseIdentityAlwaysColumn();

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");

                entity.Property(e => e.nameky).HasColumnType("character varying");

                entity.Property(e => e.value).HasColumnType("character varying");

                entity.Property(e => e.valueky).HasColumnType("character varying");
            });

            modelBuilder.Entity<CustomSvgIcon>(entity =>
            {
                entity.ToTable("CustomSvgIcon");

                entity.Property(e => e.name).HasColumnType("character varying");

                entity.Property(e => e.svgPath).HasColumnType("character varying");

                entity.Property(e => e.usedTables).HasColumnType("character varying");
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.ToTable("Language");

                entity.Property(e => e.id).UseIdentityAlwaysColumn();

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");
            });

            modelBuilder.Entity<SystemSetting>(entity =>
            {
                entity.ToTable("SystemSetting");

                entity.Property(e => e.ro_code).HasColumnType("character varying");

                entity.Property(e => e.ro_description).HasColumnType("character varying");

                entity.Property(e => e.ro_name).HasColumnType("character varying");

                entity.Property(e => e.value).HasColumnType("character varying");
            });

            modelBuilder.Entity<roDocumentTemplateCategory>(entity =>
            {
                entity.ToTable("roDocumentTemplateCategory");

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");
            });

            modelBuilder.Entity<s_DocumentTemplate>(entity =>
            {
                entity.ToTable("s_DocumentTemplate");

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.iconColor).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");

                entity.HasOne(d => d.idCustomSvgIconNavigation)
                    .WithMany(p => p.s_DocumentTemplates)
                    .HasForeignKey(d => d.idCustomSvgIcon)
                    .HasConstraintName("R_441");

                entity.HasOne(d => d.idDocumentTyplateTypeNavigation)
                    .WithMany(p => p.s_DocumentTemplates)
                    .HasForeignKey(d => d.idDocumentTyplateType)
                    .HasConstraintName("R_608");
            });

            modelBuilder.Entity<s_DocumentTemplateTranslation>(entity =>
            {
                entity.ToTable("s_DocumentTemplateTranslation");

                entity.Property(e => e.template).HasColumnType("character varying");

                entity.HasOne(d => d.idDocumentTemplateNavigation)
                    .WithMany(p => p.s_DocumentTemplateTranslations)
                    .HasForeignKey(d => d.idDocumentTemplate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_271");

                entity.HasOne(d => d.idLanguageNavigation)
                    .WithMany(p => p.s_DocumentTemplateTranslations)
                    .HasForeignKey(d => d.idLanguage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DocumentLanguage");
            });

            modelBuilder.Entity<s_PlaceHolderGroupsMtm>(entity =>
            {
                entity.ToTable("s_PlaceHolderGroupsMtm");

                entity.HasOne(d => d.idPlaceHolderNavigation)
                    .WithMany(p => p.s_PlaceHolderGroupsMtms)
                    .HasForeignKey(d => d.idPlaceHolder)
                    .HasConstraintName("R_446");

                entity.HasOne(d => d.idTypePlaceholderNavigation)
                    .WithMany(p => p.s_PlaceHolderGroupsMtms)
                    .HasForeignKey(d => d.idTypePlaceholder)
                    .HasConstraintName("R_447");
            });

            modelBuilder.Entity<s_PlaceHolderTemplate>(entity =>
            {
                entity.ToTable("s_PlaceHolderTemplate");

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");

                entity.HasOne(d => d.idQueryNavigation)
                    .WithMany(p => p.s_PlaceHolderTemplates)
                    .HasForeignKey(d => d.idQuery)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_137");
            });

            modelBuilder.Entity<s_PlaceholderGroup>(entity =>
            {
                entity.ToTable("s_PlaceholderGroup");

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.iconColor).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");

                entity.HasOne(d => d.idCustomSvgIconNavigation)
                    .WithMany(p => p.s_PlaceholderGroups)
                    .HasForeignKey(d => d.idCustomSvgIcon)
                    .HasConstraintName("R_689");
            });

            modelBuilder.Entity<s_QueriesDocumentTemplate>(entity =>
            {
                entity.ToTable("s_QueriesDocumentTemplate");

                entity.HasOne(d => d.idDocumentTemplateNavigation)
                    .WithMany(p => p.s_QueriesDocumentTemplates)
                    .HasForeignKey(d => d.idDocumentTemplate)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_101");

                entity.HasOne(d => d.idQueryNavigation)
                    .WithMany(p => p.s_QueriesDocumentTemplates)
                    .HasForeignKey(d => d.idQuery)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_102");
            });

            modelBuilder.Entity<s_Query>(entity =>
            {
                entity.ToTable("s_Query");

                entity.Property(e => e.code).HasColumnType("character varying");

                entity.Property(e => e.description).HasColumnType("character varying");

                entity.Property(e => e.name).HasColumnType("character varying");

                entity.Property(e => e.query).HasColumnType("character varying");

            });

            modelBuilder.Entity<s_TemplateDocumentPlaceholder>(entity =>
            {
                entity.ToTable("s_TemplateDocumentPlaceholder");

                entity.HasOne(d => d.idPlaceholderNavigation)
                    .WithMany(p => p.s_TemplateDocumentPlaceholders)
                    .HasForeignKey(d => d.idPlaceholder)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_139");

                entity.HasOne(d => d.idTemplateDocumentNavigation)
                    .WithMany(p => p.s_TemplateDocumentPlaceholders)
                    .HasForeignKey(d => d.idTemplateDocument)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_138");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
