﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Microsoft.Extensions.Configuration;

namespace DALDocument.EF.EF.Context
{
    public static class DbContextExtensions
    {
        public static DataTable DataTable(this DbContext context,
            string sqlQuery, Dictionary<string, object> parameters)
        {

            IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
            var str = configuration.GetConnectionString("DefaultConnection");

            DataTable dataTable = new DataTable();
            //DbConnection dbConnection = context.Database.GetDbConnection();
            var connection = new NpgsqlConnection(configuration.GetConnectionString("DefaultConnection"));
            //DbProviderFactory dbFactory = DbProviderFactories.GetFactory(connection);
            //using (var cmd = dbFactory.CreateCommand())
            using (var cmd = new NpgsqlCommand(sqlQuery, connection))
            {
                cmd.Connection = connection;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                if (parameters != null)
                {
                    foreach (var item in parameters)
                    {
                        cmd.Parameters.AddWithValue(item.Key, item.Value);
                    }
                }
                using (NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(cmd))
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(dataTable);
                }
            }
            return dataTable;
        }
    }
}
