﻿using Core.Exceptions.Database;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace DALDocument.ExceptionConcerter
{
    public static class ExceptionConverter
    {
        public static DbError ToCustomDbError(this SqlError sqlError)
        {
            return new DbError
            {
                Class = sqlError.Class,
                LineNumber = sqlError.LineNumber,
                Message = sqlError.Message,
                Number = sqlError.Number,
                Procedure = sqlError.Procedure,
                Server = sqlError.Server,
                Source = sqlError.Source,
                State = sqlError.State,
            };
        }

        public static DbException ToCustomDbException(this SqlException sqlException)
        {
            var result = new DbException();
            result.Errors = new List<DbError>();
            foreach(SqlError error in sqlException.Errors)
            {
                result.Errors.Add(error.ToCustomDbError());
            }
            return result;
        }
    }
}
