﻿using Core.Abstraction;
using Core.Exceptions.Database;
using Core.Services.BaseCrudService;
using DALDocument.Abstraction;
using DALDocument.EF.Context;
using DALDocument.ExceptionConcerter;
using LayerTransmitter.Wrapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AppDocument.General;
using DALDocument.EF.Entities;
using LinqToDB;
using LinqToDB.EntityFrameworkCore;
using LinqToDB.Mapping;

namespace DALDocument.Base
{
    public abstract class BaseTreeTableCrudRepository<
            DomainDto,
            KeyType,
            ParentKeyType,
            Entity
            > :
        BaseEfCrudRepository<
            DomainDto,
            KeyType,
            Entity> 
        where Entity : class, IEntityWithIdParent<KeyType, ParentKeyType> 
        where DomainDto : IDtoWithId<KeyType>
    {

        protected BaseTreeTableCrudRepository(ApplicationContext context) : base(context)
        {
        }

        private IDataContext _linq2dbContext;
        protected IDataContext linq2dbContext
        {
            get { return _linq2dbContext ??= context.CreateLinqToDbContext(); }
        }


        protected virtual IQueryable<KeyType> GetIdsByParentIdQuery(ParentKeyType id)
        {
            var query = linq2dbContext.GetCte<KeyType>(employeeHierarchy =>
            {
                return
                    (
                        from e in context.Set<Entity>().AsNoTracking()
                        where id.Equals(e.idParent)
                        select e.id
                    )
                    .Concat
                    (
                        from e in context.Set<Entity>().AsNoTracking()
                        from eh in employeeHierarchy
                            .InnerJoin(eh => eh.Equals(e.idParent))
                        select e.id
                    );
            });
            return query;
        }

        protected virtual IQueryable<Entity> GetByParentIdQuery(ParentKeyType id)
        {
            var query = linq2dbContext.GetCte<Entity>(employeeHierarchy =>
            {
                return
                    (
                        from e in context.Set<Entity>().AsNoTracking()
                        where id.Equals(e.idParent)
                        select e
                    )
                    .Concat
                    (
                        from e in context.Set<Entity>().AsNoTracking()
                        from eh in employeeHierarchy
                            .InnerJoin(eh => eh.id.Equals(e.idParent))
                        select e
                    );
            });
            return query;
        }


        protected virtual IQueryable<SampleCteTemp<KeyType>> GetByParentIdQuery2(ParentKeyType id)
        {
            var query = linq2dbContext.GetCte<SampleCteTemp<KeyType>>(employeeHierarchy =>
            {
                return
                    (
                        from e in context.Set<Entity>().AsNoTracking()
                        where id.Equals(e.idParent)
                        select new SampleCteTemp<KeyType> {id = e.id, level = 0}
                    )
                    .Concat
                    (
                        from e in context.Set<Entity>().AsNoTracking()
                        from eh in employeeHierarchy
                            .InnerJoin(eh => eh.id.Equals(e.idParent))
                        select new SampleCteTemp<KeyType> { id = e.id, level = eh.level + 1}
                    );
            });
            return query;
        }

        [Table]
        protected class SampleCteTemp<KeyType>
        {
            [Column] public KeyType id { get; set; }
            [Column] public int level { get; set; }
            [Column] public string path { get; set; }
        }
    }
}


