﻿using Core.Abstraction;
using DemoEfRepo.Abstraction;
using DemoEfRepo.EF.Context;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using Core.DtoWrapperErrors;
using DemoApp;
using DemoApp.Base;
using DemoEfRepo.ExceptionConcerter;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using DemoEfRepo.EF.Entities;

namespace DemoEfRepo.Base
{
    public abstract class BaseDictionaryTranslatedEfRepository<
            DomainDto,
            AddDto,
            UpdateDto,
            GetDto,
            Entity,
            TranslatedEntity> :
        BaseTranslatedEfRepository<
            DomainDto,
            AddDto,
            UpdateDto,
            GetDto,
            Entity,
            DictionaryTranslation
        >
            where GetDto : BaseDictionaryDto<int>, new()
            where AddDto : BaseDictionaryDto<int>
            where DomainDto : BaseDictionaryDto<int>, new()
            where UpdateDto : BaseDictionaryDto<int>
            where Entity : class, IEntityWithId<int>, IDictionaryEntity, new()
    {
        //protected virtual EntityTypes EntityType { get; }

        private readonly ILanguageProvider _languageProvider;
        private readonly ISystemSettingProvider _systemSettingProvider;
        public BaseDictionaryTranslatedEfRepository(ApplicationContext context,
            ILanguageProvider languageProvider, ISystemSettingProvider systemSettingProvider) : 
            base(context, languageProvider, systemSettingProvider)
        {
            _languageProvider = languageProvider;
            _systemSettingProvider = systemSettingProvider;
        }

        public override DtoWrapper<int> Add(AddDto dto)
        {
            Func<AddDto, Entity> entitySelector = (AddDto x) => new Entity
            {
                id = dto.Id,
                name = dto.Name,
                code = dto.Code,
                description = dto.Description,
                queueNumber = dto.QueueNumber
            };

            Func<AddDto, int, DictionaryTranslation> translateSelector = (AddDto x, int id) => new DictionaryTranslation
            {
                idEntity = id,
                description = x.Description,
                idLanguage = _languageProvider.LanguageId,
                idEntityType = (int)EntityType,
                name = x.Name,
                queueNumber = x.QueueNumber
            };

            return base.Add(dto, entitySelector, translateSelector);
        }

        public override DtoWrapper<int> Update(UpdateDto dto)
        {
            Action<UpdateDto, Entity> entityValueFieldFiller = (UpdateDto u, Entity e) =>
            {
                e.code = u.Code;
            };

            Action<UpdateDto, Entity> entityTranslatedFieldFiller = (UpdateDto u, Entity e) =>
            {
                e.name = u.Name;
                e.description = u.Description;
                e.queueNumber = u.QueueNumber;
            };

            Expression<Func<DictionaryTranslation, bool>> translateDbSelector = x =>
                    x.idEntity == dto.Id && x.idEntityType == (int)EntityType &&
                    x.idLanguage == _languageProvider.LanguageId;

            Action<UpdateDto, DictionaryTranslation> translateFiller = (UpdateDto u, DictionaryTranslation t) =>
            {
                t.name = u.Name;
                t.description = u.Description;
                t.queueNumber = u.QueueNumber;
            };

            Func<UpdateDto, DictionaryTranslation> translateSelector = (UpdateDto u) => new DictionaryTranslation
            {
                idEntity = u.Id,
                description = u.Description,
                idLanguage = _languageProvider.LanguageId,
                idEntityType = (int)EntityType,
                name = u.Name,
                queueNumber = u.QueueNumber
            };

            return base.Update(dto, entityValueFieldFiller, entityTranslatedFieldFiller, translateDbSelector, translateFiller, translateSelector);
        }

        public override DtoWrapper<List<GetDto>> GetAll()
        {
            return GetAll(x => x.ToDto<GetDto>());
        }

        public override DtoWrapper<GetDto> GetOneByKey(int id)
        {
            return GetOneByKey(id, x => x.ToDto<GetDto>());
        }

        protected override DtoWrapper<List<GetDto>> GetAll(Expression<Func<Entity, GetDto>> selector)
        {
            try
            {
                Expression<Func<Entity, bool>> filter = (Entity e) => true;
                var xy = GetTranslatedEntities(filter, selector).ToList();
                return DtoWrapper<List<GetDto>>.Ok(xy);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        protected override DtoWrapper<GetDto> GetOneByKey(int id, Expression<Func<Entity, GetDto>> selector)
        {
            try
            {
                Expression<Func<Entity, bool>> filter = (Entity e) => e.id == id;
                var xy = GetTranslatedEntities(filter, selector).FirstOrDefault();
                return DtoWrapper<GetDto>.Ok(xy);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private IQueryable<GetDto> GetTranslatedEntities(Expression<Func<Entity, bool>> filter, Expression<Func<Entity, GetDto>> selector)
        {
            int ideEntityType = (int)EntityType;

            var filtered = context.Set<Entity>().Where(filter);

            return (
                        from entity in filtered

                            #region left join trranslate
                        join trsl in (
                                from dt in context.DictionaryTranslations
                                where dt.idLanguage == _languageProvider.LanguageId && dt.idEntityType == ideEntityType
                                select dt)
                            on entity.id equals trsl.idEntity into join1
                        from translate in join1.DefaultIfEmpty()
                            #endregion left join 

                        select new Entity
                        {
                            #region Value fields
                            id = entity.id,
                            code = entity.code,
                            #endregion Value fields

                            #region Localized Fields
                            name = translate == null ? entity.name : translate.name,
                            description = translate == null ? entity.description : translate.description,
                            queueNumber = translate == null ? entity.queueNumber : translate.queueNumber
                            #endregion Localized Fields
                        })
                    .Select(selector);
        }

        public List<DomainDto> FilterByCode(string code)
        {
            return context.Set<Entity>().AsNoTracking().Where(x => x.code == code).Select(x => x.ToDto<DomainDto>()).ToList();
        }

        public List<DomainDto> FilterByName(string name)
        {
            return context.Set<Entity>().AsNoTracking().Where(x => x.name == name).Select(x => x.ToDto<DomainDto>()).ToList();
        }

        public int GetNewQueueNumber()
        {
            var numbers = context.Set<Entity>().AsNoTracking().Select(x => x.queueNumber ?? 0).OrderByDescending(x => x).Take(1).ToList();
            if (numbers.Count == 0)
                return 0;
            return numbers[0];
        }

        public bool HasOneWithKey(int id)
        {
            return context.Set<Entity>().AsNoTracking().Any(x => x.id == id);
        }
    }
}
