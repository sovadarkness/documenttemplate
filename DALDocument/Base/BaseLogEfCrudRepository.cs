﻿using Core.Abstraction;
using Core.DtoWrapperErrors;
using Core.Exceptions.Database;
using Core.Providers;
using Core.Services.BaseCrudService;
using DALDocument.Abstraction;
using DALDocument.EF.Context;
using DALDocument.ExceptionConcerter;
using LayerTransmitter.Wrapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace DALDocument.Base
{
    public abstract class BaseLogEfCrudRepository<
            DomainDto,
            KeyType,
            Entity
        > :
        BaseEfCrudRepository<
            DomainDto,
            KeyType,
            Entity>
        where DomainDto : ILogDtoWithId<KeyType>
        where Entity : class, ILogEntityWithId<KeyType>
    {
        private ISessionProvider _SessionProvider;


        protected BaseLogEfCrudRepository(ApplicationContext context,
            ISessionProvider sessionProvider) : base(context)
        {
            _SessionProvider = sessionProvider;

        }

        #region Add
        public override KeyType Add(DomainDto dto)
        {
            var entity = entitySelector.Invoke(dto);
            return Add(entity);
        }

        #endregion Add

        #region Update
        public override KeyType Update(DomainDto dto)
        {
            var entity = entitySelector.Invoke(dto);
            entity.lastUpdatedTime = DateTime.Now;
            entity.lastUpdatedUser = _SessionProvider.UserId;
            var item = GetOneByKey(dto.id);
            if(item.lastUpdatedTime.HasValue)
            {
                if(item.lastUpdatedTime.Value.ToString("yyyy-MM-dd HH:mm:ss") != dto.strlastUpdatedTime)
                {
                    //return DtoWrapper.Exception(new UpdatedError());
                    //throw new DtoWrapper.Exception(iExecutingException);
                    throw new UpdatedError();
                    //throw new Exception("error"); //TODO wtf exception
                }
            }
            return Update(dto, entity);
        }

        #endregion Update

    }
}


