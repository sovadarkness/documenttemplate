﻿using Core.Abstraction;
using Core.Exceptions.Database;
using Core.Services.BaseCrudService;
using DALDocument.Abstraction;
using DALDocument.EF.Context;
using DALDocument.ExceptionConcerter;
using LayerTransmitter.Wrapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DALDocument.Base
{
    public abstract class BaseEfCrudRepository<
            DomainDto,
            KeyType,
            Entity
        > :
        IBaseCrudRepository<
            DomainDto,
            KeyType>
        where DomainDto : IDtoWithId<KeyType>
        where Entity : class, IEntityWithId<KeyType>
    {
        protected ApplicationContext context;

        protected abstract Func<DomainDto, Entity> entitySelector { get; }
        protected abstract Expression<Func<DomainDto, Entity>> entitySelectorExpr { get; }
        protected abstract Func<Entity, DomainDto> dtoSelector { get; }
        protected abstract Expression<Func<Entity, DomainDto>> dtoSelectorExpr { get; }
        protected abstract Expression<Func<Entity, bool>> filterSameItemsExpr(DomainDto dto);

        protected abstract IQueryable<Entity> TheWholeEntities { get; }

        protected BaseEfCrudRepository(ApplicationContext context)
        {
            this.context = context;
        }

        #region Add
        public virtual KeyType Add(DomainDto dto)
        {
            var entity = entitySelector.Invoke(dto);
            return Add(entity);
        }

        protected KeyType Add(Entity entity)
        {
            try
            {
                context.Set<Entity>().Add(entity);
                context.SaveChanges();
                return entity.id;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }
        #endregion Add

        #region Update
        public virtual KeyType Update(DomainDto dto)
        {
            var entity = entitySelector.Invoke(dto);
            return Update(dto, entity);
        }
        protected KeyType Update(DomainDto dto, Entity entity)
        {
            try
            {
                var item = context.Set<Entity>().FirstOrDefault(x => x.id.Equals(dto.id));
                context.Entry(item).CurrentValues.SetValues(entity);
                context.SaveChanges();
                return dto.id;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }
        #endregion Update

        #region Delete 
        public void Remove(KeyType key)
        {
            try
            {
                var item = context.Set<Entity>().AsNoTracking().FirstOrDefault(x => x.id.Equals(key));
                context.Set<Entity>().Remove(item);
                context.SaveChanges();
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
            catch (Exception e)
            {
                if (e.InnerException is SqlException)
                {
                    if (((SqlException) e.InnerException).Message.Contains("DELETE"))
                    {
                        throw new DeleteException();
                    }
                    throw ((SqlException)e.InnerException).ToCustomDbException();
                }
                throw;
            }
        }
        #endregion

        #region Get All
        public virtual List<DomainDto> GetAll()
        {
            try
            {
                var items = TheWholeEntities
                    .OrderByDescending(x=>x.id)
                    .Select(dtoSelectorExpr)
                    .ToList();
                return items;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        #endregion Get All

        #region Get One 
        public virtual DomainDto GetOneByKey(KeyType id)
        {
            try
            {
                var item = TheWholeEntities
                    .Where(x => x.id.Equals(id))
                    .Select(dtoSelectorExpr)
                    .FirstOrDefault();
                return item;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        #endregion Get One

        #region Get Same Items
        public virtual List<DomainDto> GetSameItems(DomainDto dto)
        {
            return GetByFilterDto(filterSameItemsExpr, dto);
        }

        #endregion Get Same Items


        #region Get by filterDto
        public virtual List<DomainDto> GetByFilterDto(Func<DomainDto, Expression<Func<Entity, bool>>> filter, DomainDto dto)
        {
            //usage1 -> GetByFilterDto(filterSameItemsExpr, dto);
            //usage2 (same as GetByFilter) -> GetByFilterDto(x => (entity) => entity.idSex == id, null);
            try
            {
                var items = TheWholeEntities
                    .Where(filter(dto))
                    .Select(dtoSelectorExpr)
                    .ToList();
                return items;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        #endregion Get by filterDto

        #region Get by filter
        public virtual List<DomainDto> GetByFilter(Expression<Func<Entity, bool>> filter)
        {
            try
            {
                var items = TheWholeEntities
                    .Where(filter)
                    .OrderByDescending(x => x.id)
                    .Select(dtoSelectorExpr)
                    .ToList();
                return items;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        #endregion Get by filter


        public virtual bool CheckCanBeRemoved(KeyType key)
        {
            return true;
        }


    }
}


