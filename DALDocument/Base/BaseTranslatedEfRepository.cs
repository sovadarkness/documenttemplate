﻿using Core.Abstraction;
using DALDocument.Abstraction;
using DALDocument.EF.Context;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using Core.DtoWrapperErrors;
using Microsoft.Data.SqlClient;
using DALDocument.ExceptionConcerter;
using Microsoft.EntityFrameworkCore;
using AppDocument;
using DALDocument.EF.Entities;

namespace DALDocument.Base
{
    public abstract class BaseTranslatedEfRepository<
            Dto,
            Entity,
            TranslatedEntity> : BaseEfCrudRepository<
            Dto,
            int,
            Entity
        >
            where Dto : IDtoWithId<int>
            where Entity : class, IEntityWithId<int>, new()
            where TranslatedEntity : class, ITranslatedEntityWithId<int>, new()
    {
        protected readonly ILanguageProvider _languageProvider;
        protected readonly ISystemSettingProvider _systemSettingProvider;
        public BaseTranslatedEfRepository(ApplicationContext context,
            ILanguageProvider languageProvider,
            ISystemSettingProvider systemSettingProvider) : base(context)
        {
            _languageProvider = languageProvider;
            _systemSettingProvider = systemSettingProvider;
        }


        protected abstract Func<Entity, TranslatedEntity> translateSelector { get; }
        protected abstract Func<Entity, Entity> translateEntity { get; }
        protected abstract Action<Dto, Entity> entityValueFieldFiller { get; }
        protected abstract Action<Dto, Entity> entityTranslatedFieldFiller { get; }


        public override int Add(Dto dto)
        {
            try
            {
                var entity = entitySelector.Invoke(dto);
                context.Set<Entity>().Add(entity);
                context.SaveChanges();

                var translatedItem = translateSelector.Invoke(entity);
                context.Set<TranslatedEntity>().Add(translatedItem);
                context.SaveChanges();

                return entity.id;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        public override int Update(Dto dto)
        {
            try
            {
                var entity = entitySelector.Invoke(dto);
                var item = context.Set<Entity>().FirstOrDefault(x => x.id.Equals(dto.id));

                var translated = translateSelector.Invoke(entity);
                entityValueFieldFiller.Invoke(dto, item);

                var currentLang = _languageProvider.LanguageId;
                if (currentLang == _systemSettingProvider.DefaultLanguageId)
                {
                    entityTranslatedFieldFiller.Invoke(dto, item);
                }

                var trEntity = context.Set<TranslatedEntity>().FirstOrDefault(x => x.id.Equals(dto.id) && x.idLanguage == currentLang);
                if (trEntity == null)
                    context.Set<TranslatedEntity>().Add(translated);
                else
                    context.Entry(trEntity).CurrentValues.SetValues(translated);

                context.SaveChanges();
                return dto.id;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        //protected virtual int Update(Dto dto,
        //    Action<Dto, Entity> entityValueFieldFiller,
        //    Action<Dto, Entity> entityTranslatedFieldFiller,
        //    Expression<Func<TranslatedEntity, bool>> translateDbSelector,
        //    Action<Dto, TranslatedEntity> translateFiller,
        //    Func<Dto, TranslatedEntity> translateSelector
        //    )
        //{
        //    try
        //    {
        //        var item = context.Set<Entity>().FirstOrDefault(x => x.id.Equals(dto.Id));
        //        if (item == null) throw new Exception();

        //        entityValueFieldFiller.Invoke(dto, item);

        //        if (_languageProvider.LanguageId == _systemSettingProvider.DefaultLanguageId)
        //        {
        //            entityTranslatedFieldFiller.Invoke(dto, item);
        //        }

        //        var translate = context.Set<TranslatedEntity>().FirstOrDefault(translateDbSelector);

        //        if (translate != null)
        //        {
        //            translateFiller.Invoke(dto, translate);
        //        }
        //        else
        //        {
        //            translate = translateSelector.Invoke(dto);
        //            context.Set<TranslatedEntity>().Add(translate);
        //        }
        //        context.SaveChanges();

        //        return dto.Id;
        //    }
        //    catch (SqlException sqlException)
        //    {
        //        throw sqlException.ToCustomDbException();
        //    }
        //}



        public override List<Dto> GetAll()
        {
            try
            {
                var items = TheWholeEntities
                    .Select(x => translateEntity.Invoke(x))
                    .Select(dtoSelectorExpr)
                    .ToList();
                return items;
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }

        public override Dto GetOneByKey(int id)
        {
            try
            {
                var item = TheWholeEntities.FirstOrDefault(x => x.id == id);
                var translated = translateEntity.Invoke(item);
                return dtoSelector.Invoke(translated);
            }
            catch (SqlException sqlException)
            {
                throw sqlException.ToCustomDbException();
            }
        }


    }
}
