﻿using DALDocument.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace DALDocument.Base
{
    public class DictionaryEntity: IDictionaryEntity
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int? queueNumber { get; set; }
    }
}
