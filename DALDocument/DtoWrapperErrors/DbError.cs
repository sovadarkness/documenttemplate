﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Exceptions.Database
{
    public class DbError
    {
        public byte Class { get; set; }
        public int LineNumber { get; set; }
        public string Message { get; set; }
        public int Number { get; set; }
        public string Procedure { get; set; }
        public string Server { get; set; }
        public string Source { get; set; }
        public byte State { get; set; }
    }
}
