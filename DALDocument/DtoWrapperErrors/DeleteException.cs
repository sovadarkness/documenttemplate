﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Exceptions.Database
{
    public class DeleteException : Exception, IExecutionException
    {
        public List<DbError> Errors { get; set; }

        public int ErrorCode => (int)ExecutionExceptions.DeleteError;

        public string ErrorType => ErrorTypes.Execution;
    }
}
