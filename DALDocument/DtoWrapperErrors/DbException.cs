﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Exceptions.Database
{
    public class DbException : Exception, IExecutionException
    {
        public List<DbError> Errors { get; set; }

        public int ErrorCode => (int)ExecutionExceptions.DatabaseError;

        public string ErrorType => ErrorTypes.Execution;
    }
}
