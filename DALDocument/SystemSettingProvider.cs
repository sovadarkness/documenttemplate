﻿using AppDocument;
using DALDocument.EF.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace DALDocument
{
    public class SystemSettingProvider : ISystemSettingProvider
    {
        private readonly ApplicationContext _applicationContext;
        private readonly IConfiguration _configuration;
        public SystemSettingProvider(ApplicationContext applicationContext, IConfiguration configuration)
        {
            _applicationContext = applicationContext;
            _configuration = configuration;
        }

        public int DefaultLanguageId => 1;
        //_applicationContext.SystemSettings
        //    .Where(x => x.code == "DefaultLanguage")
        //    .Select(x=>x.value)
        //    .Select(int.Parse)
        //    .Single();

        public int ImageFileTypeId => 1;
        //_applicationContext.SystemSettings
        //    .Where(x => x.code == "ImageFileType")
        //    .Select(x => x.value)
        //    .Select(int.Parse)
        //    .Single();



        public Dictionary<string, int> SystemLanguages => new Dictionary<string, int> { { "ru-RU", 1 } };


        private string[] ClientSettingsCodes = new[] { "HideDictionaryCode", "HideSystemDictionaryCRUD" };

        public Dictionary<string, object> SettingsOnClient => throw new NotImplementedException();
            //_applicationContext.SystemSettings
            //    .Where(x => ClientSettingsCodes.Contains(x.code))
            //    .ToDictionary(x => x.code, x => (object)x.value);

        public Dictionary<string, int> WeekDay => new Dictionary<string, int>
        {
            { "Monday", 1 },
            { "Tuesday", 2 },
            { "Wednesday", 3 },
            { "Thursday", 4 },
            { "Friday", 5 },
            { "Saturday", 6 },
            { "Sunday", 7 }
        };

        public string QRServiseModuleApi => _configuration.GetSection("QRServise").Value;
        public string DoctorFront => _configuration.GetSection("DoctorFront").Value;
        public string IdentityModuleApi => _configuration.GetSection("Identity").GetSection("Api").Value;
        public string IdentityModuleToken => _configuration.GetSection("Identity").GetSection("Oauth2_Token").Value;

        //_applicationContext.SystemLanguages.ToDictionary(x => x.code, x => x.id);

        public string ScheduledVisitStatus => _configuration.GetSection("Dictionary").GetSection("VisitStatus").GetSection("Scheduled").Value;
        public string StartedVisitStatus => _configuration.GetSection("Dictionary").GetSection("VisitStatus").GetSection("Started").Value;
        public string CanceledVisitStatus => _configuration.GetSection("Dictionary").GetSection("VisitStatus").GetSection("Canceled").Value;
        public string SuspendedVisitStatus => _configuration.GetSection("Dictionary").GetSection("VisitStatus").GetSection("Suspended").Value;
        public string FinishedVisitStatus => _configuration.GetSection("Dictionary").GetSection("VisitStatus").GetSection("Finished").Value;
        public string ReceptionVisitType => _configuration.GetSection("Dictionary").GetSection("VisitType").GetSection("Reception").Value;
        public string LiveQueueVisitType => _configuration.GetSection("Dictionary").GetSection("VisitType").GetSection("LiveQueue").Value;
    }
}
