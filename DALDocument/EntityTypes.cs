﻿namespace DALDocument
{
    public enum EntityTypes
    {
        Post = 1,
        Sex = 2,
        DictionaryTranslation = 3,
        SystemLanguage = 4,
        Language = 5,
        AppModule = 6,
        DocumentType = 7,
        TypeMessage = 8,
        TypeContact = 9,
        ApplicationRole = 10,
        EntityType = 11,
        NeededLanguage = 23,

    }
}
