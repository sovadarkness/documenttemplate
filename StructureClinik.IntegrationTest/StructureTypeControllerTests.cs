using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AppDocument.General;
using AppDocument.MedicalService;
using AppDocument.State;
using AppDocument.StructureClinik;
using BackendLogin.IntegrationTests;
using DALDocument.EF.Context;
using DALDocument.EF.Entities;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using PLDocument;

namespace StructureClinik.IntegrationTest
{
    [TestFixture]
    public class StructureTypeControllerTests : BaseMemoryTest
    {
        //��� ��� ���������
        private int StructureTypeId_Clinic = 0;
        private int StructureTypeId_Mainfilial = 0;
        private int StructureTypeId_Branch = 0;
        private int StructureTypeId_Cabinet = 0;

        //������� ��� ���������
        private int StructureId_Clinic = 0;

        private int StructureId_Mainfilial = 0;

        private int StrutureId_LOR = 0;
        private int StrutureId_Ginecology = 0;
        private int StrutureId_Pediatr = 0;
        private int StrutureId_Therapy = 0;
        private int StrutureId_Traumatology = 0;
        private int StrutureId_Neuralgia = 0;
        private int StrutureId_Physiotherapy = 0;
        private int StrutureId_Laboratory = 0;
        private int StrutureId_Exercisetherapy = 0;
        private int StrutureId_Massage = 0;
        private int StrutureId_Dermatologist = 0;

        private int StrutureId_CabinetNeuralgia_1 = 0;
        private int StrutureId_CabinetNeuralgia_2 = 0;
        private int StrutureId_CabinetNeuralgia_3 = 0;

        private int RoomId_Clinic = 0;
        private int RoomId_Mainfilial = 0;
        private int RoomId_BranchNeuro = 0;
        private int RoomId_CabinetNeuralgia_1 = 0;
        private int RoomId_CabinetNeuralgia_2 = 0;
        private int RoomId_CabinetNeuralgia_3 = 0;


        private int RoomToStructureId_Clinic = 0;
        private int RoomToStructureId_Mainfilial = 0;
        private int RoomToStructureId_BranchNeuro = 0;
        private int RoomToStructureId_CabinetNeuralgia_1 = 0;
        private int RoomToStructureId_CabinetNeuralgia_2 = 0;
        private int RoomToStructureId_CabinetNeuralgia_3 = 0;

        //�� ������
        private int LegalStatusId_Clinic = 0;




        [Test]
        public async Task Scenario1()
        {
            //�����������
            var admin = await loginWithUser("admin", "Pa$$word123");

            #region  ���������� StructureTypeDto
            //���������� ���� �������
            var structureType�linic = new StructureTypeDto()
            {
                id = 0,
                name = "�������",
                description = "������� - ",
                code = "Clinic"
            };
            StructureTypeId_Clinic = await AddStructureType(admin, structureType�linic);
            Assert.IsTrue(StructureTypeId_Clinic > 0);

            //���������� ���� �������
            var structureTypeDepartment = new StructureTypeDto()
            {
                id = 0,
                name = "������",
                description = "������ - ",
                code = "Department"
            };
            StructureTypeId_Mainfilial = await AddStructureType(admin, structureTypeDepartment);
            Assert.IsTrue(StructureTypeId_Mainfilial > 0);

            //���������� ���� ��������
            var structureTypeCabinet = new StructureTypeDto()
            {
                id = 0,
                name = "�������",
                description = "������� - ",
                code = "Cabinet"
            };
            StructureTypeId_Cabinet = await AddStructureType(admin, structureTypeCabinet);
            Assert.IsTrue(StructureTypeId_Cabinet > 0);
            #endregion ���������� StructureTypeDto

            //������
            //���������� ������������ �������
            var legalStatus�linic = new LegalStatusDto()
            {
                id = 0,
                name = "����",
                description = "�������� � ������������ ����������������",
                code = "LLC"
            };
            LegalStatusId_Clinic = await AddLegalStatus(admin, legalStatus�linic);
            Assert.IsTrue(LegalStatusId_Clinic > 0);

            //�������� �������
            var structure�linic = new StructureDto()
            {
                id = 0,
                fullName = "���� ��������-����������",
                shortName = "��������-����������",
                description = "��������-���������� �������",
                licenseNumber = "564654669862363",
                siteUrl = "404.com",
                //requisites = "[���������]"
            };
            StructureId_Clinic = await AddClinic(admin, structure�linic);
            Assert.IsTrue(StructureId_Clinic > 0);

            //�������� �������
            var structureDepartment = new StructureDto()
            {
                fullName = "��������-���������� ������ - 1",
                shortName = "������ - 1",
                description = "��������-���������� �������",
                licenseNumber = "564654669862363",
                siteUrl = "404-dep1.com",
                //requisites = "[���������]"
            };
            StructureId_Mainfilial = await AddDepartment(admin, structure�linic);
            Assert.IsTrue(StructureId_Mainfilial > 0);


            //���������� ���� ���������
            var structureTypeBranch = new StructureTypeDto()
            {
                id = 0,
                name = "���������",
                description = "��������� - ",
                code = "Branch"
            };
            StructureTypeId_Branch = await AddStructureType(admin, structureTypeBranch);
            Assert.IsTrue(StructureTypeId_Branch > 0);

            //�������� ��������� �������������������
            var structureBranchLOR = new StructureDto()
            {
                fullName = "�������������������",
                shortName = "���",
                description = "������������������� - ",
                siteUrl = "404-lor.com",
            };
            StrutureId_LOR = await AddBranch(admin, structureBranchLOR);
            Assert.IsTrue(StrutureId_LOR > 0);

            //�������� ��������� ����������-�����������
            var structureBranchGinecology = new StructureDto()
            {
                fullName = "����������-�����������",
                shortName = "�����������",
                description = "����������-����������� - ",
                siteUrl = "404-ginecology.com",
            };
            StrutureId_Ginecology = await AddBranch(admin, structureBranchGinecology);
            Assert.IsTrue(StrutureId_Ginecology > 0);

            //�������� ��������� ���������
            var structureBranchPediatr = new StructureDto()
            {
                fullName = "���������",
                shortName = "���������",
                description = "��������� - ",
                siteUrl = "404-pediatr.com",
            };
            StrutureId_Pediatr = await AddBranch(admin, structureBranchPediatr);
            Assert.IsTrue(StrutureId_Pediatr > 0);

            //�������� ��������� ����� �������
            var structureBranchTherapy = new StructureDto()
            {
                fullName = "����� �������",
                shortName = "�������",
                description = "����� ������� - ",
                siteUrl = "404-therapy.com",
            };
            StrutureId_Therapy = await AddBranch(admin, structureBranchTherapy);
            Assert.IsTrue(StrutureId_Therapy > 0);

            //�������� ��������� �������������
            var structureBranchTraumatology = new StructureDto()
            {
                fullName = "�������������",
                shortName = "����",
                description = "������������� - ",
                siteUrl = "404-traumatology.com",
            };
            StrutureId_Traumatology = await AddBranch(admin, structureBranchTraumatology);
            Assert.IsTrue(StrutureId_Traumatology > 0);

            //�������� ��������� ���������
            var structureBranchNeuralgia = new StructureDto()
            {
                fullName = "���������",
                shortName = "���������",
                description = "��������� - ",
                siteUrl = "404-neuralgia.com",
            };
            StrutureId_Neuralgia = await AddBranch(admin, structureBranchNeuralgia);
            Assert.IsTrue(StrutureId_Neuralgia > 0);

            //�������� ��������� ������������
            var structureBranchPhysiotherapy = new StructureDto()
            {
                fullName = "������������",
                shortName = "������������",
                description = "������������ - ",
                siteUrl = "404-physiotherapy.com",
            };
            StrutureId_Physiotherapy = await AddBranch(admin, structureBranchPhysiotherapy);
            Assert.IsTrue(StrutureId_Physiotherapy > 0);

            //�������� ��������� �����������
            var structureBranchLaboratory = new StructureDto()
            {
                fullName = "�����������",
                shortName = "�����������",
                description = "����������� - ",
                siteUrl = "404-Laboratory.com",
            };
            StrutureId_Laboratory = await AddBranch(admin, structureBranchLaboratory);
            Assert.IsTrue(StrutureId_Laboratory > 0);

            //�������� ��������� ���
            var structureBranchExercisetherapy = new StructureDto()
            {
                fullName = "���",
                shortName = "���",
                description = "��� - ",
                siteUrl = "404-Exercisetherapy.com",
            };
            StrutureId_Exercisetherapy = await AddBranch(admin, structureBranchExercisetherapy);
            Assert.IsTrue(StrutureId_Exercisetherapy > 0);

            //�������� ��������� ������
            var structureBranchMassage = new StructureDto()
            {
                fullName = "������",
                shortName = "������",
                description = "������ - ",
                siteUrl = "404-Massage.com",
            };
            StrutureId_Massage = await AddBranch(admin, structureBranchMassage);
            Assert.IsTrue(StrutureId_Massage > 0);

            //�������� ��������� ����������
            var structureBranchDermatologist = new StructureDto()
            {
                fullName = "����������",
                shortName = "����������",
                description = "���������� - ",
                siteUrl = "404-Dermatologist .com",
            };
            StrutureId_Dermatologist = await AddBranch(admin, structureBranchDermatologist);
            Assert.IsTrue(StrutureId_Dermatologist > 0);

            //�������� ��������� ��� ��������� ���������
            var structureCabinetNeuralgia_1 = new StructureDto()
            {
                fullName = "������� ���������� 1",
                shortName = "������� 1",
                idParent = StrutureId_Neuralgia
            };
            StrutureId_CabinetNeuralgia_1 = await AddCabinet(admin, structureCabinetNeuralgia_1);
            Assert.IsTrue(StrutureId_CabinetNeuralgia_1 > 0);

            var structureCabinetNeuralgia_2 = new StructureDto()
            {
                fullName = "������� ���������� 2",
                shortName = "������� 2",
                idParent = StrutureId_Neuralgia
            };
            StrutureId_CabinetNeuralgia_2 = await AddCabinet(admin, structureCabinetNeuralgia_2);
            Assert.IsTrue(StrutureId_CabinetNeuralgia_2 > 0);

            var structureCabinetNeuralgia_3 = new StructureDto()
            {
                fullName = "������� ���������� 3",
                shortName = "������� 3",
                idParent = StrutureId_Neuralgia
            };
            StrutureId_CabinetNeuralgia_3 = await AddCabinet(admin, structureCabinetNeuralgia_3);
            Assert.IsTrue(StrutureId_CabinetNeuralgia_3 > 0);

            //��������� ��� �������
            var roomClinic = new RoomDto()
            {
                address = "�. ������, ��. ������� �����, 123",
            };
            RoomId_Clinic = await AddRoom(admin, roomClinic);
            Assert.IsTrue(RoomId_Clinic > 0);

            //��������� ��� �������
            var roomMainfilial = new RoomDto()
            {
                idParent = RoomId_Clinic
            };
            RoomId_Mainfilial = await AddRoom(admin, roomMainfilial);
            Assert.IsTrue(RoomId_Mainfilial > 0);

            //��������� ��� ���������
            var roomBranch = new RoomDto()
            {
                idParent = RoomId_Mainfilial
            };
            RoomId_BranchNeuro = await AddRoom(admin, roomBranch);
            Assert.IsTrue(RoomId_BranchNeuro > 0);

            //��������� ��� �������� ��������� ����� 1
            var roomCabinetNeuro1 = new RoomDto()
            {
                idParent = RoomId_BranchNeuro
            };
            RoomId_CabinetNeuralgia_1 = await AddRoom(admin, roomCabinetNeuro1);
            Assert.IsTrue(RoomId_CabinetNeuralgia_1 > 0);

            //��������� ��� �������� ��������� ����� 2
            var roomCabinetNeuro2 = new RoomDto()
            {
                idParent = RoomId_BranchNeuro
            };
            RoomId_CabinetNeuralgia_2 = await AddRoom(admin, roomCabinetNeuro2);
            Assert.IsTrue(RoomId_CabinetNeuralgia_2 > 0);

            //��������� ��� �������� ��������� ����� 3
            var roomCabinetNeuro3 = new RoomDto()
            {
                idParent = RoomId_BranchNeuro
            };
            RoomId_CabinetNeuralgia_3 = await AddRoom(admin, roomCabinetNeuro3);
            Assert.IsTrue(RoomId_CabinetNeuralgia_3 > 0);

            //���������� ��� �������
            var roomToStructureClinic = new RoomToStructureDto()
            {
                idRoom = RoomId_Clinic,
                idStructure = StructureId_Clinic
            };
            RoomToStructureId_Clinic = await AddToRoom(admin, roomToStructureClinic);
            Assert.IsTrue(RoomToStructureId_Clinic > 0);

            //���������� ��� �������
            var roomToStructureMainfilial = new RoomToStructureDto()
            {
                idRoom = RoomId_Mainfilial,
                idStructure = StructureId_Mainfilial
            };
            RoomToStructureId_Mainfilial = await AddToRoom(admin, roomToStructureMainfilial);
            Assert.IsTrue(RoomToStructureId_Mainfilial > 0);

            //���������� ��� ���������
            var roomToStructureBranch = new RoomToStructureDto()
            {
                idRoom = RoomId_BranchNeuro,
                idStructure = StrutureId_Neuralgia
            };
            RoomToStructureId_BranchNeuro = await AddToRoom(admin, roomToStructureBranch);
            Assert.IsTrue(RoomToStructureId_BranchNeuro > 0);

            //��������� ��� �������� ��������� ����� 1
            var roomToStructureCabinetNeuro1 = new RoomToStructureDto()
            {
                idRoom = RoomId_CabinetNeuralgia_1,
                idStructure = StrutureId_CabinetNeuralgia_1
            };
            RoomToStructureId_CabinetNeuralgia_1 = await AddToRoom(admin, roomToStructureCabinetNeuro1);
            Assert.IsTrue(RoomToStructureId_CabinetNeuralgia_1 > 0);

            //��������� ��� �������� ��������� ����� 2
            var roomToStructureCabinetNeuro2 = new RoomToStructureDto()
            {
                idRoom = RoomId_CabinetNeuralgia_1,
                idStructure = StrutureId_CabinetNeuralgia_1
            };
            RoomToStructureId_CabinetNeuralgia_2 = await AddToRoom(admin, roomToStructureCabinetNeuro2);
            Assert.IsTrue(RoomToStructureId_CabinetNeuralgia_2 > 0);

            //��������� ��� �������� ��������� ����� 3
            var roomToStructureCabinetNeuro3 = new RoomToStructureDto()
            {
                idRoom = RoomId_CabinetNeuralgia_1,
                idStructure = StrutureId_CabinetNeuralgia_1
            };
            RoomToStructureId_CabinetNeuralgia_3 = await AddToRoom(admin, roomToStructureCabinetNeuro3);
            Assert.IsTrue(RoomToStructureId_CabinetNeuralgia_3 > 0);



























            //�������
            #region Post
            var chiefDoctor = new PostDto() {
                name = "������� ����",
                description = "������� ����",
                code = "ChiefDoctor"
            };
            PostId_ChiefDoctor = await AddPost(admin, chiefDoctor);

            var postAdmin = new PostDto()
            {
                name = "�������������",
                description = "�������������",
                code = "Admin"
            };
            PostId_Admin = await AddPost(admin, postAdmin);

            var postRegistry = new PostDto()
            {
                name = "�����������",
                description = "�����������",
                code = "Registry"
            };
            PostId_Registry = await AddPost(admin, postRegistry);

            var postAccountant = new PostDto()
            {
                name = "���������",
                description = "���������",
                code = "Accountant"
            };
            PostId_Accountant = await AddPost(admin, postAccountant);

            var postDoctor = new PostDto()
            {
                name = "����",
                description = "����",
                code = "Doctor"
            };
            PostId_Doctor = await AddPost(admin, postDoctor);
            #endregion

            #region Speciality
            var specLOR = new SpecialityDto() {
                name = "�������������������",
                description = "�������������������",
                code = "Otorhinolaryngology"
            };
            SpecialityId_LOR = await AddSpeciality(admin, specLOR);

            var specGynecology = new SpecialityDto() {
                name = "����������-�����������",
                description = "����������-�����������",
                code = "Gynecology"
            };
            SpecialityId_Gynecology = await AddSpeciality(admin, specGynecology);

            var specPediatrics = new SpecialityDto() {
                name = "���������",
                description = "���������",
                code = "Pediatrics"
            };
            SpecialityId_Pediatrics = await AddSpeciality(admin, specPediatrics);

            var specTherapy = new SpecialityDto() {
                name = "����� �������",
                description = "����� �������",
                code = "Therapy"
            };
            SpecialityId_Therapy = await AddSpeciality(admin, specTherapy);

            var specTraumatology = new SpecialityDto() {
                name = "�������������",
                description = "�������������",
                code = "Traumatology"
            };
            SpecialityId_Traumatology = await AddSpeciality(admin, specTraumatology);

            var specNeuralogy = new SpecialityDto() {
                name = "����������",
                description = "����������",
                code = "Neuralogy"
            };
            SpecialityId_Neuralogy = await AddSpeciality(admin, specNeuralogy);
            #endregion

            #region StateItem
            StateItem_Lor = await AddStateItem(admin, PostId_Doctor, StrutureId_LOR, 5);
            StateItem_Gynecology = await AddStateItem(admin, PostId_Doctor, StrutureId_Ginecology, 4);
            StateItem_Pediatrics = await AddStateItem(admin, PostId_Doctor, StrutureId_Pediatr, 6);
            StateItem_Therapy = await AddStateItem(admin, PostId_Doctor, StrutureId_Therapy, 3);
            StateItem_Physio = await AddStateItem(admin, PostId_Doctor, StrutureId_Physiotherapy, 1);
            StateItem_Lab = await AddStateItem(admin, PostId_Doctor, StrutureId_Laboratory, 1);
            StateItem_LFK = await AddStateItem(admin, PostId_Doctor, StrutureId_Exercisetherapy, 1);
            StateItem_Massage = await AddStateItem(admin, PostId_Doctor, StrutureId_Massage, 1);
            StateItem_Dermatology = await AddStateItem(admin, PostId_Doctor, StrutureId_Dermatologist, 1);
            StateItem_Traumatology = await AddStateItem(admin, PostId_Doctor, StrutureId_Traumatology, 1);
            StateItem_Neuralogy = await AddStateItem(admin, PostId_Doctor, StrutureId_Neuralgia, 1);

            #endregion

            var sex = new SexDto
            {
                name = "�������",
                code = "MALE"
            };
            Sex_Muj_Test = await AddSex(admin, sex);


            var telegramContactType = new ContactTypeDto
            {
                name = "telegram",
                code = "TELEGRAM"
            };
            ContactType_Telegram_Test = await AddContactType(admin, telegramContactType);

            var personNew = new PersonDto()
            {
                surName = "��������",
                firstName = "�������",
                secondName = "���������",
                idSex = Sex_Muj_Test,
                dob = new DateTime(1992, 1, 1)
            };
            Person_Test = await AddPerson(admin, personNew);

            //var employeeHistory = new EmployeeHistoryDto()
            //{
            //    idPerson = Person_Test,
            //    idStateItem = StateItem_Lor,
            //    dateStart = DateTime.UtcNow,
            //    dateEnd = DateTime.UtcNow
            //};
            //Employee_Test = await AddEmployee(admin, employeeHistory);


            var xxx = dbContext.StateItems.ToList();
            var persons = dbContext.People.ToList();
        }

        [Test]
        public async Task PriceList()
        {
            //Login
            var admin = await loginWithUser("admin", "Pa$$word123");
            var headDoc = await loginWithUser("head_doctor", "!12345Qq");
            var accountant = await loginWithUser("accountant", "!12345Qq");

            #region addStructures

            var structure�linic = new StructureDto()
            {
                id = 0,
                fullName = "���� ��������-����������",
                shortName = "��������-����������",
                description = "��������-���������� �������",
                licenseNumber = "564654669862363",
                siteUrl = "404.com",
                //requisites = "[���������]"
            };
            StructureId_Clinic = await AddClinic(admin, structure�linic);
            //Assert.IsTrue(StructureId_Clinic > 0);

            //�������� �������
            var structureDepartment = new StructureDto()
            {
                fullName = "��������-���������� ������ - 1",
                shortName = "������ - 1",
                description = "��������-���������� �������",
                licenseNumber = "564654669862363",
                siteUrl = "404-dep1.com",
                //requisites = "[���������]"
            };
            StructureId_Mainfilial = await AddDepartment(admin, structureDepartment);
            //Assert.IsTrue(StructureId_Mainfilial > 0);


            //���������� ���� ���������
            var structureTypeBranch = new StructureTypeDto()
            {
                id = 0,
                name = "���������",
                description = "��������� - ",
                code = "Branch"
            };
            StructureTypeId_Branch = await AddStructureType(admin, structureTypeBranch);
            //Assert.IsTrue(StructureTypeId_Branch > 0);
            #endregion addStructures

            #region PriceList

            var baseClinicPrice = new PriceListDto()
            {
                idParent = null,
                name = "�������",
                description = "������� ��� ����",
                code ="BasePrice",
                idStructure = StructureId_Clinic
            };
            BaseClinicPriceList = await AddPrice(accountant, baseClinicPrice);
            var branchPrice = new PriceListDto()
            {
                idParent = BaseClinicPriceList,
                name = "������",
                description = "������� ��� �������",
                code = "BranchPrice",
                idStructure = StructureId_Clinic
            };
            BranchPrice = await AddPrice(accountant, branchPrice);
            var corpPrice = new PriceListDto()
            {
                idParent = BaseClinicPriceList,
                name = "�������������",
                description = "�������������",
                code = "CorpPrice",
                idStructure = StructureId_Clinic
            };
            CorpPrice = await AddPrice(accountant, corpPrice);

            #endregion PriceList

            #region ServiceType
            var serviceAnalysis = new ServiceCategoryDto()
            {
                name = "������",
                description = "������",
                code = "Analysis"
            };
            ServiceCatId_Analysis = await AddServiceType(admin, serviceAnalysis);
            var serviceExamination = new ServiceCategoryDto()
            {
                name = "������",
                description = "������",
                code = "Examination"
            };
            ServiceCatId_Examination = await AddServiceType(admin, serviceExamination);
            var serviceProcedure = new ServiceCategoryDto()
            {
                name = "���������",
                description = "���������",
                code = "Procedure"
            };
            ServiceCatId_Procedure = await AddServiceType(admin, serviceProcedure);
            var serviceReception = new ServiceCategoryDto()
            {
                name = "�����",
                description = "�����",
                code = "Reception"
            };
            ServicaCatId_Reception = await AddServiceType(admin, serviceReception);
            var serviceAppoin = new ServiceCategoryDto()
            {
                name = "����������",
                description = "����������",
                code = "Appoint"
            };
            ServicaCatId_Appointment = await AddServiceType(admin, serviceAppoin);
            #endregion ServiceType

            #region Service
            var therapyRecept = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = "��������� ����� ����� ���������",
                shortName = "����� ����� ���������",
                description = "����� ����� ���������",
                code = "Therapy Reception"
            };
            Service_Therapy_Reception = await AddService(admin, therapyRecept);
            var therapySecRecept = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = "��������� ����� ����� ���������",
                shortName = "����� ����� ���������",
                description = "����� ����� ���������",
                code = "Therapy Rep Reception"
            };
            Service_Therapy_Sec_Reception = await AddService(admin, therapySecRecept);
            var neuroRecept = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = "��������� ����� ����� �������������",
                shortName = "����� ����� �������������",
                description = "����� ����� �������������",
                code = "Neuropathology Reception"
            };
            Service_Neuropathology_Reception = await AddService(admin, neuroRecept);
            var neuroSecRecept = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = "��������� ����� ����� �������������",
                shortName = "����� ����� �������������",
                description = "����� ����� �������������",
                code = "Neuropathology Rep Reception"
            };
            Service_Neuropathology_Sec_Reception = await AddService(admin, neuroSecRecept);
            var trerapAppoint = new ServiceDto()
            {
                idServiceType = ServicaCatId_Appointment,
                fullName = " ���������� ����� ������� ����������",
                shortName = "����������",
                description = "����������",
                code = "Therapy Rep Reception"
            };
            Service_Therapy_Appointment = await AddService(admin, trerapAppoint);
            var ecg = new ServiceDto()
            {
                idServiceType = ServiceCatId_Examination,
                fullName = "��� � ������������",
                shortName = "���",
                description = "���",
                code = "ECG"
            };
            Service_ECG = await AddService(admin, ecg);
            var massageNeck = new ServiceDto()
            {
                idServiceType = ServiceCatId_Procedure,
                fullName = "������ ������������ �������",
                shortName = "������",
                description = "������",
                code = "Massage Neck"
            };
            Service_Massage_Neck = await AddService(admin, massageNeck);
            var massageFull = new ServiceDto()
            {
                idServiceType = ServiceCatId_Procedure,
                fullName = "������ ������",
                shortName = "������",
                description = "������",
                code = "Massage Full"
            };
            Service_Massage_Full = await AddService(admin, massageFull);
            var massageBack = new ServiceDto()
            {
                idServiceType = ServiceCatId_Procedure,
                fullName = "������ �����",
                shortName = "������",
                description = "������",
                code = "Massage Back"
            };
            Service_Massage_Back = await AddService(admin, massageBack);
            #endregion Service

            #region ServiceInPrice
            var base1 = new ServiceInPriceDto()
            {
                idPrice = BaseClinicPriceList,
                idService = Service_Therapy_Reception,
                unitCost = 700,
            };
            ServicePrice_base1 = await AddServiceInPrice(admin, base1);
            var base2 = new ServiceInPriceDto()
            {
                idPrice = BaseClinicPriceList,
                idService = Service_Therapy_Sec_Reception,
                unitCost = 400,
            };
            ServicePrice_base2 = await AddServiceInPrice(admin, base2);
            var base3 = new ServiceInPriceDto()
            {
                idPrice = BaseClinicPriceList,
                idService = Service_Therapy_Appointment,
                unitCost = 300,
            };
            ServicePrice_base3 = await AddServiceInPrice(admin, base3);
            var base4 = new ServiceInPriceDto()
            {
                idPrice = BaseClinicPriceList,
                idService = Service_ECG,
                unitCost = 300,
            };
            ServicePrice_base4 = await AddServiceInPrice(admin, base4);
            var branch1 = new ServiceInPriceDto()
            {
                idPrice = BranchPrice,
                idService = Service_Therapy_Reception,
                unitCost = 650,
            };
            ServicePrice_branch1 = await AddServiceInPrice(admin, branch1);
            var branch2 = new ServiceInPriceDto()
            {
                idPrice = BranchPrice,
                idService = Service_Therapy_Sec_Reception,
                unitCost = 300,
            };
            ServicePrice_branch2 = await AddServiceInPrice(admin, branch2);
            var branch3 = new ServiceInPriceDto()
            {
                idPrice = BranchPrice,
                idService = Service_Massage_Neck,
                unitCost = 400,
            };
            ServicePrice_branch3 = await AddServiceInPrice(admin, branch3);
            var branch4 = new ServiceInPriceDto()
            {
                idPrice = BranchPrice,
                idService = Service_Massage_Full,
                unitCost = 900,
            };
            ServicePrice_branch4 = await AddServiceInPrice(admin, branch4);
            var corp1 = new ServiceInPriceDto()
            {
                idPrice = CorpPrice,
                idService = Service_Massage_Back,
                unitCost = 600,
            };
            ServicePrice_corp1 = await AddServiceInPrice(admin, corp1);
            var corp2 = new ServiceInPriceDto()
            {
                idPrice = CorpPrice,
                idService = Service_Neuropathology_Reception,
                unitCost = 700,
            };
            ServicePrice_corp2 = await AddServiceInPrice(admin, corp2);
            var corp3 = new ServiceInPriceDto()
            {
                idPrice = CorpPrice,
                idService = Service_Neuropathology_Sec_Reception,
                unitCost = 400,
            };
            ServicePrice_corp3 = await AddServiceInPrice(admin, corp3);

            #endregion ServiceInPrice

        }
        //������
        public async Task<int> AddStructureType(HttpClient user, StructureTypeDto structureType)
        {
            var response = await MakeItem(user, "/api/StructureType/AddOrUpdate", structureType);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        public async Task<int> AddLegalStatus(HttpClient user, LegalStatusDto legalStatus)
        {
            var response = await MakeItem(user, "/api/LegalStatus/AddOrUpdate", legalStatus);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        public async Task<int> AddClinic(HttpClient admin, StructureDto clinic)
        {
            clinic.idType = StructureTypeId_Clinic;
            clinic.idLegalState = LegalStatusId_Clinic;

            var response = await MakeItem(admin, "/api/Structure/AddOrUpdate", clinic);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        public async Task<int> AddDepartment(HttpClient admin, StructureDto department)
        {
            department.idParent = StructureId_Clinic;
            department.idType = StructureTypeId_Mainfilial;

            var response = await MakeItem(admin, "/api/Structure/AddOrUpdate", department);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        public async Task<int> AddBranch(HttpClient admin, StructureDto branch)
        {
            branch.idParent = StructureId_Mainfilial;
            branch.idType = StructureTypeId_Branch;

            var response = await MakeItem(admin, "/api/Structure/AddOrUpdate", branch);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        public async Task<int> AddCabinet(HttpClient admin, StructureDto cabinet)
        {
            cabinet.idType = StructureTypeId_Cabinet;

            var response = await MakeItem(admin, "/api/Structure/AddOrUpdate", cabinet);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        public async Task<int> AddRoom(HttpClient admin, RoomDto room)
        {
            var response = await MakeItem(admin, "/api/Room/AddOrUpdate", room);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }
        public async Task<int> AddToRoom(HttpClient admin, RoomToStructureDto roomToStructure)
        {
            var response = await MakeItem(admin, "/api/Room/AddOrUpdate", roomToStructure);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }









        //�������
        #region PostId
        private int PostId_ChiefDoctor = 0;
        private int PostId_Doctor = 0;
        private int PostId_Admin = 0;
        private int PostId_Accountant = 0;
        private int PostId_Registry = 0;
        #endregion PostId

        #region SpecialityId
        private int SpecialityId_LOR = 0;
        private int SpecialityId_Gynecology = 0;
        private int SpecialityId_Pediatrics = 0;
        private int SpecialityId_Therapy = 0;
        private int SpecialityId_Traumatology = 0;
        private int SpecialityId_Neuralogy = 0;
        #endregion SpecialityId

        #region StateItemId
        private int StateItem_Lor = 0;
        private int StateItem_Gynecology = 0;
        private int StateItem_Pediatrics = 0;
        private int StateItem_Therapy = 0;
        private int StateItem_Physio = 0;
        private int StateItem_Lab = 0;
        private int StateItem_LFK = 0;
        private int StateItem_Massage = 0;
        private int StateItem_Dermatology = 0;
        private int StateItem_Traumatology = 0;
        private int StateItem_Neuralogy = 0;
        #endregion StateItemId

        private int Person_Test = 0;
        private int PersonContact_Test = 0;
        private int Sex_Muj_Test = 0;
        private int ContactType_Telegram_Test = 0;

        private int Employee_Test = 0;

        #region TestPriceList
        private int BaseClinicPriceList = 0;
        private int BranchPrice = 0;
        private int CorpPrice = 0;

        #region serviceCatrgory
        private int ServiceCatId_Analysis = 0;
        private int ServiceCatId_Examination = 0;
        private int ServiceCatId_Procedure = 0;
        private int ServicaCatId_Reception = 0;
        private int ServicaCatId_Appointment = 0;
        #endregion serviceCatrgory

        #region serviceId
        private int Service_Therapy_Reception = 0;
        private int Service_Therapy_Sec_Reception = 0;
        private int Service_Neuropathology_Reception = 0;
        private int Service_Neuropathology_Sec_Reception = 0;
        private int Service_Therapy_Appointment = 0;
        private int Service_ECG = 0;
        private int Service_Massage_Neck = 0;
        private int Service_Massage_Full = 0;
        private int Service_Massage_Back = 0;
        #endregion ServiceId

        #region ServiceInPrice
        private int ServicePrice_base1 = 0;
        private int ServicePrice_base2 = 0;
        private int ServicePrice_base3 = 0;
        private int ServicePrice_base4 = 0;

        private int ServicePrice_branch1 = 0;
        private int ServicePrice_branch2 = 0;
        private int ServicePrice_branch3 = 0;
        private int ServicePrice_branch4 = 0;

        private int ServicePrice_corp1 = 0;
        private int ServicePrice_corp2 = 0;
        private int ServicePrice_corp3 = 0;


        #endregion ServiceInPrice

        #endregion TestPriceList

        private async Task<int> AddPrice(HttpClient user, PriceListDto priceList)
        {
            var response = await MakeItem(user, "/api/PriceList/AddOrUpdate", priceList);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }
        private async Task<int> AddServiceInPrice(HttpClient user, ServiceInPriceDto price)
        {
            var response = await MakeItem(user, "/api/PriceList/AddOrUpdate", price);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        private async Task<int> AddSex(HttpClient user, SexDto person)
        {
            var response = await MakeItem(user, "/api/Sex/AddOrUpdate", person);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }

        private async Task<int> AddContactType(HttpClient user, ContactTypeDto person)
        {
            var response = await MakeItem(user, "/api/ContactType/AddOrUpdate", person);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }

        private async Task<int> AddPersonContact(HttpClient user, PersonContactDto person)
        {
            var response = await MakeItem(user, "/api/PersonContact/AddOrUpdate", person);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }
  
        private async Task<int> AddEmployee(HttpClient user, EmployeeHistoryDto employeeHistory)
        {
            var response = await MakeItem(user, "/api/EmployeeHistory/AddOrUpdate", employeeHistory);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        private async Task<int> AddPerson(HttpClient user, PersonDto person)
        {
            var response = await MakeItem(user, "/api/Person/AddOrUpdate", person);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }

        private async Task<int> AddPost(HttpClient user, PostDto post)
        {

            var response = await MakeItem(user, "/api/Post/AddOrUpdate", post);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }

        private async Task<int> AddSpeciality(HttpClient user, SpecialityDto speciality)
        {

            var response = await MakeItem(user, "/api/Speciality/AddOrUpdate", speciality);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }

        private async Task<int> AddStateItem(HttpClient user, int idPost, int idStruc, int amount)
        {
            string url = "/api/StateItem/AddOrUpdate";
            var stateItem = new StateItemDto
            {
                idStructure = idStruc,
                idPost = idPost,
                amount = amount,
            };

            var response = await MakeItem(user, url, stateItem);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;    
        }

        // speciality
        private async Task<int> AddServiceType(HttpClient user, ServiceCategoryDto serviceCategory)
        {
            var response = await MakeItem(user, "/api/ServiceCategory/AddOrUpdate", serviceCategory);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        private async Task<int> AddService(HttpClient user, ServiceDto service)
        {
            var response = await MakeItem(user, "/api/Service/AddOrUpdate", service);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }


        private Task<HttpResponseMessage> MakeItem(HttpClient user, string url, object data)
        {
            var str = JsonConvert.SerializeObject(data);
            var item = new StringContent(str, Encoding.UTF8, "application/json");

            var response = user.PostAsync(url, item);
            return response;
            }


        }
}