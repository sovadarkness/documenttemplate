using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AppDocument.General;
using AppDocument.MedicalService;
using AppDocument.State;
using AppDocument.StructureClinik;
using BackendLogin.IntegrationTests;
using DALDocument.EF.Context;
using DALDocument.EF.Entities;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using PLDocument;

namespace StructureClinik.IntegrationTest
{
    [TestFixture]
    public class ServiceForSpecialityTests : BaseMemoryTest
    {

        [Test]
        public async Task ScenarioSpecialityService()
        {
            //�����������
            var admin = await loginWithUser("admin", "Pa$$word123");
            var headDoc = await loginWithUser("head_doctor", "!12345Qq");
            var accountant = await loginWithUser("accountant", "!12345Qq");

            //�������
           
            #region Speciality
            var specLOR = new SpecialityDto() {
                name = "�������������������",
                description = "�������������������",
                code = "Otorhinolaryngology"
            };
            SpecialityId_LOR = await AddSpeciality(admin, specLOR);

            var specGynecology = new SpecialityDto() {
                name = "����������-�����������",
                description = "����������-�����������",
                code = "Gynecology"
            };
            SpecialityId_Gynecology = await AddSpeciality(admin, specGynecology);

            var specPediatrics = new SpecialityDto() {
                name = "���������",
                description = "���������",
                code = "Pediatrics"
            };
            SpecialityId_Pediatrics = await AddSpeciality(admin, specPediatrics);

            var specTherapy = new SpecialityDto() {
                name = "����� �������",
                description = "����� �������",
                code = "Therapy"
            };
            SpecialityId_Therapy = await AddSpeciality(admin, specTherapy);

            var specTraumatology = new SpecialityDto() {
                name = "�������������",
                description = "�������������",
                code = "Traumatology"
            };
            SpecialityId_Traumatology = await AddSpeciality(admin, specTraumatology);

            var specNeuralogy = new SpecialityDto() {
                name = "����������",
                description = "����������",
                code = "Neuralogy"
            };
            SpecialityId_Neuralogy = await AddSpeciality(admin, specNeuralogy);
            var specUltra = new SpecialityDto()
            {
                name = "���",
                description = "���",
                code = "Ultra"
            };
            SpecialityId_UltraSound = await AddSpeciality(admin, specUltra);
            #endregion

            #region ServiceType
            var serviceAnalysis = new ServiceCategoryDto()
            {
                name = "������",
                description = "������",
                code = "Analysis"
            };
            SpecialityId_LOR = await AddServiceType(admin, serviceAnalysis);
            var serviceExamination = new ServiceCategoryDto()
            {
                name = "������",
                description = "������",
                code = "Examination"
            };
            SpecialityId_LOR = await AddServiceType(admin, serviceExamination);
            var serviceProcedure = new ServiceCategoryDto()
            {
                name = "���������",
                description = "���������",
                code = "Procedure"
            };
            SpecialityId_LOR = await AddServiceType(admin, serviceProcedure);
            var serviceReception = new ServiceCategoryDto()
            {
                name = "�����",
                description = "�����",
                code = "Reception"
            };
            SpecialityId_LOR = await AddServiceType(admin, serviceReception);
            #endregion ServiceType

            #region Service
            var gynecolRecept = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = "��������� ����� ����� ����������",
                shortName = "����� ����� ����������",
                description = "����� ����� ����������",
                code = "Gynecology Reception"
            };
            Service_Gynecology_Reception = await AddService(admin, gynecolRecept);
            var gynecolPregRecept = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = " ����� ���������� � ����� ����������",
                shortName = "����� ����������",
                description = "����� ����������",
                code = "Pregnant Gynecology Reception"
            };
            Service_Gynecology_Pregnant_Reception = await AddService(admin, gynecolPregRecept);
            var gynecolExam = new ServiceDto()
            {
                idServiceType = ServicaCatId_Reception,
                fullName = " ������ � ����� ����������",
                shortName = "������ ����������",
                description = "������ ����������",
                code = "Gynecology Examination"
            };
            Service_Gynecology_Pregnant_Reception = await AddService(admin, gynecolExam);
            var usLiver = new ServiceDto()
            {
                idServiceType = ServiceCatId_Examination,
                fullName = "��� ������",
                shortName = "��� ������",
                description = "��� ������",
                code = "UltraSound Liver"
            };
            Service_UltraSound_Liver = await AddService(admin, usLiver);
            var usTotal = new ServiceDto()
            {
                idServiceType = ServiceCatId_Examination,
                fullName = "��� ���������� �������",
                shortName = "��� ������",
                description = "��� ������",
                code = "UltraSound Total"
            };
            Service_UltraSound_Total = await AddService(admin, usTotal);
            #endregion Service

            #region ServiceSpeciality
            var servSpecGynecology_Reception = new ServiceForSpecialityDto()
            {
                idSpeciality = SpecialityId_Gynecology,
                idService = Service_Gynecology_Reception
            };
            ServiceSpec_Gynecology_Reception = await AddServiceSpec(admin, servSpecGynecology_Reception);

            var servSpecGynecology_Pregnant_Reception = new ServiceForSpecialityDto()
            {
                idSpeciality = SpecialityId_Gynecology,
                idService = Service_Gynecology_Pregnant_Reception
            };
            ServiceSpec_Gynecology_Reception = await AddServiceSpec(admin, servSpecGynecology_Pregnant_Reception);

            var servSpecGynecology_Examination = new ServiceForSpecialityDto()
            {
                idSpeciality = SpecialityId_Gynecology,
                idService = Service_Gynecology_Examination
            };
            ServiceSpec_Gynecology_Reception = await AddServiceSpec(admin, servSpecGynecology_Examination);

            var servSpecUltraSound_Liver = new ServiceForSpecialityDto()
            {
                idSpeciality = SpecialityId_UltraSound,
                idService = Service_UltraSound_Liver
            };
            ServiceSpec_UltraSound_Liver = await AddServiceSpec(admin, servSpecUltraSound_Liver);
            var servSpecUltraSound_Total = new ServiceForSpecialityDto()
            {
                idSpeciality = SpecialityId_UltraSound,
                idService = Service_UltraSound_Total
            };
            ServiceSpec_UltraSound_Total = await AddServiceSpec(admin, servSpecUltraSound_Total);
            #endregion ServiceSpeciality

        }

        private int SpecialityId_LOR = 0;
        private int SpecialityId_Gynecology = 0;
        private int SpecialityId_Pediatrics = 0;
        private int SpecialityId_Therapy = 0;
        private int SpecialityId_Traumatology = 0;
        private int SpecialityId_Neuralogy = 0;
        private int SpecialityId_UltraSound = 0;

        private int ServiceCatId_Analysis = 0;
        private int ServiceCatId_Examination = 0;
        private int ServiceCatId_Procedure = 0;
        private int ServicaCatId_Reception = 0;

        private int Service_Gynecology_Reception = 0;
        private int Service_Gynecology_Pregnant_Reception = 0;
        private int Service_Gynecology_Examination = 0;
        private int Service_UltraSound_Liver = 0;
        private int Service_UltraSound_Total = 0;

        private int ServiceSpec_Gynecology_Reception = 0;
        private int ServiceSpec_Gynecology_Pregnant_Reception = 0;
        private int ServiceSpec_Gynecology_Examination = 0;
        private int ServiceSpec_UltraSound_Liver = 0;
        private int ServiceSpec_UltraSound_Total = 0;



        private async Task<int> AddSpeciality(HttpClient user, SpecialityDto speciality)
        {

            var response = await MakeItem(user, "/api/Speciality/AddOrUpdate", speciality);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;

        }

        private async Task<int> AddServiceType(HttpClient user, ServiceCategoryDto serviceCategory)
        {
            var response = await MakeItem(user, "/api/ServiceCategory/AddOrUpdate", serviceCategory);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        private async Task<int> AddService(HttpClient user, ServiceDto service)
        {
            var response = await MakeItem(user, "/api/Service/AddOrUpdate", service);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        private async Task<int> AddServiceSpec(HttpClient user, ServiceForSpecialityDto serviceSpec)
        {
            var response = await MakeItem(user, "/api/ServiceForSpeciality/AddOrUpdate", serviceSpec);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }

        private Task<HttpResponseMessage> MakeItem(HttpClient user, string url, object data)
        {
            var str = JsonConvert.SerializeObject(data);
            var item = new StringContent(str, Encoding.UTF8, "application/json");

            var response = user.PostAsync(url, item);
            return response;
        }
        
    }
}