﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StructureClinik.IntegrationTest.Helpers
{
    public static class NetworkHelper
    {
        public static Task<HttpResponseMessage> MakeHttpPostRequest(HttpClient user, string url, object data)
        {
            var str = JsonConvert.SerializeObject(data);
            var item = new StringContent(str, Encoding.UTF8, "application/json");

            var response = user.PostAsync(url, item);
            return response;
        }
    }
}
