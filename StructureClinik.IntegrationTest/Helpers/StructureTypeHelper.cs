﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureClinik.IntegrationTest.Helpers
{
    public static class StructureTypeHelper
    {
        public class Values
        {
            public class Clinic
            {
                public static int Id;
                public static string Code => "Clinic";
            }
            public class Branch
            {
                public static int Id;
                public static string Code => "Branch";
            }
            public class Department
            {
                public static int Id;
                public static string Code => "Department";
            }
            public class Cabinet
            {
                public static int Id;
                public static string Code => "Cabinet";
            }
        }
    }
}
