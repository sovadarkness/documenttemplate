﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StructureClinik.IntegrationTest.Helpers
{
    public static class LegalStatusHelper
    {
        public class Values
        {
            public class Individual
            {
                public static int Id;
                public static string Code => "Individual";
            }
            public class LLC
            {
                public static int Id;
                public static string Code => "LLC";
            }
        }
    }
}
