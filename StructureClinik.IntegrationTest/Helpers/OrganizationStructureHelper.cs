﻿using AppDocument.StructureClinik;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StructureClinik.IntegrationTest.Helpers
{
    public static class OrganizationStructureHelper
    {
        public static async Task<int> AddClinic(HttpClient user, StructureDto clinic)
        {
            clinic.idType = StructureTypeHelper.Values.Clinic.Id;
            return await AddOrgStructure(user, clinic);
        }

        public static async Task<int> AddBranch(HttpClient user, StructureDto branch)
        {
            branch.idType = StructureTypeHelper.Values.Branch.Id;
            return await AddOrgStructure(user, branch);
        }

        public static async Task<int> AddDepartment(HttpClient user, StructureDto department)
        {
            department.idType = StructureTypeHelper.Values.Department.Id;
            return await AddOrgStructure(user, department);
        }

        public static async Task<int> AddCabinet(HttpClient user, StructureDto cabinet)
        {
            cabinet.idType = StructureTypeHelper.Values.Cabinet.Id;
            return await AddOrgStructure(user, cabinet);
        }

        private static async Task<int> AddOrgStructure(HttpClient client, StructureDto dto)
        {
            var response = await NetworkHelper.MakeHttpPostRequest(client, "/api/Structure/AddOrUpdate", dto);

            var contentResponse = await response.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<int>(contentResponse);
            return id;
        }
    }
}
