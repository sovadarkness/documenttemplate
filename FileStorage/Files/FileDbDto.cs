﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.Files
{
    public class FileDbDto
    {
        public int id { get; set; }
        public string filePath{ get; set; }
        public string name { get; set; }
    }
}
