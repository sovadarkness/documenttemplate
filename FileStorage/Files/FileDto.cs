﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDocument.Files
{
    public class FileDto
    {
        public string name { get; set; }
        public byte[] body { get; set; }
    }
}
