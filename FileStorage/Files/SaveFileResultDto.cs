﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDocument.Files
{
    public class SaveFileResultDto
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
    }
}
