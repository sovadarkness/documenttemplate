﻿using AppDocument.Files;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.Files
{
    public interface IFileRepository
    {
        DtoWrapper<SaveFileResultDto> Save(FileDto file);
        DtoWrapper Remove(int id);
        DtoWrapper<FileDto> GetFileByPath(string path);
        DtoWrapper<FileDto> GetFileById(int id);
    }
}
