﻿using AppDocument.Files;
using LayerTransmitter.Wrapper;

namespace FileStorage.Files
{
    public interface IFileService
    {
        DtoWrapper<FileDto> GetFileByPath(string path);
        DtoWrapper<FileDto> GetFileById(int id);
        DtoWrapper Remove(int id);
        DtoWrapper<SaveFileResultDto> Save(FileDto saveFileDto);
    }
}