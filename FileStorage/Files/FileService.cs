﻿using AppDocument.Files;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.Files
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;
        public FileService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }
        public DtoWrapper<SaveFileResultDto> Save(FileDto saveFileDto)
        {
            return _fileRepository.Save(saveFileDto);
        }

        public DtoWrapper Remove(int id)
        {
            return _fileRepository.Remove(id);
        }

        public DtoWrapper<FileDto> GetFileByPath(string path)
        {
            return _fileRepository.GetFileByPath(path);
        }

        public DtoWrapper<FileDto> GetFileById(int id)
        {
            return _fileRepository.GetFileById(id);
        }
    }
}
