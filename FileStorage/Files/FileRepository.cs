﻿using AppDocument.Files;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.Files
{
    public class FileRepository : IFileRepository
    {
        private readonly IFileSystemProvider _fileSystemProvider;
        private readonly IFileDbContextProvider _fileDbContextProvider;
        public FileRepository(IFileSystemProvider fileSystemProvider,
            IFileDbContextProvider fileDbContextProvider
            )
        {
            _fileSystemProvider = fileSystemProvider;
            _fileDbContextProvider = fileDbContextProvider;
        }
        public DtoWrapper<FileDto> GetFileById(int id)
        {
            var file = _fileDbContextProvider.GetById(id);
            var fileBody = _fileSystemProvider.GetByPath(file.filePath);
            var dto = new FileDto()
            {
                body = fileBody,
                name = file.name
            };
            return DtoWrapper<FileDto>.Ok(dto);
        }

        public DtoWrapper<FileDto> GetFileByPath(string path)
        {
            var file = _fileDbContextProvider.GetByPath(path);
            if (file == null) return DtoWrapper<FileDto>.Error(new List<IExecutionError> { });
            var fileBody = _fileSystemProvider.GetByPath(path);
            var dto = new FileDto()
            {
                body = fileBody,
                name = file.name
            };
            return DtoWrapper<FileDto>.Ok(dto);
        }

        public DtoWrapper Remove(int id)
        {
            return _fileDbContextProvider.Remove(id);
        }

        public DtoWrapper<SaveFileResultDto> Save(FileDto file)
        {
            var filePath = _fileSystemProvider.Save(file.body);
            var fileDbDto = new FileDbDto()
            {
                name = file.name,
                filePath = filePath
            };
            return _fileDbContextProvider.Add(fileDbDto);
        }
    }

    public interface IFileSystemProvider
    {
        string BaseDirectory { get; }
        byte[] GetByPath(string path);
        string Save(byte[] body);
    }

    public interface IFileDbContextProvider
    {
        DtoWrapper<SaveFileResultDto> Add(FileDbDto fileDbDto);
        FileDbDto GetById(int id);
        FileDbDto GetByPath(string path);
        DtoWrapper Remove(int id);
    }
}
