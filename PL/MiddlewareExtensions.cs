﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using PLDocument.Middleware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PLDocument
{
    public static class MiddlewareExtensions
    {
        public static void AddMiddlewareExtensions(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<RequestIdMiddleware>();
            //builder.UseMiddleware<StopWatchMiddleware>();
            //builder.UseMiddleware<RequestBodyMiddleware>();
            builder.UseMiddleware<ExceptionHandlerMiddleware>();
            builder.UseMiddleware<AnswerWrapperMiddleware>();
        }
    }
}
