using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PLDocument.Middleware
{
    /// <summary>
    /// Компонент для измерения времени выполнения запроса
    /// </summary>
    class StopWatchMiddleware
    {
        private readonly RequestDelegate _next;

        public StopWatchMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            context.Items.Add(FilterConstants.Stopwatch, stopwatch);
            context.Response.Headers.Add(FilterConstants.Stopwatch, stopwatch.ElapsedMilliseconds.ToString());//to delete 

            await _next.Invoke(context);
        }
    }
}
