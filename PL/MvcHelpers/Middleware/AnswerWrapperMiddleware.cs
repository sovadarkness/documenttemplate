using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace PLDocument.Middleware
{
    /// <summary>
    /// Обработчик ошибок
    /// </summary>
    class AnswerWrapperMiddleware
    {
        private readonly RequestDelegate _next;

        public AnswerWrapperMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            //TODO DO
            await _next.Invoke(context);
        }
    }
}
