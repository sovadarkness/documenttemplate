//using System;
//using System.Diagnostics;
//using System.IO;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;

//namespace ReactApplication.Middleware
//{
//    /// <summary>
//    /// Техническое логирование
//    /// </summary>
//    /// <remarks>
//    /// Что делает данный компонент:
//    /// 1 - Ставит в контекст текущего аутентифицированного пользователя;
//    /// 2 - Создает сообщение для тех. логирования и ставит его в контекст запроса
//    /// 3 - После отработки всех остальных компонентов, выполняет запись сообщения об ошибке,
//    ///     если в контексте запроса будет обнаружено соответствующее значение - exceptionReceived;
//    /// 4 - Запись итога работы запроса со всем его окружением в тех. лог.
//    /// </remarks>
//    class TechLogMiddleware
//    {
//        private readonly RequestDelegate _next;
//        private readonly ILogger<TechLogMiddleware> _logger;
//        private readonly LoggingService _loggingService;
//        public TechLogMiddleware(RequestDelegate next, ILogger<TechLogMiddleware> logger)
//        {
//            _next = next;
//            _logger = logger;
//            _loggingService = new LoggingService(logger);
//        }

//        public async Task Invoke(HttpContext context)
//        {
//            var originalBody = context.Response.Body;
//            try
//            {
//                await FixUserName(context);

//                await using var memStream = new MemoryStream();
//                context.Response.Body = memStream;
//                var webApiLogMessage = _loggingService.CreateLogMessage<WebApiLogMessage>(context);
//                await _next(context);
//                await ProcessException(context);
//                await FinalizeResponse(memStream, originalBody, context, webApiLogMessage);

//                if (Debugger.IsAttached)
//                    Console.WriteLine(JsonConvert.SerializeObject(webApiLogMessage));
//            }
//            catch (Exception exception)
//            {
//                if (Debugger.IsAttached)
//                    Console.WriteLine(exception);
//                _logger.LogError(exception.ToString());
//            }
//            finally
//            {
//                context.Response.Body = originalBody;
//            }
//        }

//        /// <summary>
//        /// Фиксация исключения, и изменение ответа клиенту.
//        /// </summary>
//        private static async Task ProcessException(HttpContext context)
//        {
//            if (context.Items.ContainsKey("exceptionReceived"))
//            {
//                var exceptionReceived = context.Items["exceptionReceived"] as Exception;

//                string errorMessage;

//                if (exceptionReceived is UserMessageException userMessageException)
//                {
//                    errorMessage = userMessageException.Message;
//                    context.Response.StatusCode = 400;
//                }
//                else
//                {
//                    errorMessage = "Уважаемый пользователь, на сервере возникла ошибка, обратитесь, пожалуйста, в службу технической поддержки.";
//                    context.Response.StatusCode = 500;
//                }

//                if (Debugger.IsAttached)
//                    Console.WriteLine(JsonConvert.SerializeObject(exceptionReceived));
//                await context.Response.WriteAsync($"{errorMessage} [Код ошибки:{context.TraceIdentifier}]");
//                context.Response.ContentType = "text/html;charset=utf-8";
//            }
//        }

//        private async Task FixUserName(HttpContext context)
//        {
//            if (context.Items.ContainsKey(FilterConstants.UserName) && !string.IsNullOrEmpty(context.Items[FilterConstants.UserName]?.ToString()))
//                return;

//            if (!context.Items.ContainsKey(FilterConstants.UserName) &&
//                context.User?.Identity != null &&
//                !string.IsNullOrEmpty(context.User.Identity.Name))
//            {
//                context.Items.Add(FilterConstants.UserName, context.User.Identity.Name);
//            }
//            else
//            {
//                var authResult = await context.AuthenticateAsync("Bearer");
//                if (!context.Items.ContainsKey(FilterConstants.UserName) && !string.IsNullOrEmpty(authResult?.Principal?.Identity?.Name))
//                    context.Items.Add(FilterConstants.UserName, authResult?.Principal?.Identity?.Name);
//            }
//        }

//        /// <summary>
//        /// Финализация обработки запроса
//        /// </summary>
//        private async Task FinalizeResponse(MemoryStream memStream, Stream originalBody, HttpContext context, WebApiLogMessage webApiLogMessage)
//        {
//            memStream.Position = 0;
//            var responseBodyStream = new StreamReader(memStream);
//            using (responseBodyStream)
//            {
//                string responseBody = await responseBodyStream.ReadToEndAsync();
//                memStream.Position = 0;
//                await memStream.CopyToAsync(originalBody);
//                if (context.Items.ContainsKey("exceptionReceived"))
//                {
//                    var webApiExceptionLogMessage = _loggingService.CreateLogMessage<WebApiExceptionLogMessage>(context);
//                    webApiExceptionLogMessage.ExceptionReceived = context.Items["exceptionReceived"] as Exception;
//                    _loggingService.WriteLogMessage(context, webApiExceptionLogMessage, responseBody);
//                }
//                else
//                {
//                    _loggingService.WriteLogMessage(context, webApiLogMessage, responseBody);
//                }
//            }
//        }

//    }
//}
