﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PLDocument.Middleware
{
    /// <summary>
    /// Компонент для маркировки входящих запросов
    /// </summary>
    public class RequestIdMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestIdMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)        
        {
            var requestId = context.TraceIdentifier;

            if (context.Request.Headers.ContainsKey("Host"))//To replace
                context.Items.Add("Host", context.Request.Headers["Host"][0]);

            context.Items.Add(FilterConstants.RequestId, requestId);
            context.Response.Headers.Add(FilterConstants.RequestId, requestId);

            await _next.Invoke(context);
        }
    }
}
