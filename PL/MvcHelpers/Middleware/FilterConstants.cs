﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PLDocument.Middleware
{
    public class FilterConstants
    {
        public static string RequestId { get; internal set; } = "RequestId";
        public static string Stopwatch { get; internal set; } = "Stopwatch";
        public static string UserName { get; internal set; } = "UserName";
        public static string Exception { get; internal set; } = "Exception";
    }
}
