using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using LayerTransmitter.Wrapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace PLDocument.Middleware
{
    /// <summary>
    /// Обработчик ошибок
    /// </summary>
    class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionMessageAsync(context, ex).ConfigureAwait(false);
            }
        }

        private static Task HandleExceptionMessageAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            int statusCode = (int)HttpStatusCode.InternalServerError;
            string result;
            var iExecutingException = exception as IExecutionException;
            DtoWrapper handledResult;
            if (iExecutingException == null)
            {
                iExecutingException = new UnknownException(exception);
            }
            handledResult = DtoWrapper.Exception(iExecutingException);
            result = JsonConvert.SerializeObject(handledResult);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(result);
        }
    }
}
