﻿using Core.Abstraction;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppDocument;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace PLDocument.ActionFilters
{
    public class SessionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var language = context.HttpContext.Request.GetUserLanguages();


            var serviceProvider = context.HttpContext.RequestServices;
            var sessionProvider = (ISessionProvider)serviceProvider.GetService(typeof(ISessionProvider));
            var languageProvider = (ILanguageProvider)serviceProvider.GetService(typeof(ILanguageProvider));

            var token = context.HttpContext.Request.Headers[HeaderNames.Authorization];
            sessionProvider.Initialize(context.HttpContext.User.Claims, token);
            languageProvider.Initialize(language);
        }
    }

    public static class HttpRequestExtensions
    {
        public static string GetUserLanguages(this HttpRequest request)
        {
            return request.GetTypedHeaders()
                .AcceptLanguage
                ?.OrderByDescending(x => x.Quality ?? 1)
                .Select(x => x.Value.ToString())
                .FirstOrDefault() ?? string.Empty;
        }
    }
}
