﻿using Core.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PLDocument.Providers
{
    public class SessionProvider : ISessionProvider
    {
        public string UserId { get; private set; }
        public string UserName { get { return "unknown user"; } }

        public int CurrentStructureId => throw new NotImplementedException();

        public string Token { get; set; }

        public SessionProvider()
        {
        }

        public void Initialize(IEnumerable<Claim> claims, string token)
        {
            Token = token;
            UserId = claims.SingleOrDefault(x => x.Type == "sub")?.Value;
        }
    }
}
