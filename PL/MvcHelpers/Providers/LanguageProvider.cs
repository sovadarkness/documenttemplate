﻿using Core.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppDocument;

namespace PLDocument.Providers
{
    public class LanguageProvider : ILanguageProvider
    {
        public string SelectedLanguage { get; private set; }
        public int LanguageId { get; private set; }

        private readonly ISystemSettingProvider _systemSettingProvider;

        public LanguageProvider(ISystemSettingProvider systemSettingProvider)
        {
            _systemSettingProvider = systemSettingProvider;
        }

        public void Initialize(string langCode)
        {
            //todo check review
            var systemLanguages = _systemSettingProvider.SystemLanguages;
            if (systemLanguages.ContainsKey(langCode))
            {
                SelectedLanguage = langCode;
                LanguageId = systemLanguages[langCode];
            }
            else
            {
                LanguageId = _systemSettingProvider.DefaultLanguageId;
                SelectedLanguage = systemLanguages.FirstOrDefault(x => x.Value == LanguageId).Key;
            }
        }
    }
}
