﻿using AppDocument.DocTemplate;
using AppDocument.General;
using AppDocument.SystemSettings;
using Microsoft.Extensions.DependencyInjection;
using PLDocument.Base;
using PLDocument.DocTemplate;
using PLDocument.General;

namespace PLDocument
{
    public class DiConverters
    {
        public static void AddConverters(IServiceCollection services)
        {
            services.AddSingleton<IModelConverter<CommonSettingsModel, CommonSettingsDto>, CommonSettingsConverter>();
            services.AddSingleton<IModelConverter<s_QueryModel, s_QueryDto>, s_QueryConverter>();
            services.AddSingleton<IModelConverter<LanguageModel, LanguageDto>, LanguageConverter>();
            services.AddSingleton<IModelConverter<CustomSvgIconModel, CustomSvgIconDto>, CustomSvgIconConverter>();
            services.AddSingleton<IModelConverter<s_DocumentTemplateTranslationModel, s_DocumentTemplateTranslationDto>, s_DocumentTemplateTranslationConverter>();
            services.AddSingleton<IModelConverter<s_QueriesDocumentTemplateModel, s_QueriesDocumentTemplateDto>, s_QueriesDocumentTemplateConverter>();
            services.AddSingleton<IModelConverter<s_TemplateDocumentPlaceholderModel, s_TemplateDocumentPlaceholderDto>, s_TemplateDocumentPlaceholderConverter>();
            services.AddSingleton<IModelConverter<s_DocumentTemplateModel, s_DocumentTemplateDto>, s_DocumentTemplateConverter>();
            services.AddSingleton<IModelConverter<s_PlaceHolderTemplateModel, s_PlaceHolderTemplateDto>, s_PlaceHolderTemplateConverter>();
            services.AddSingleton<IModelConverter<s_PlaceHolderGroupsMtmModel, s_PlaceHolderGroupsMtmDto>, s_PlaceHolderGroupsMtmConverter>();
            services.AddSingleton<IModelConverter<s_PlaceholderGroupModel, s_PlaceholderGroupDto>, s_PlaceholderGroupConverter>();
            services.AddSingleton<IModelConverter<roDocumentTemplateCategoryModel, roDocumentTemplateCategoryDto>, roDocumentTemplateCategoryConverter>();
            services.AddSingleton<IModelConverter<CustomSvgIconModel, CustomSvgIconDto>, CustomSvgIconConverter>();
        }
    }
}
