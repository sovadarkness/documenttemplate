﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace PLDocument.Base
{
    public abstract class BaseModel
    {
        public abstract bool IsNew();
        public string lastUpdatedUser { get; set; }
        public DateTime? lastUpdatedTime { get; set; }
        public string strlastUpdatedTime
        {
            get
            {
                return lastUpdatedTime?.ToString("yyyy-MM-dd HH:mm:ss");
            }
            set
            {
                DateTime date;
                if (DateTime.TryParseExact(value, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    lastUpdatedTime = date;
                }
                else
                {
                    lastUpdatedTime = null;
                }
            }
        }
    }
}
