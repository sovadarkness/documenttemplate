﻿namespace PLDocument.Base
{
    public interface IModelConverter<Model, Dto>
    {
        Dto ToDto(Model model);
    }
}
