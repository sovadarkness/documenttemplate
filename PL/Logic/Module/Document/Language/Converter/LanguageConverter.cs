using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppDocument.SystemSettings;
using PLDocument.Base;

namespace PLDocument.General
{
    public class LanguageConverter : IModelConverter<LanguageModel, LanguageDto>
    {
        public LanguageDto ToDto(LanguageModel model)
        {
            var result = new LanguageDto
            {
		id = model.id,
		name = model.name,
		description = model.description,
		code = model.code,
		isDefault = model.isDefault
            };
            return result;
        }

        
    }
}
