using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using System;
using AppDocument.General;
using LayerTransmitter.Wrapper;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.General;
using System.Collections.Generic;
using AppDocument.SystemSettings;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private readonly ILanguageService _LanguageService;
        private readonly IModelConverter<LanguageModel, LanguageDto> _converter;

        public LanguageController(ILanguageService service, IModelConverter<LanguageModel, LanguageDto> converter)
        {
            _LanguageService = service;
            _converter = converter;
        }

        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(LanguageModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _LanguageService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _LanguageService.Update(addDto);
                return updated;
            }
        }

        [HttpGet]
        [Route("GetAll")]
        public List<LanguageDto> GetAll()
        {
            var items = _LanguageService.GetAll();
            return items;
        }

	


        [HttpGet]
        [Route("GetOneById")]
        public LanguageDto GetOneById(int id)
        {
            var item = _LanguageService.GetOneByKey(id);
            return item;
        }

        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _LanguageService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
