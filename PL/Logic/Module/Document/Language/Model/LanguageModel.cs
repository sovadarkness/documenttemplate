using AppDocument.General;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.General
{
    public class LanguageModel: BaseModel
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public System.Nullable<bool> isDefault { get; set; }
		public int id { get; set; }
      
	public override bool IsNew()
        {
            return id == 0;
        }

    }
}
