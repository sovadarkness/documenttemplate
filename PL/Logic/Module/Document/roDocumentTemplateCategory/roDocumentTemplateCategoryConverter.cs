using AppDocument.DocTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.DocTemplate
{
    public class roDocumentTemplateCategoryConverter : IModelConverter<roDocumentTemplateCategoryModel, roDocumentTemplateCategoryDto>
    {
        public roDocumentTemplateCategoryDto ToDto(roDocumentTemplateCategoryModel model)
        {
            var result = new roDocumentTemplateCategoryDto
            {
		id = model.Id,
		name = model.name,
		description = model.description,
		code = model.code,
		queueNumber = model.queueNumber
            };
            return result;
        }

        
    }
}
