using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using AppDocument.DocTemplate;
using PLDocument.Base;
using PLDocument.DocTemplate;
using System.Collections.Generic;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class roDocumentTemplateCategoryController : ControllerBase
    {
        private readonly IroDocumentTemplateCategoryService _roDocumentTemplateCategoryService;
        private readonly IModelConverter<roDocumentTemplateCategoryModel, roDocumentTemplateCategoryDto> _converter;

        public roDocumentTemplateCategoryController(IroDocumentTemplateCategoryService service, IModelConverter<roDocumentTemplateCategoryModel, roDocumentTemplateCategoryDto> converter)
        {
            _roDocumentTemplateCategoryService = service;
            _converter = converter;
        }
        /// <summary>
        /// AddOrUpdate instance of DocumentTemplateCategory
        /// </summary>
        /// <param name="model"></param>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        "id": "int"
        ///        "name": "string",
        ///        "description": "string",
        ///        "code": "string",
        ///        "queueNumber": "int?"
        ///     }
        /// </remarks>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(roDocumentTemplateCategoryModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _roDocumentTemplateCategoryService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _roDocumentTemplateCategoryService.Update(addDto);
                return updated;
            }
        }
        /// <summary>
        ///  Returns all DocumentTemplateCategory
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<roDocumentTemplateCategoryDto> GetAll()
        {
            var items = _roDocumentTemplateCategoryService.GetAll();
            return items;
        }



        /// <summary>
        /// Return one record of DocumentTemplateCategory
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <response code="200">Ok, returns record of DocumentTemplateCategory with requested id</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOneById")]
        public roDocumentTemplateCategoryDto GetOneById(int id)
        {
            var item = _roDocumentTemplateCategoryService.GetOneByKey(id);
            return item;
        }

        /// <summary>
        /// Delete record of DocumentTemplateCategory with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _roDocumentTemplateCategoryService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
