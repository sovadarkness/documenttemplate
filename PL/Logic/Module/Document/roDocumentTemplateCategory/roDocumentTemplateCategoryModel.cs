using AppDocument.DocTemplate;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.DocTemplate
{
    public class roDocumentTemplateCategoryModel: BaseModel
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public int? queueNumber { get; set; }
		public int Id { get; set; }
      
	public override bool IsNew()
        {
            return Id == 0;
        }

    }
}
