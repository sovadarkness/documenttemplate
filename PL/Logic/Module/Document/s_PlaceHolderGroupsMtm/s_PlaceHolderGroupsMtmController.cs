using AppDocument.General;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using LayerTransmitter.Wrapper;
using PLDocument.General;
using System.Collections.Generic;
using PLDocument.Base;


namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_PlaceHolderGroupsMtmController : ControllerBase
    {

        private readonly Is_PlaceHolderGroupsMtmService _s_PlaceHolderGroupsMtmService;
        private readonly IModelConverter<s_PlaceHolderGroupsMtmModel, s_PlaceHolderGroupsMtmDto> _converter;
        public s_PlaceHolderGroupsMtmController(Is_PlaceHolderGroupsMtmService service, IModelConverter<s_PlaceHolderGroupsMtmModel, s_PlaceHolderGroupsMtmDto> converter)
        {
            _s_PlaceHolderGroupsMtmService = service;
            _converter = converter;
        }
        /// <summary>
        ///  Returns all s_PlaceHolderGroupsMtm
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_PlaceHolderGroupsMtmDto> GetAll()
        {
            var items = _s_PlaceHolderGroupsMtmService.GetAll();
            return items;
        }

        /// <summary>
        /// Add Or Update instance of s_PlaceHolderGroupsMtm
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        "id": "int",
        ///        "idPlaceHolder": "int?",
        ///        "idTypePlaceholder":"int?"
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_PlaceHolderGroupsMtmModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_PlaceHolderGroupsMtmService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_PlaceHolderGroupsMtmService.Update(addDto);
                return updated;
            }
        }
        /// <summary>
        /// Delete record of s_PlaceHolderGroupsMtm with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetOneById")]
        public s_PlaceHolderGroupsMtmDto GetOneById(int id)
        {
            var item = _s_PlaceHolderGroupsMtmService.GetOneByKey(id);
            return item;
        }

        /// <summary>
        /// Delete record of s_PlaceholderGroup with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_PlaceHolderGroupsMtmService.Remove(id);
            return Ok(new { result = "true" });
        }

        /// <summary>
        /// Returns all s_PlaceholderGroup with requested idPlaceHolder
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idPlaceHolder": "int"
        ///     }
        /// </remarks>
        /// <param name="idPlaceHolder"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidPlaceHolder")]
        public List<s_PlaceHolderGroupsMtmDto> GetByidPlaceHolder(int idPlaceHolder)
        {
            var items = _s_PlaceHolderGroupsMtmService.GetByidPlaceHolder(idPlaceHolder);
            return items;
        }

        /// <summary>
        /// Returns all s_PlaceholderGroup with requested idTypePlaceholder
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idTypePlaceholder": "int"
        ///     }
        /// </remarks>
        /// <param name="idTypePlaceholder"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidTypePlaceholder")]
        public List<s_PlaceHolderGroupsMtmDto> GetByidTypePlaceholder(int idTypePlaceholder)
        {
            var items = _s_PlaceHolderGroupsMtmService.GetByidTypePlaceholder(idTypePlaceholder);
            return items;
        }



    }

}
