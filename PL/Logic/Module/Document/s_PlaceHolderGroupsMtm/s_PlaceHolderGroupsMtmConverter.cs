using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.General
{
    public class s_PlaceHolderGroupsMtmConverter : IModelConverter<s_PlaceHolderGroupsMtmModel, s_PlaceHolderGroupsMtmDto>
    {
        public s_PlaceHolderGroupsMtmDto ToDto(s_PlaceHolderGroupsMtmModel model)
        {
            var result = new s_PlaceHolderGroupsMtmDto
            {
		id = model.Id,
		idPlaceHolder = model.idPlaceHolder,
		idTypePlaceholder = model.idTypePlaceholder
            };
            return result;
        }

        
    }
}
