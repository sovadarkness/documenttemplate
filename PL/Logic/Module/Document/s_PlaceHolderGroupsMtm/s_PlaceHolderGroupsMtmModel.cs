using Microsoft.AspNetCore.Http;
using PLDocument.Base;
using System;

namespace PLDocument.General
{
    public class s_PlaceHolderGroupsMtmModel: BaseModel
    {
		public int Id { get; set; }
	// start dictionary  Null Allow
        public int? idPlaceHolder { get; set; }    
        public string idPlaceHolderNavName { get; set; }
	//end dictionary  
	// start dictionary  Null Allow
        public int? idTypePlaceholder { get; set; }    
        public string idTypePlaceholderNavName { get; set; }
	//end dictionary  


        public override bool IsNew()
        {
            return Id == 0;
        }
    }
}
