using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using System;
using AppDocument.General;
using LayerTransmitter.Wrapper;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.General;
using System.Collections.Generic;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_PlaceholderGroupController : ControllerBase
    {
        private readonly Is_PlaceholderGroupService _s_PlaceholderGroupService;
        private readonly IModelConverter<s_PlaceholderGroupModel, s_PlaceholderGroupDto> _converter;

        public s_PlaceholderGroupController(Is_PlaceholderGroupService service, IModelConverter<s_PlaceholderGroupModel, s_PlaceholderGroupDto> converter)
        {
            _s_PlaceholderGroupService = service;
            _converter = converter;
        }
        /// <summary>
        /// AddOrUpdate instance of s_PlaceholderGroup
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        id: 0,
        ///        name: "new name",
        ///        description:"description",
        ///        code:"code01",
        ///        queueNumber: 2,
        ///        idCustomSvgIcon:1,
        ///        iconColor:"FFF"
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_PlaceholderGroupModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_PlaceholderGroupService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_PlaceholderGroupService.Update(addDto);
                return updated;
            }
        }
        /// <summary>
        ///  Returns all s_PlaceholderGroup
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_PlaceholderGroupDto> GetAll()
        {
            var items = _s_PlaceholderGroupService.GetAll();
            return items;
        }



        /// <summary>
        /// Delete record of s_PlaceholderGroup with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetOneById")]
        public s_PlaceholderGroupDto GetOneById(int id)
        {
            var item = _s_PlaceholderGroupService.GetOneByKey(id);
            return item;
        }
        /// <summary>
        /// Delete record of s_PlaceholderGroup with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_PlaceholderGroupService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
