using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.General
{
    public class s_PlaceholderGroupConverter : IModelConverter<s_PlaceholderGroupModel, s_PlaceholderGroupDto>
    {
        public s_PlaceholderGroupDto ToDto(s_PlaceholderGroupModel model)
        {
            var result = new s_PlaceholderGroupDto
            {
		id = model.id,
		name = model.name,
		description = model.description,
		code = model.code,
		queueNumber = model.queueNumber,
        iconColor = model.iconColor,
        idCustomSvgIcon = model.idCustomSvgIcon,

            };
            return result;
        }

        
    }
}
