using AppDocument.General;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.General
{
    public class s_PlaceholderGroupModel: BaseModel
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public int? queueNumber { get; set; }
            public int? idCustomSvgIcon { get; set; }
            public string iconColor { get; set; }
        public int id { get; set; }
      
	public override bool IsNew()
        {
            return id == 0;
        }

    }
}
