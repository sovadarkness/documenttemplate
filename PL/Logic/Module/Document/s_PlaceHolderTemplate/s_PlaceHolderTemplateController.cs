using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using System;
using AppDocument.DocTemplate;
using LayerTransmitter.Wrapper;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.DocTemplate;
using System.Collections.Generic;
using System.Linq;
using AppDocument.General;
using PLDocument.General;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_PlaceHolderTemplateController : ControllerBase
    {
        private readonly Is_PlaceHolderTemplateService _s_PlaceHolderTemplateService;
        private readonly Is_PlaceHolderGroupsMtmService _s_PlaceHolderGroupsMtmService;

        private readonly IModelConverter<s_PlaceHolderTemplateModel, s_PlaceHolderTemplateDto> _converter;
        private readonly IModelConverter<s_PlaceHolderGroupsMtmModel, s_PlaceHolderGroupsMtmDto> _converterMtm;

        public s_PlaceHolderTemplateController(Is_PlaceHolderTemplateService service, IModelConverter<s_PlaceHolderTemplateModel, s_PlaceHolderTemplateDto> converter, Is_PlaceHolderGroupsMtmService sPlaceHolderGroupsMtmService, IModelConverter<s_PlaceHolderGroupsMtmModel, s_PlaceHolderGroupsMtmDto> converterMtm)
        {
            _s_PlaceHolderTemplateService = service;
            _converter = converter;
            _s_PlaceHolderGroupsMtmService = sPlaceHolderGroupsMtmService;
            _converterMtm = converterMtm;
        }
        /// <summary>
        /// Add Or Update instance of s_PlaceHolderTemplate and conected s_PlaceHolderGroupsMtms
        /// </summary>
        /// <remarks>
        /// Sample of post data:
        ///
        ///     {
        ///        id: 0,
        ///        name: "new name",
        ///        description: "description",
        ///        code: "PHG01",
        ///        s_PlaceHolderGroupsMtms: List<s_PlaceHolderGroupsMtmModel/>,
        ///        idQuery: 1,
        ///     }
        ///     
        /// Sample of s_PlaceHolderGroupsMtmModel:
        ///  
        ///     {
        ///         id:1,
        ///         idPlaceHolder:1,
        ///         idTypePlaceholder:1
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_PlaceHolderTemplateModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_PlaceHolderTemplateService.Add(addDto);

                //foreach (var m in model.s_PlaceHolderGroupsMtms)
                //{
                //    m.id = 0;
                //    var mtmGr = _converterMtm.ToDto(m);
                //    mtmGr.idPlaceHolder = added;
                //    _s_PlaceHolderGroupsMtmService.Add(mtmGr);
                //}

                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_PlaceHolderTemplateService.Update(addDto);


                var itemsInDb = _s_PlaceHolderGroupsMtmService.GetByidPlaceHolder(model.Id);
                var toDelete = itemsInDb.Where(x => model.s_PlaceHolderGroupsMtms.All(z => z.Id != x.id));

                foreach (var m in toDelete)
                {
                    _s_PlaceHolderGroupsMtmService.Remove(m.id);
                }

                foreach (var m in model.s_PlaceHolderGroupsMtms)
                {
                    if (m.idPlaceHolder == null || m.idTypePlaceholder == null) continue;
                    var group = _s_PlaceHolderGroupsMtmService.GetExact(addDto.id, m.idTypePlaceholder.Value);
                    if (group == null)
                        _s_PlaceHolderGroupsMtmService.Add(_converterMtm.ToDto(m));
                }

                return updated;
            }
        }
        /// <summary>
        ///  Returns all s_PlaceHolderTemplate
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_PlaceHolderTemplateDto> GetAll()
        {
            var items = _s_PlaceHolderTemplateService.GetAll();
            return items;
        }

        /// <summary>
        /// Returns all s_PlaceHolderTemplate with requested idQuery
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        idQuery: 1
        ///     }
        /// </remarks>
        /// <param name="idQuery"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidQuery")]
        public List<s_PlaceHolderTemplateDto> GetByidQuery(int idQuery)
        {
            var items = _s_PlaceHolderTemplateService.GetByidQuery(idQuery);
            return items;
        }

        /// <summary>
        /// Delete record of s_PlaceHolderTemplate with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetOneById")]
        public s_PlaceHolderTemplateDto GetOneById(int id)
        {
            var item = _s_PlaceHolderTemplateService.GetOneByKey(id);
            return item;
        }
        /// <summary>
        /// Delete record of s_PlaceHolderTemplate with requested id and all s_PlaceHolderGroupsMtm conected to it
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            var toDelete = _s_PlaceHolderGroupsMtmService.GetByidPlaceHolder(id);
            foreach (var mtm in toDelete)
            {
                _s_PlaceHolderGroupsMtmService.Remove(mtm.id);
            }
            _s_PlaceHolderTemplateService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
