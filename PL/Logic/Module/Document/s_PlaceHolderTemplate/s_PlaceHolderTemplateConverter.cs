using AppDocument.DocTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.DocTemplate
{
    public class s_PlaceHolderTemplateConverter : IModelConverter<s_PlaceHolderTemplateModel, s_PlaceHolderTemplateDto>
    {
        public s_PlaceHolderTemplateDto ToDto(s_PlaceHolderTemplateModel model)
        {
            var result = new s_PlaceHolderTemplateDto
            {
		id = model.Id,
		name = model.name,
		description = model.description,
		code = model.code,
		idQuery = model.idQuery
            };
            return result;
        }

        
    }
}
