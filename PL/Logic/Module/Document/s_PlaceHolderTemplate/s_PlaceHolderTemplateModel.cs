using AppDocument.DocTemplate;
using PLDocument.Base;
using System;
using System.Collections.Generic;
using PLDocument.General;

namespace PLDocument.DocTemplate
{
    public class s_PlaceHolderTemplateModel : BaseModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int Id { get; set; }
        // start dictionary  Not Null 
        public int idQuery { get; set; }
        public string idQueryNavName { get; set; }
        public List<s_PlaceHolderGroupsMtmModel> s_PlaceHolderGroupsMtms { get; set; }
        //end dictionary  

        public override bool IsNew()
        {
            return Id == 0;
        }

    }
}
