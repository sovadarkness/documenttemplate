using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using System;
using AppDocument.General;
using LayerTransmitter.Wrapper;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.General;
using System.Collections.Generic;
using System.Linq;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_QueryController : ControllerBase
    {
        private readonly Is_QueryService _s_QueryService;
        private readonly IModelConverter<s_QueryModel, s_QueryDto> _converter;

        public s_QueryController(Is_QueryService service, IModelConverter<s_QueryModel, s_QueryDto> converter)
        {
            _s_QueryService = service;
            _converter = converter;
        }

        /// <summary>
        /// Add Or Update instance of s_Query
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        "id": "int"
        ///        "name": "string",
        ///        "description": "string",
        ///        "code": "string",
        ///        "query": "string",
        ///        "idQueryType": "int",
        ///        "idDBMS": "int"
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_QueryModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_QueryService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_QueryService.Update(addDto);
                return updated;
            }
        }

        /// <summary>
        ///  Returns all s_Query
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_QueryDto> GetAll()
        {
            var items = _s_QueryService.GetAll();
            return items;
        }

        /// <summary>
        ///  Returns all s_Query with idQueryTypeNavCode == "Filter"
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFilters")]
        public List<s_QueryDto> GetFilters()
        {
            var items = _s_QueryService.GetAll().Where(x=>x.idQueryTypeNavCode == "Filter").ToList(); //TODO
            return items;
        }

        /// <summary>
        /// Returns all s_Query with requested idQueryType
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idQueryType": "int"
        ///     }
        /// </remarks>
        /// <param name="idQueryType"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidQueryType")]
        public List<s_QueryDto> GetByidQueryType(int idQueryType)
        {
            var items = _s_QueryService.GetByidQueryType(idQueryType);
            return items;
        }

        /// <summary>
        /// Returns all s_Query with requested idDBMS
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idDBMS": "int"
        ///     }
        /// </remarks>
        /// <param name="idDBMS"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidDBMS")]
        public List<s_QueryDto> GetByidDBMS(int idDBMS)
        {
            var items = _s_QueryService.GetByidDBMS(idDBMS);
            return items;
        }

        /// <summary>
        /// Delete record of s_Query with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetOneById")]
        public s_QueryDto GetOneById(int id)
        {
            var item = _s_QueryService.GetOneByKey(id);
            return item;
        }

        /// <summary>
        /// Delete record of s_Query with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_QueryService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
