using AppDocument.General;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.General
{
    public class s_QueryModel : BaseModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public string query { get; set; }
        public int id { get; set; }
        // start dictionary  Not Null 
        public int idQueryType { get; set; }
        public string idQueryTypeNavName { get; set; }
        //end dictionary  
        // start dictionary  Not Null 
        public int idDBMS { get; set; }
        public string idDBMSNavName { get; set; }
        //end dictionary  

        public override bool IsNew()
        {
            return id == 0;
        }

    }
}
