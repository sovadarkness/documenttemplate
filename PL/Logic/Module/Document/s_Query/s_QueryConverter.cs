using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.General
{
    public class s_QueryConverter : IModelConverter<s_QueryModel, s_QueryDto>
    {
        public s_QueryDto ToDto(s_QueryModel model)
        {
            var result = new s_QueryDto
            {
                id = model.id,
                name = model.name,
                description = model.description,
                code = model.code,
                query = model.query,
                idQueryType = model.idQueryType,
                idDBMS = model.idDBMS
            };
            return result;
        }


    }
}
