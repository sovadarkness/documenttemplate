using AppDocument.DocTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.DocTemplate
{
    public class s_TemplateDocumentPlaceholderConverter : IModelConverter<s_TemplateDocumentPlaceholderModel, s_TemplateDocumentPlaceholderDto>
    {
        public s_TemplateDocumentPlaceholderDto ToDto(s_TemplateDocumentPlaceholderModel model)
        {
            var result = new s_TemplateDocumentPlaceholderDto
            {
		id = model.Id,
		idTemplateDocument = model.idTemplateDocument,
		idPlaceholder = model.idPlaceholder
            };
            return result;
        }

        
    }
}
