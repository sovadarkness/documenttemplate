using Microsoft.AspNetCore.Http;
using PLDocument.Base;
using System;

namespace PLDocument.DocTemplate
{
    public class s_TemplateDocumentPlaceholderModel: BaseModel
    {
		public int Id { get; set; }
	// start dictionary  Not Null 
        public int idTemplateDocument { get; set; }    
        public string idTemplateDocumentNavName { get; set; }
	//end dictionary  
	// start dictionary  Not Null 
        public int idPlaceholder { get; set; }    
        public string idPlaceholderNavName { get; set; }
	//end dictionary  


        public override bool IsNew()
        {
            return Id == 0;
        }
    }
}
