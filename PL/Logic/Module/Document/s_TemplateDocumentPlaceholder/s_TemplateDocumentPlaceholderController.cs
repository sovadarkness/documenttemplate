using AppDocument.DocTemplate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using LayerTransmitter.Wrapper;
using PLDocument.DocTemplate;
using System.Collections.Generic;
using PLDocument.Base;


namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_TemplateDocumentPlaceholderController : ControllerBase
    {

        private readonly Is_TemplateDocumentPlaceholderService _s_TemplateDocumentPlaceholderService;
        private readonly IModelConverter<s_TemplateDocumentPlaceholderModel, s_TemplateDocumentPlaceholderDto> _converter;
        public s_TemplateDocumentPlaceholderController(Is_TemplateDocumentPlaceholderService service, IModelConverter<s_TemplateDocumentPlaceholderModel, s_TemplateDocumentPlaceholderDto> converter)
        {
            _s_TemplateDocumentPlaceholderService = service;
            _converter = converter;
        }
        /// <summary>
        ///  Returns all s_TemplateDocumentPlaceholder
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_TemplateDocumentPlaceholderDto> GetAll()
        {
            var items = _s_TemplateDocumentPlaceholderService.GetAll();
            return items;
        }

        /// <summary>
        /// Add Or Update instance of s_TemplateDocumentPlaceholder
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        "id": "int"
        ///        "idTemplateDocument": "int",
        ///        "idPlaceholder": "int",
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_TemplateDocumentPlaceholderModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_TemplateDocumentPlaceholderService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_TemplateDocumentPlaceholderService.Update(addDto);
                return updated;
            }
        }

        /// <summary>
        /// Delete record of s_TemplateDocumentPlaceholder with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetOneById")]
        public s_TemplateDocumentPlaceholderDto GetOneById(int id)
        {
            var item = _s_TemplateDocumentPlaceholderService.GetOneByKey(id);
            return item;
        }

        /// <summary>
        /// Delete record of s_TemplateDocumentPlaceholder with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_TemplateDocumentPlaceholderService.Remove(id);
            return Ok(new { result = "true" });
        }

        /// <summary>
        /// Returns all s_TemplateDocumentPlaceholder with requested idTemplateDocument
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idTemplateDocument": "int"
        ///     }
        /// </remarks>
        /// <param name="idTemplateDocument"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidTemplateDocument")]
        public List<s_TemplateDocumentPlaceholderDto> GetByidTemplateDocument(int idTemplateDocument)
        {
            var items = _s_TemplateDocumentPlaceholderService.GetByidTemplateDocument(idTemplateDocument);
            return items;
        }

        /// <summary>
        /// Returns all s_TemplateDocumentPlaceholder with requested idPlaceholder
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idPlaceholder": "int"
        ///     }
        /// </remarks>
        /// <param name="idPlaceholder"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetByidPlaceholder")]
        public List<s_TemplateDocumentPlaceholderDto> GetByidPlaceholder(int idPlaceholder)
        {
            var items = _s_TemplateDocumentPlaceholderService.GetByidPlaceholder(idPlaceholder);
            return items;
        }



    }

}
