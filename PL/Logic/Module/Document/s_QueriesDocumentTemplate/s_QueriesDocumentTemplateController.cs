using AppDocument.DocTemplate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using LayerTransmitter.Wrapper;
using PLDocument.DocTemplate;
using System.Collections.Generic;
using PLDocument.Base;


namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_QueriesDocumentTemplateController : ControllerBase
    {

        private readonly Is_QueriesDocumentTemplateService _s_QueriesDocumentTemplateService;
        private readonly IModelConverter<s_QueriesDocumentTemplateModel, s_QueriesDocumentTemplateDto> _converter;
        public s_QueriesDocumentTemplateController(Is_QueriesDocumentTemplateService service, IModelConverter<s_QueriesDocumentTemplateModel, s_QueriesDocumentTemplateDto> converter)
        {
            _s_QueriesDocumentTemplateService = service;
            _converter = converter;
        }
        /// <summary>
        ///  Returns all s_QueriesDocumentTemplate
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_QueriesDocumentTemplateDto> GetAll()
        {
            var items = _s_QueriesDocumentTemplateService.GetAll();
            return items;
        }

        /// <summary>
        /// Add Or Update instance of s_QueriesDocumentTemplate
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        "id": "int"
        ///        "idDocumentTemplate": "int",
        ///        "idQuery": "int"
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_QueriesDocumentTemplateModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_QueriesDocumentTemplateService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_QueriesDocumentTemplateService.Update(addDto);
                return updated;
            }
        }
        /// <summary>
        /// Delete record of s_QueriesDocumentTemplate with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        [HttpGet]
        [Route("GetOneById")]
        public s_QueriesDocumentTemplateDto GetOneById(int id)
        {
            var item = _s_QueriesDocumentTemplateService.GetOneByKey(id);
            return item;
        }
        /// <summary>
        /// Delete record of s_QueriesDocumentTemplate with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        "id": "int"
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_QueriesDocumentTemplateService.Remove(id);
            return Ok(new { result = "true" });
        }
        /// <summary>
        /// Returns all s_QueriesDocumentTemplate with requested idDocumentTemplate
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idDocumentTemplate": "int"
        ///     }
        /// </remarks>
        /// <param name="idDocumentTemplate"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidDocumentTemplate")]
        public List<s_QueriesDocumentTemplateDto> GetByidDocumentTemplate(int idDocumentTemplate)
        {
            var items = _s_QueriesDocumentTemplateService.GetByidDocumentTemplate(idDocumentTemplate);
            return items;
        }

        /// <summary>
        /// Returns all s_QueriesDocumentTemplate with requested idQuery
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        "idQuery": "int"
        ///     }
        /// </remarks>
        /// <param name="idQuery"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidQuery")]
        public List<s_QueriesDocumentTemplateDto> GetByidQuery(int idQuery)
        {
            var items = _s_QueriesDocumentTemplateService.GetByidQuery(idQuery);
            return items;
        }



    }

}
