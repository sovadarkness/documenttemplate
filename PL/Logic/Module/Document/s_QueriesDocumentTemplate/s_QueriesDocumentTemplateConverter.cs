using AppDocument.DocTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.DocTemplate
{
    public class s_QueriesDocumentTemplateConverter : IModelConverter<s_QueriesDocumentTemplateModel, s_QueriesDocumentTemplateDto>
    {
        public s_QueriesDocumentTemplateDto ToDto(s_QueriesDocumentTemplateModel model)
        {
            var result = new s_QueriesDocumentTemplateDto
            {
		id = model.Id,
		idDocumentTemplate = model.idDocumentTemplate,
		idQuery = model.idQuery
            };
            return result;
        }

        
    }
}
