using Microsoft.AspNetCore.Http;
using PLDocument.Base;
using System;

namespace PLDocument.DocTemplate
{
    public class s_QueriesDocumentTemplateModel: BaseModel
    {
		public int Id { get; set; }
	// start dictionary  Not Null 
        public int idDocumentTemplate { get; set; }    
        public string idDocumentTemplateNavName { get; set; }
	//end dictionary  
	// start dictionary  Not Null 
        public int idQuery { get; set; }    
        public string idQueryNavName { get; set; }
	//end dictionary  


        public override bool IsNew()
        {
            return Id == 0;
        }
    }
}
