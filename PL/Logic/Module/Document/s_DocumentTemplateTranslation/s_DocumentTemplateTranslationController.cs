using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using System;
using AppDocument.DocTemplate;
using LayerTransmitter.Wrapper;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.DocTemplate;
using System.Collections.Generic;
using WkHtmlToPdfDotNet.Contracts;
using Microsoft.AspNetCore.Hosting;
using WkHtmlToPdfDotNet;
using PLDocument.Providers;

namespace PLDocument.Controllers
{
    [SessionFilter]
    //[Authorize] //TODO
    [Route("api/[controller]")]
    [ApiController]
    public class s_DocumentTemplateTranslationController : ControllerBase
    {
        private readonly IWebHostEnvironment _appEnvironment;

        private readonly IConverter _pdfConverter;
        private readonly Is_DocumentTemplateTranslationService _s_DocumentTemplateTranslationService;
        private readonly IModelConverter<s_DocumentTemplateTranslationModel, s_DocumentTemplateTranslationDto> _converter;
        //private readonly IFileService _fileService;
        public s_DocumentTemplateTranslationController(Is_DocumentTemplateTranslationService service,
            IModelConverter<s_DocumentTemplateTranslationModel, s_DocumentTemplateTranslationDto> converter,
            IWebHostEnvironment appEnvironment,
            IConverter pdfConverter
            )
        {
            _s_DocumentTemplateTranslationService = service;
            _converter = converter;
            _appEnvironment = appEnvironment;
            _pdfConverter = pdfConverter;

            //_fileService = fileService;
        }
        /// <summary>
        /// AddOrUpdate instance of s_DocumentTemplateTranslation
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        id: 0
        ///        template: "<p>������� ������� test</p>",
        ///        idDocumentTemplate: 20,
        ///        idLanguage: 2
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_DocumentTemplateTranslationModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_DocumentTemplateTranslationService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_DocumentTemplateTranslationService.Update(addDto);
                return updated;
            }
        }
        /// <summary>
        ///  Returns all s_DocumentTemplateTranslation
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_DocumentTemplateTranslationDto> GetAll()
        {
            var items = _s_DocumentTemplateTranslationService.GetAll();
            return items;
        }

        /// <summary>
        ///  Returns all s_DocumentTemplateTranslation available for subscribtion
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSubscription")]
        public List<s_DocumentTemplateTranslationDto> GetSubscription()
        {
            var items = _s_DocumentTemplateTranslationService.GetSubscription();
            return items;
        }

        /// <summary>
        /// Returns all s_DocumentTemplateTranslation with requested idDocumentTemplate
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        idDocumentTemplate: 4
        ///     }
        /// </remarks>
        /// <param name="idDocumentTemplate"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidDocumentTemplate")]
        public List<s_DocumentTemplateTranslationDto> GetByidDocumentTemplate(int idDocumentTemplate)
        {
            var items = _s_DocumentTemplateTranslationService.GetByidDocumentTemplate(idDocumentTemplate);
            return items;
        }
        /// <summary>
        /// Returns all s_DocumentTemplateTranslation with requested idLanguage
        /// </summary>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        idLanguage: 2
        ///     }
        /// </remarks>
        /// <param name="idLanguage"></param>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByidLanguageAndIdTemplate")]
        public s_DocumentTemplateTranslationDto GetByidLanguageAndIdTemplate(int idLanguage, int idTemplate)
        {
            var items = _s_DocumentTemplateTranslationService.GetByidLanguageAndIdTemplate(idLanguage, idTemplate);
            return items;
        }

        /// <summary>
        /// Delete record of s_DocumentTemplateTranslation with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOneById")]
        public s_DocumentTemplateTranslationDto GetOneById(int id)
        {
            var item = _s_DocumentTemplateTranslationService.GetOneByKey(id);
            return item;
        }

        /// <summary>
        /// Delete record of s_DocumentTemplateTranslation with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_DocumentTemplateTranslationService.Remove(id);
            return Ok(new { result = "true" });
        }

        /// <summary>
        /// Returns Object { template = html, code = "template" }, where html is string in html format that contains the body 
        /// of document with all placeholders. 
        /// template parameter is id of requested s_DocumentTemplateTranslation 
        /// </summary>
        ///<remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        template: 1
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns object { template = html, code = "template" } </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("GetFilledTemplate")]
        public object GetFilledTemplate(DocModelWithHtml model)
        {
            var p = new Dictionary<string, object> { };
            if (model.parameters == null || model.parameters.Count == 0)
                model.parameters = p;

            var html = _s_DocumentTemplateTranslationService.GetFilledTemplate(model.idTemplate, model.parameters, model.language);
            return new { template = html, code = "template" };
        }

        /// <summary>
        /// generates filled document with all placeholders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetFilledDocumentHtml")]
        public object GetFilledDocumentHtml(DocModelWithHtml model)
        {
            var p = new Dictionary<string, object> { };
            if (model.parameters == null || model.parameters.Count == 0)
                model.parameters = p;

            var res = _s_DocumentTemplateTranslationService.GetFilledTemplateHtml(model.idTemplate, model.html, model.parameters);
            return new { template = res, code = "template" };
        }

        /// <summary>
        /// Generate Document in pdf format, return guid id this document
        /// </summary>
        /// <param name="model"></param>
        ///<remarks>
        /// Sample of post data
        ///
        ///     {
        ///        idTemplate: 4
        ///        html: "string",
        ///        fileName: "testFile.pdf",
        ///     }
        /// </remarks>
        /// <response code="200">Ok, returns object { template = html, code = "template" } </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("ConvertPdfByDocument")]
        public object ConvertPdfByDocument(DocModelWithHtml model)
        {
            var name = Guid.NewGuid().ToString();
            var path2 = PathHelper.PathCombine(_appEnvironment.WebRootPath, @"\Files\TempPdf\", name);

            var p = new Dictionary<string, object> { };
            if (model.parameters == null || model.parameters.Count == 0)
                model.parameters = p;

            var html = _s_DocumentTemplateTranslationService.GetFilledTemplate(model.idTemplate, model.parameters, model.language);
            var type = PaperKind.A4;
            if (model.type == "albom")
            {
                type = PaperKind.A4Rotated;
            }

            var doc = new HtmlToPdfDocument()
            {
                GlobalSettings = {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = type,
                    Margins = new MarginSettings() { Bottom = 18, Top = 18, Left = 18, Right = 18 },
                    Out = path2,
                },
                Objects = {
                    new ObjectSettings() {
                        HtmlContent = html,
                        WebSettings = { DefaultEncoding = "utf-8" },
                    }
                }
            };
            _pdfConverter.Convert(doc);


            return new { guid = name };

        }

        /// <summary>
        /// Donload file with requested guid
        /// </summary>
        ///<remarks>
        /// Sample of requesrtdata
        ///
        ///     {
        ///        guid: "22345200-abe8-4f60-90c8-0d43c5f6c0f6",
        ///        fileName: "example.pdf"
        ///     }
        /// </remarks>
        /// <response code="200">Ok, returns object { template = html, code = "template" } </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTempFile")]
        public IActionResult GetTempFile(string guid, string fileName)
        {
            //string name = null;
            //try
            //{
            //    var doc = _s_DocumentTemplateTranslationService.GetOneByKey(idDocTranslate);
            //    name = doc?.idDocumentTemplateNavName;
            //}
            //catch (Exception ex)
            //{

            //}
            
            var path = PathHelper.PathCombine(@"\Files\TempPdf\", guid);
            var stream = System.IO.File.OpenRead(PathHelper.PathCombine(_appEnvironment.WebRootPath, path));
            return new FileStreamResult(stream, "application/octet-stream")
            {
                FileDownloadName = fileName != null ? fileName + ".pdf" : "new file.pdf"
            };
        }


        /// <summary>
        /// Convert Document in pdf format, return guid id this document
        /// </summary>
        /// <param name="model"></param>
        ///<remarks>
        /// Sample of post data
        ///
        ///     {
        ///        idTemplate: 20
        ///        html: "<p>Text in html format</p>",
        ///        fileName: "string",
        ///     }
        /// </remarks>
        /// <response code="200">Ok, returns object { template = html, code = "template" } </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("ConvertToPdf")]
        public object ConvertToPdf(DocModelWithHtml model, [FromServices] IConverter converter)
        {
            var name = Guid.NewGuid().ToString();
            var path = PathHelper.PathCombine(_appEnvironment.WebRootPath, @"\Files\TempPdf\", name);

            //var converter = new SynchronizedConverter(new PdfTools());
            //var clientAppUrl = _configuration.GetValue<string>("ClientAppUrl");
            //TODO del
            //clientAppUrl = "http://localhost:3000/";
            //var n = model.html.IndexOf(@"/static/");
            //model.html = model.html.Replace(@"src=""/static/", $@"src=""{clientAppUrl}/static/");
            var doc = new HtmlToPdfDocument()
            {
                GlobalSettings = {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings() { Bottom = 18, Top = 18, Left = 18, Right = 18 },
                    Out = path,
                },
                Objects = {
                    new ObjectSettings() {
                        HtmlContent = model.html,
                        WebSettings = { DefaultEncoding = "utf-8" },
                    }
                }
            };

            //converter.Convert(doc);

            return new { guid = name };
        }
    }

    public class DocModelWithHtml
    {
        public int idTemplate { get; set; }
        public string language { get; set; }
        public string html { get; set; }
        public string type { get; set; }
        public string fileName { get; set; }
        public Dictionary<string, object> parameters { get; set; }
        //public int idVisit { get; set; }
    }
}
