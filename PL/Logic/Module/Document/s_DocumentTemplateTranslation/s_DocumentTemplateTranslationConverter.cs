using AppDocument.DocTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.DocTemplate
{
    public class s_DocumentTemplateTranslationConverter : IModelConverter<s_DocumentTemplateTranslationModel, s_DocumentTemplateTranslationDto>
    {
        public s_DocumentTemplateTranslationDto ToDto(s_DocumentTemplateTranslationModel model)
        {
            var result = new s_DocumentTemplateTranslationDto
            {
		id = model.id,
		template = model.template,
		idDocumentTemplate = model.idDocumentTemplate,
		idLanguage = model.idLanguage
            };
            return result;
        }

        
    }
}
