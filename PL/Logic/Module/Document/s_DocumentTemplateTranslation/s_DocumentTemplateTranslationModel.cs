using AppDocument.DocTemplate;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.DocTemplate
{
    public class s_DocumentTemplateTranslationModel: BaseModel
    {
			public string template { get; set; }
		public int id { get; set; }
	// start dictionary  Not Null 
        public int idDocumentTemplate { get; set; }    
        public string idDocumentTemplateNavName { get; set; }
	//end dictionary  
	// start dictionary  Not Null 
        public int idLanguage { get; set; }    
        public string idLanguageNavName { get; set; }
	//end dictionary  
      
	public override bool IsNew()
        {
            return id == 0;
        }

    }
}
