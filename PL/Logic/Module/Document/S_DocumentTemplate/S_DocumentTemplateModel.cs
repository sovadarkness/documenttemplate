using AppDocument.General;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.General
{
    public class s_DocumentTemplateModel : BaseModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int Id { get; set; }
        public int? idCustomSvgIcon { get; set; }
        public string idCustomSvgIconPreview { get; set; }
        public string iconColor { get; set; }
        public int? idDocumentTyplateType { get; set; }
        public bool? useDiagnos { get; set; }
        public int[] Specialitys { get; set; }


        public override bool IsNew()
        {
            return Id == 0;
        }

    }
}
