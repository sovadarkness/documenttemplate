using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.General
{
    public class s_DocumentTemplateConverter : IModelConverter<s_DocumentTemplateModel, s_DocumentTemplateDto>
    {
        public s_DocumentTemplateDto ToDto(s_DocumentTemplateModel model)
        {
            var result = new s_DocumentTemplateDto
            {
                id = model.Id,
                name = model.name,
                description = model.description,
                code = model.code,
                iconColor = model.iconColor,
                idCustomSvgIcon = model.idCustomSvgIcon,
                idCustomSvgIconPreview = model.idCustomSvgIconPreview,
                idDocumentTyplateType = model.idDocumentTyplateType,
                useDiagnos = model.useDiagnos,
            };
            return result;
        }


    }
}
