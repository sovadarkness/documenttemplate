using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using AppDocument.General;
using PLDocument.Base;
using PLDocument.General;
using System.Collections.Generic;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class s_DocumentTemplateController : ControllerBase
    {
        private readonly Is_DocumentTemplateService _s_DocumentTemplateService;
        private readonly IModelConverter<s_DocumentTemplateModel, s_DocumentTemplateDto> _converter;


        public s_DocumentTemplateController(Is_DocumentTemplateService service, IModelConverter<s_DocumentTemplateModel, s_DocumentTemplateDto> converter)
        {
            _s_DocumentTemplateService = service;
            _converter = converter;

        }
        /// <summary>
        /// AddOrUpdate instance of s_DocumentTemplate
        /// </summary>
        /// <remarks>
        /// Sample of post data
        ///
        ///     {
        ///        id: 0
        ///        name: "New Document",
        ///        description: "description",
        ///        code: "Doc01",
        ///        queueNumber: 1,
        ///        idCustomSvgIcon: 1,
        ///        iconColor: "FFFF",
        ///        idDocumentTyplateType: 2,
        ///        useDiagnos: false,
        ///        Specialitys: [1,2,3]
        ///     }
        /// </remarks>
        /// <param name="model"></param>
        /// <response code="200">Ok, returns id of Updated/Created record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(s_DocumentTemplateModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _s_DocumentTemplateService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _s_DocumentTemplateService.Update(addDto);
                return updated;
            }
        }
        /// <summary>
        ///  Returns all s_DocumentTemplate
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<s_DocumentTemplateDto> GetAll()
        {
            var items = _s_DocumentTemplateService.GetAll();
            return items;
        }
        /// <summary>
        /// Returns list s_DocumentTemplate with s_DocumentTemplateTranslation in "RU" language
        /// </summary>
        /// <response code="200">Ok, returns List of record </response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllTemplate")]
        public List<s_DocumentTemplateDto> GetAllTemplate()
        {
            var items = _s_DocumentTemplateService.GetAllTemplate();
            return items;
        }
        /// <summary>
        /// Return one record of s_DocumentTemplate
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <response code="200">Ok, returns record of DocumentTemplateCategory with requested id</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOneById")]
        public s_DocumentTemplateDto GetOneById(int id)
        {
            var item = _s_DocumentTemplateService.GetOneByKey(id);
            return item;
        }
        /// <summary>
        /// Delete record of s_DocumentTemplate with requested id
        /// </summary>
        /// <param name="id"></param>
        /// <remarks>
        /// 
        /// Request parameters
        ///
        ///     {
        ///        id: 1
        ///     }
        /// </remarks>
        /// <request></request>
        /// <response code="200">Ok</response>
        /// <response code="500">Internal Server Error </response>
        /// <response code="401">Unautorised(Wrong token)</response>
        /// <response code="404">Bad Request</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _s_DocumentTemplateService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
