using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using System;
using AppDocument.General;
using LayerTransmitter.Wrapper;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.General;
using System.Collections.Generic;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomSvgIconController : ControllerBase
    {
        private readonly ICustomSvgIconService _CustomSvgIconService;
        private readonly IModelConverter<CustomSvgIconModel, CustomSvgIconDto> _converter;

        public CustomSvgIconController(ICustomSvgIconService service, IModelConverter<CustomSvgIconModel, CustomSvgIconDto> converter)
        {
            _CustomSvgIconService = service;
            _converter = converter;
        }

        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(CustomSvgIconModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _CustomSvgIconService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _CustomSvgIconService.Update(addDto);
                return updated;
            }
        }

        [HttpGet]
        [Route("GetAll")]
        public List<CustomSvgIconDto> GetAll()
        {
            var items = _CustomSvgIconService.GetAll();
            return items;
        }

        [HttpGet]
        [Route("GetForTable")]
        public List<CustomSvgIconDto> GetForTable(string tableName)
        {
            var items = _CustomSvgIconService.GetByTableName(tableName);
            return items;
        }



        [HttpGet]
        [Route("GetOneById")]
        public CustomSvgIconDto GetOneById(int id)
        {
            var item = _CustomSvgIconService.GetOneByKey(id);
            return item;
        }

        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _CustomSvgIconService.Remove(id);
            return Ok(new { result = "true" });
        }
    }
}
