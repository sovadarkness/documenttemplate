using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.General
{
    public class CustomSvgIconConverter : IModelConverter<CustomSvgIconModel, CustomSvgIconDto>
    {
        public CustomSvgIconDto ToDto(CustomSvgIconModel model)
        {
            var result = new CustomSvgIconDto
            {
		id = model.id,
		name = model.name,
		svgPath = model.svgPath,
		usedTables = model.usedTables
            };
            return result;
        }

        
    }
}
