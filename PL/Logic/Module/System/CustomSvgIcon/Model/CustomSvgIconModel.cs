using AppDocument.General;
using PLDocument.Base;
using System;
using System.Collections.Generic;

namespace PLDocument.General
{
    public class CustomSvgIconModel: BaseModel
    {
			public string name { get; set; }
			public string svgPath { get; set; }
			public string usedTables { get; set; }
		public int id { get; set; }
      
	public override bool IsNew()
        {
            return id == 0;
        }

    }
}
