using PLDocument.Base;
using System;
using System.Globalization;

namespace PLDocument.General
{
    public class CommonSettingsModel : BaseModel
    {
        public int id { get; set; }
		public string name { get; set; }
		public string nameky { get; set; }
		public string value { get; set; }
		public string valueky { get; set; }
		public string code { get; set; }
		public string description { get; set; }
		

        public override bool IsNew()
        {
            return id == 0;
        }

    }
}
