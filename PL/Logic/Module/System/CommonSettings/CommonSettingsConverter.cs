using AppDocument.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PLDocument.Base;

namespace PLDocument.General
{
    public class CommonSettingsConverter : IModelConverter<CommonSettingsModel, CommonSettingsDto>
    {
        public CommonSettingsDto ToDto(CommonSettingsModel model)
        {
            var result = new CommonSettingsDto
            {
                id = model.id,
				name = model.name,
				nameky = model.nameky,
				value = model.value,
				valueky = model.valueky,
				code = model.code,
				description = model.description,
				
            };
            return result;
        }
    }
}
