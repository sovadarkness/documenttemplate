using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PLDocument.ActionFilters;
using AppDocument.General;
using PLDocument.Base;
using AppDocument.Base;
using PLDocument.General;
using System.Collections.Generic;

namespace PLDocument.Controllers
{
    [SessionFilter]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CommonSettingsController : ControllerBase
    {
        private readonly ICommonSettingsService _CommonSettingsService;
        private readonly IModelConverter<CommonSettingsModel, CommonSettingsDto> _converter;

        public CommonSettingsController(ICommonSettingsService service, IModelConverter<CommonSettingsModel, CommonSettingsDto> converter)
        {
            _CommonSettingsService = service;
            _converter = converter;
        }

        /// <summary>
        /// �������� ��� �������� ��������� ����� ���������
        /// </summary>
        /// <remarks>
        /// <param name="model"></param>
        /// Sample of post data
        ///
        ///     {
        ///        "lastUpdatedUser": "string",
        ///        "lastUpdatedTime": "2023-03-23T06:36:45.904Z",
        ///        "strlastUpdatedTime": "string",
        ///        "id": 0,
        ///        "name": "string",
        ///        "description": "string",
        ///        "code": "string",
        ///        "nameky": "string",
        ///        "value": "string",
        ///        "valueky": "string",
        ///     }
        /// </remarks>
        /// <response code="200">Ok, ���������� id �����������/��������� ������ </response>
        /// <response code="500">���������� ������ ������� </response>
        /// <response code="401">���������������� (�������� �����)</response>
        /// <response code="404">�������� ������</response>
        /// <returns></returns>
        [HttpPost]
        [Route("AddOrUpdate")]
        public int AddOrUpdate(CommonSettingsModel model)
        {
            if (model.IsNew())
            {
                var addDto = _converter.ToDto(model);
                var added = _CommonSettingsService.Add(addDto);
                return added;
            }
            else
            {
                var addDto = _converter.ToDto(model);
                var updated = _CommonSettingsService.Update(addDto);
                return updated;
            }
        }

        /// <summary>
        /// ������� ��� ������ ����� ���������
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///
        /// </remarks>
        /// <response code="200">Ok, ���������� ������ ����� ���������</response>
        /// <response code="500">���������� ������ �������</response>
        /// <response code="401">���������������� (�������� �����)</response>
        /// <response code="404">�������� ������</response>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public List<CommonSettingsDto> GetAll()
        {
            var items = _CommonSettingsService.GetAll();
            return items;
        }

        /// <summary>
        /// ������� id ������ ������������ ����� ���������
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <response code="200">Ok, ���������� ����� ���������</response>
        /// <response code="500">���������� ������ �������</response>
        /// <response code="404">�������� ������</response>
        /// <response code="401">���������������� (�������� �����)</response>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOneById")]
        public CommonSettingsDto GetOneById(int id)
        {
            var item = _CommonSettingsService.GetOneByKey(id);
            return item;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetOneByCode")]
        public CommonSettingsDto GetOneByCode(string code)
        {
            var item = _CommonSettingsService.GetOneByCode(code);
            return item;
        }

        /// <summary>
        /// ������� ������������ ������ � ����������� id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Ok</response>
        /// <response code="500">���������� ������ �������</response>
        /// <response code="404">�������� ������</response>
        /// <response code="401">���������������� (�������� �����)</response>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] int id)
        {
            _CommonSettingsService.Remove(id);
            return Ok(new { result = "true" });
        }

           
    }
}
