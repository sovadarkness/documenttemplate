﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PLDocument.Logic
{
    public class AtsHub : Hub<ITypedHubClient>
    {
        public void Send(string message)
        {
            Clients.All.BroadcastMessage(message);
        }
    }

    public interface ITypedHubClient
    {
        Task BroadcastMessage(string message);
        Task SendNotifications(string message);
        Task NewCall(string message);
        Task Redirect(string message, string sip);
        Task Hold(string sip);
        Task TakeOffHold(string sip);
        Task Hangup(string sip);
        Task Answer(string sip);
    }
}
