#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/aspnet:3.1-alpine AS base
WORKDIR /app
EXPOSE 5000

FROM mcr.microsoft.com/dotnet/sdk:3.1-alpine AS build
WORKDIR /src

COPY ["PL/PL.csproj", "PL/"]
COPY ["DAL/DAL.csproj", "DAL/"]
COPY ["App/App.csproj", "App/"]
COPY ["FileStorage/FileStorage.csproj", "FileStorage/"]
RUN dotnet restore "PL/PL.csproj"
COPY . .
WORKDIR "/src/PL"
RUN dotnet build "PL.csproj" --runtime linux-x64 -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PL.csproj" --runtime linux-x64 --self-contained false -c Release -o /app/publish

FROM base AS final
ENV ASPNETCORE_URLS=http://+:5000
ENV TZ=Asia/Bishkek

ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false
RUN apk add --no-cache icu-libs

RUN apk add --no-cache tzdata
RUN cp /usr/share/zoneinfo/$TZ /etc/localtime
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "PL.dll"]