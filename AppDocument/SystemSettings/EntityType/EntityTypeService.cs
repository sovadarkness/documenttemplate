﻿using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using AppDocument.Base.DtoWrapperErrors;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Linq;

namespace AppDocument.SystemSettings
{
    public partial class EntityTypeService : BaseProtectedCrudService<
            EntityTypeDto,
            int
        >, IEntityTypeService
    {
        public EntityTypeService(ISessionProvider sessionProvider,
            IAccessService accessService,
            IEntityTypeRepository EntityTypeRepository) :
            base(EntityTypeRepository, sessionProvider,
                accessService
                )
        {
        }

        protected override string AddMedtodKey => "EntityType.Add";

        protected override string UpdateMedtodKey => "EntityType.Update";

        protected override string RemoveMedtodKey => "EntityType.Remove";

        protected override string ReadMedtodKey => "EntityType.Read";

         
        
    }
}
