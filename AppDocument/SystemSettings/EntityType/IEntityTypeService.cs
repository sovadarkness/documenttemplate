﻿using Core.Services.BaseCrudService;

namespace AppDocument.SystemSettings
{
    public interface IEntityTypeService: IBaseCrudService<
        EntityTypeDto,
        int
        >
    {
    }
}