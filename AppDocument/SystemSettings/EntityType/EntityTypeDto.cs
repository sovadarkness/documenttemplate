﻿using Core.Abstraction;
using AppDocument.Base;
using System;

namespace AppDocument.SystemSettings
{
    public class EntityTypeDto : IDtoWithId<int>
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int? QueueNumber { get; set; }
        public string LanguageTable { get; set; }
        public bool? HasChecksum { get; set; }
        public string Checksum { get; set; }
        public DateTime? CheckDate { get; set; }
    }
}
