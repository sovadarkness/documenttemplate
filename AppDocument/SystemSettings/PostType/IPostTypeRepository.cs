using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface IPostTypeRepository : IBaseCrudRepository<PostTypeDto, int>
    {

    }
}
