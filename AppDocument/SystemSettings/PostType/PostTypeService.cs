using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.General
{
    public partial class PostTypeService : BaseProtectedCrudService<PostTypeDto, int>, IPostTypeService
    {
        private readonly IPostTypeRepository _PostTypeRepository;
        public PostTypeService(ISessionProvider sessionProvider,
            IAccessService accessService,
            IPostTypeRepository PostTypeRepository) :
            base(PostTypeRepository,
                sessionProvider,
                accessService)
        {
            _PostTypeRepository = PostTypeRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "PostType.Add";

        protected override string UpdateMedtodKey => "PostType.Update";

        protected override string RemoveMedtodKey => "PostType.Remove";

        protected override string ReadMedtodKey => "PostType.Read";
        #endregion

        protected override void ModelValidate(PostTypeDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(PostTypeDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(PostTypeDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(PostTypeDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update



    }
}
