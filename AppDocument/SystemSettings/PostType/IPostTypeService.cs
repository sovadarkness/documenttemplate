using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.General
{
    public interface IPostTypeService: IBaseCrudService<PostTypeDto, int>
    {
	
    }
}
