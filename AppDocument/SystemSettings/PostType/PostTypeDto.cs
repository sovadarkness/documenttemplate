using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.General
{
    public class PostTypeDto : IDtoWithId<int>
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public int? queueNumber { get; set; }
			public string? foregroundColor { get; set; }
			public string? backgroundColor { get; set; }

		public int id { get; set; }
    }
}
