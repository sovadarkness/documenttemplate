﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Core.Validation.Errors;
using LayerTransmitter.Wrapper;

namespace AppDocument
{
    public static class ModuleAccessHelper
    {
        public static class SettingsModule
        {
            public static string AboutClinic => "AboutClinic";
            public static string StructureClinic => "StructureClinic";
            public static string StructureClinicPhysic => "StructureClinicPhysic";
            public static string Specialities => "Specialities";
            public static string Services => "Services";
            public static string AccessRights => "AccessRights";
            public static string Roles => "Roles";
            public static string SystemInformation => "SystemInformation";
            public static string UserInformation => "UserInformation";
        }

        public static class StaffModule
        {
            public static class Employee
            {
                public static string CUD => "StaffModule.EmployeeOld.CUD";
                public static string Read => "StaffModule.EmployeeOld.Read";
            }



            public static string EmployeeOld => "EmployeeOld";
            public static string EmployeeHistory => "EmployeeHistory";
            public static string Post => "Post";
        }

        public static class AccountingModule
        {
            public static string Price => "Price";
            public static string ReportSetting => "ReportSetting";
        }


        public static class WorkSheduleModule
        {
            public static class Schedule
            {
                public static string CUD => "WorkShedule.Schedule.CUD";
                public static string Read => "WorkShedule.Schedule.Read";
            }
            public static class Employee
            {
                public static string SetSchedule => "WorkShedule.EmployeeOld.SetSchedule";
                public static string GetSchedule => "WorkShedule.EmployeeOld.GetSchedule";
                public static string SetWorkEvent => "WorkShedule.EmployeeOld.SetWorkEvent";
                public static string GetWorkEvent => "WorkShedule.EmployeeOld.GetWorkEvent";
            }
            public static class Structure
            {
                public static string SetSchedule => "WorkShedule.Structure.SetSchedule";
                public static string GetSchedule => "WorkShedule.Structure.GetSchedule";
                public static string SetWorkEvent => "WorkShedule.Structure.SetWorkEvent";
                public static string GetWorkEvent => "WorkShedule.Structure.GetWorkEvent";
            }
        }

        public static class Cashbox
        {
            public static string ReadPayment => "Cashbox.ReadPayment";
            public static string CudPayment => "Cashbox.CudPayment";

            public static string OpenCashbox => "Cashbox.CanOpenCashbox";
            public static string CloseCashbox => "Cashbox.CanCloseCashbox";

            public static string GetAvailableCashboxesToConnect => "Cashbox.GetAvailableCashboxesToConnect";
            public static string GetCashboxByCashier => "Cashbox.GetCashboxByCashier";

            public static string TakeMoneyToCashbox => "Cashbox.TakeMoneyToCashbox";
            public static string GiveMoneyFromCashbox => "Cashbox.GiveMoneyFromCashbox";

            public static string TakeMoneyToBalance => "Cashbox.TakeMoneyToBalance";
            public static string GiveMoneyFromBalance => "Cashbox.GiveMoneyFromBalance";

            public static class Invoice
            {
                public static string GetForDate => "Cashbox.Invoice.GetForDate";
            }
        }

        #region System dictionary 
        public static class SystemDictionaries
        {
            public static class ro_Month
            {
                public static string CUD => "SystemDictionaries.ro_Month.CUD";
                public static string Read => "SystemDictionaries.ro_Month.Read";
            }

            public static class ro_WorkEventRepeatDay
            {
                public static string CUD => "SystemDictionaries.ro_WorkEventRepeatDay.CUD";
                public static string Read => "SystemDictionaries.ro_WorkEventRepeatDay.Read";
            }

            public static class WorkEventType
            {
                public static string CUD => "SystemDictionaries.WorkEventType.CUD";
                public static string Read => "SystemDictionaries.WorkEventType.Read";
            }
            public static class WeekDay
            {
                public static string CUD => "SystemDictionaries.WeekDay.CUD";
                public static string Read => "SystemDictionaries.WeekDay.Read";
            }

            public static class WorkEventRepeatType
            {
                public static string CUD => "SystemDictionaries.WorkEventRepeatType.CUD";
                public static string Read => "SystemDictionaries.WorkEventRepeatType.Read";
            }

            public static class Currency
            {
                public static string CUD => "SystemDictionaries.Currency.CUD";
                public static string Read => "SystemDictionaries.Currency.Read";
            }

            public static class VisitType
            {
                public static string CUD => "SystemDictionaries.VisitType.CUD";
                public static string Read => "SystemDictionaries.VisitType.Read";
            }
        }
        #endregion System dictionary

        #region Customer Dictionary
        public static class CustomerDictionaries
        {
            public static class PromoChannel
            {
                public static string CUD => "CustomerDictionaries.PromoChannel.CUD";
                public static string Read => "CustomerDictionaries.PromoChannel.Read";
            }

            public static class Post
            {
                public static string CUD => "CustomerDictionaries.Post.CUD";
                public static string Read => "CustomerDictionaries.Post.Read";
            }

            public static class Speciality
            {
                public static string CUD => "CustomerDictionaries.Speciality.CUD";
                public static string Read => "CustomerDictionaries.Speciality.Read";
            }

            public static class Specialization
            {
                public static string CUD => "CustomerDictionaries.Specialization.CUD";
                public static string Read => "CustomerDictionaries.Specialization.Read";
            }

            public static class ServiceType
            {
                public static string CUD => "CustomerDictionaries.ServiceType.CUD";
                public static string Read => "CustomerDictionaries.ServiceType.Read";
            }

            public static class ServiceCategory
            {
                public static string CUD => "CustomerDictionaries.ServiceCategory.CUD";
                public static string Read => "CustomerDictionaries.ServiceCategory.Read";
            }

            public static class CreditType
            {
                public static string CUD => "CustomerDictionaries.CreditType.CUD";
                public static string Read => "CustomerDictionaries.CreditType.Read";
            }

            public static class CreditCategory
            {
                public static string CUD => "CustomerDictionaries.CreditCategory.CUD";
                public static string Read => "CustomerDictionaries.CreditCategory.Read";
            }

            public static class DebitCategory
            {
                public static string CUD => "CustomerDictionaries.DebitCategory.CUD";
                public static string Read => "CustomerDictionaries.DebitCategory.Read";
            }

            public static class DebitType
            {
                public static string CUD => "CustomerDictionaries.DebitType.CUD";
                public static string Read => "CustomerDictionaries.DebitType.Read";
            }

            public static class PaymentType
            {
                public static string CUD => "CustomerDictionaries.PaymentType.CUD";
                public static string Read => "CustomerDictionaries.PaymentType.Read";
            }
        }
        #endregion Customer Dictionary
    }


}
