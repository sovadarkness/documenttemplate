﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDocument
{
    public interface ISystemSettingProvider
    {
        int DefaultLanguageId { get; }
        int ImageFileTypeId { get; }
        string IdentityModuleApi { get; }
        string IdentityModuleToken { get; }
        string QRServiseModuleApi { get; }
        string DoctorFront { get; }
        public string ScheduledVisitStatus { get; }
        public string StartedVisitStatus { get; }
        public string CanceledVisitStatus { get; }
        public string SuspendedVisitStatus { get; }
        public string FinishedVisitStatus { get; }
        public string ReceptionVisitType { get; }
        public string LiveQueueVisitType { get; }
        //int StructureTypeClinicId { get; }
        Dictionary<string, int> SystemLanguages { get; }
        Dictionary<string, object> SettingsOnClient { get; }
    }
}
