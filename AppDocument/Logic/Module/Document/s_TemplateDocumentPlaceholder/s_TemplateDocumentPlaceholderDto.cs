using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Globalization;

namespace AppDocument.DocTemplate
{
    public class s_TemplateDocumentPlaceholderDto : IDtoWithId<int>
    {
		public int id { get; set; }
	// start dictionary  Not Null 
        public int idTemplateDocument { get; set; }    
        public string idTemplateDocumentNavName { get; set; }
	//end dictionary  
	// start dictionary  Not Null 
        public int idPlaceholder { get; set; }    
        public string idPlaceholderNavName { get; set; }
	//end dictionary  

    }
}
