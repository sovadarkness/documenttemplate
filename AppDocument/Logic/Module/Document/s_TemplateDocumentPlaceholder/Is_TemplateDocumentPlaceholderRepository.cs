using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface Is_TemplateDocumentPlaceholderRepository : IBaseCrudRepository<s_TemplateDocumentPlaceholderDto, int>
    {

        List<s_TemplateDocumentPlaceholderDto> GetByidTemplateDocument(int idTemplateDocument);
        List<s_TemplateDocumentPlaceholderDto> GetByidPlaceholder(int idPlaceholder);
        List<s_PlaceHolderTemplateDto> GetPlaceholderByidTemplateDocument(int idDocument);
        string GetFilledHtml(string html, List<s_PlaceHolderTemplateDto> placeholders, Dictionary<string, object> sqlParameters);
    }
}
