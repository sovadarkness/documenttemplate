using Core.Services.BaseCrudService;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface Is_TemplateDocumentPlaceholderService: IBaseCrudService<s_TemplateDocumentPlaceholderDto,int>
    {
	public List<s_TemplateDocumentPlaceholderDto> GetByidTemplateDocument(int idTemplateDocument);
	public List<s_TemplateDocumentPlaceholderDto> GetByidPlaceholder(int idPlaceholder);
    }
}
