using Core.Abstraction;
using Core.Exceptions;
using Core.Services.BaseCrudService;
using Core.Validation;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq;

namespace AppDocument.DocTemplate
{
    public partial class s_TemplateDocumentPlaceholderService : BaseProtectedCrudService<s_TemplateDocumentPlaceholderDto, int>, Is_TemplateDocumentPlaceholderService
    {
        private Is_TemplateDocumentPlaceholderRepository _s_TemplateDocumentPlaceholderRepository;

        public s_TemplateDocumentPlaceholderService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_TemplateDocumentPlaceholderRepository s_TemplateDocumentPlaceholderRepository) :
            base(s_TemplateDocumentPlaceholderRepository,
                sessionProvider,
                accessService)
        {
            _s_TemplateDocumentPlaceholderRepository = s_TemplateDocumentPlaceholderRepository;
        }


        #region Custom Add
        protected override int CustomAdd(s_TemplateDocumentPlaceholderDto dto)
        {
            return base.CustomAdd(dto);
        }
        protected override void ValidateAdd(s_TemplateDocumentPlaceholderDto dto, List<IExecutionError> errors)
        {
            base.ValidateAdd(dto, errors);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_TemplateDocumentPlaceholderDto dto)
        {
            return base.CustomUpdate(dto);
        }
        protected override void ValidateUpdate(s_TemplateDocumentPlaceholderDto dto, List<IExecutionError> errors)
        {
            base.ValidateUpdate(dto, errors);
        }
        #endregion Custom Update

        #region Custom Delete
        protected override void ValidateDelete(int key, List<IExecutionError> errors)
        {
            base.ValidateDelete(key, errors);
        }
        #endregion Custom Delete

        protected override void ModelValidate(s_TemplateDocumentPlaceholderDto dto, List<IExecutionError> errors)
        {
          
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_TemplateDocumentPlaceholderDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region MtM Methods


        public List<s_TemplateDocumentPlaceholderDto> GetByidTemplateDocument(int idTemplateDocument)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_TemplateDocumentPlaceholderRepository.GetByidTemplateDocument(idTemplateDocument);
        }


        public List<s_TemplateDocumentPlaceholderDto> GetByidPlaceholder(int idPlaceholder)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_TemplateDocumentPlaceholderRepository.GetByidPlaceholder(idPlaceholder);
        }


        #endregion MtM Methods

        #region Access keys
        protected override string AddMedtodKey => "s_TemplateDocumentPlaceholder.Add"; //ModuleAccessHelper.Documents.S_TemplateDocumentPlaceholder.CUD;

        protected override string UpdateMedtodKey => "s_TemplateDocumentPlaceholder.Update"; //ModuleAccessHelper.Documents.S_TemplateDocumentPlaceholder.CUD;

        protected override string RemoveMedtodKey => "s_TemplateDocumentPlaceholder.Remove"; // ModuleAccessHelper.Documents.S_TemplateDocumentPlaceholder.CUD;

        protected override string ReadMedtodKey => "s_TemplateDocumentPlaceholder.Read"; // ModuleAccessHelper.Documents.S_TemplateDocumentPlaceholder.Read;
        #endregion
    }
}
