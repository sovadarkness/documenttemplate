using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.DocTemplate
{
    public interface IroDocumentTemplateCategoryService: IBaseCrudService<roDocumentTemplateCategoryDto, int>
    {
	
    }
}
