using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.DocTemplate
{
    public partial class roDocumentTemplateCategoryService : BaseProtectedCrudService<roDocumentTemplateCategoryDto, int>, IroDocumentTemplateCategoryService
    {
        private readonly IroDocumentTemplateCategoryRepository _roDocumentTemplateCategoryRepository;
        public roDocumentTemplateCategoryService(ISessionProvider sessionProvider,
            IAccessService accessService,
            IroDocumentTemplateCategoryRepository roDocumentTemplateCategoryRepository) :
            base(roDocumentTemplateCategoryRepository,
                sessionProvider,
                accessService)
        {
            _roDocumentTemplateCategoryRepository = roDocumentTemplateCategoryRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "roDocumentTemplateCategory.Add"; //ModuleAccessHelper.Documents.roDocumentTemplatecategory.CUD;

        protected override string UpdateMedtodKey => "roDocumentTemplateCategory.Update"; // ModuleAccessHelper.Documents.roDocumentTemplatecategory.CUD;

        protected override string RemoveMedtodKey => "roDocumentTemplateCategory.Remove"; // ModuleAccessHelper.Documents.roDocumentTemplatecategory.CUD;

        protected override string ReadMedtodKey => "roDocumentTemplateCategory.Read"; //ModuleAccessHelper.Documents.roDocumentTemplatecategory.Read;
        #endregion

        protected override void ModelValidate(roDocumentTemplateCategoryDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(roDocumentTemplateCategoryDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(roDocumentTemplateCategoryDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(roDocumentTemplateCategoryDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update



    }
}
