using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface IroDocumentTemplateCategoryRepository : IBaseCrudRepository<roDocumentTemplateCategoryDto, int>
    {

    }
}
