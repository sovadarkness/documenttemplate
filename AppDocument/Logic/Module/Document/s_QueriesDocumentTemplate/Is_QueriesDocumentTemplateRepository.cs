using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface Is_QueriesDocumentTemplateRepository : IBaseCrudRepository<s_QueriesDocumentTemplateDto, int>
    {
        
	List<s_QueriesDocumentTemplateDto> GetByidDocumentTemplate(int idDocumentTemplate);
	List<s_QueriesDocumentTemplateDto> GetByidQuery(int idQuery);
    }
}
