using Core.Services.BaseCrudService;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface Is_QueriesDocumentTemplateService: IBaseCrudService<s_QueriesDocumentTemplateDto,int>
    {
	public List<s_QueriesDocumentTemplateDto> GetByidDocumentTemplate(int idDocumentTemplate);
	public List<s_QueriesDocumentTemplateDto> GetByidQuery(int idQuery);
    }
}
