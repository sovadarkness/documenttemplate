using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Globalization;

namespace AppDocument.DocTemplate
{
    public class s_QueriesDocumentTemplateDto : IDtoWithId<int>
    {
		public int id { get; set; }
	// start dictionary  Not Null 
        public int idDocumentTemplate { get; set; }    
        public string idDocumentTemplateNavName { get; set; }
	//end dictionary  
	// start dictionary  Not Null 
        public int idQuery { get; set; }    
        public string idQueryNavName { get; set; }
	//end dictionary  

    }
}
