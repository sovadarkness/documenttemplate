using Core.Abstraction;
using Core.Exceptions;
using Core.Services.BaseCrudService;
using Core.Validation;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq;

namespace AppDocument.DocTemplate
{
    public partial class s_QueriesDocumentTemplateService : BaseProtectedCrudService<s_QueriesDocumentTemplateDto, int>, Is_QueriesDocumentTemplateService
    {
        private Is_QueriesDocumentTemplateRepository _s_QueriesDocumentTemplateRepository;

        public s_QueriesDocumentTemplateService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_QueriesDocumentTemplateRepository s_QueriesDocumentTemplateRepository) :
            base(s_QueriesDocumentTemplateRepository,
                sessionProvider,
                accessService)
        {
            _s_QueriesDocumentTemplateRepository = s_QueriesDocumentTemplateRepository;
        }


        #region Custom Add
        protected override int CustomAdd(s_QueriesDocumentTemplateDto dto)
        {
            return base.CustomAdd(dto);
        }
        protected override void ValidateAdd(s_QueriesDocumentTemplateDto dto, List<IExecutionError> errors)
        {
            base.ValidateAdd(dto, errors);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_QueriesDocumentTemplateDto dto)
        {
            return base.CustomUpdate(dto);
        }
        protected override void ValidateUpdate(s_QueriesDocumentTemplateDto dto, List<IExecutionError> errors)
        {
            base.ValidateUpdate(dto, errors);
        }
        #endregion Custom Update

        #region Custom Delete
        protected override void ValidateDelete(int key, List<IExecutionError> errors)
        {
            base.ValidateDelete(key, errors);
        }
        #endregion Custom Delete

        protected override void ModelValidate(s_QueriesDocumentTemplateDto dto, List<IExecutionError> errors)
        {
          
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_QueriesDocumentTemplateDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region MtM Methods


        public List<s_QueriesDocumentTemplateDto> GetByidDocumentTemplate(int idDocumentTemplate)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_QueriesDocumentTemplateRepository.GetByidDocumentTemplate(idDocumentTemplate);
        }


        public List<s_QueriesDocumentTemplateDto> GetByidQuery(int idQuery)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_QueriesDocumentTemplateRepository.GetByidQuery(idQuery);
        }


        #endregion MtM Methods

        #region Access keys
        protected override string AddMedtodKey => "s_QueriesDocumentTemplate.Add"; // ModuleAccessHelper.Documents.S_queryDocumentsTemplate.CUD;

        protected override string UpdateMedtodKey => "s_QueriesDocumentTemplate.Update"; // ModuleAccessHelper.Documents.S_queryDocumentsTemplate.CUD;

        protected override string RemoveMedtodKey => "s_QueriesDocumentTemplate.Remove"; //ModuleAccessHelper.Documents.S_queryDocumentsTemplate.CUD;

        protected override string ReadMedtodKey => "s_QueriesDocumentTemplate.Read"; //ModuleAccessHelper.Documents.S_queryDocumentsTemplate.Read;
        #endregion
    }
}
