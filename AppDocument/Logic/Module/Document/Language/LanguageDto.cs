using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.SystemSettings
{
    public class LanguageDto : IDtoWithId<int>
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public System.Nullable<bool> isDefault { get; set; }
		public int id { get; set; }
    }
}
