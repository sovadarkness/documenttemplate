using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.SystemSettings
{
    public partial class LanguageService : BaseProtectedCrudService<LanguageDto, int>, ILanguageService
    {
        private readonly ILanguageRepository _LanguageRepository;
        public LanguageService(ISessionProvider sessionProvider,
            IAccessService accessService,
            ILanguageRepository LanguageRepository) :
            base(LanguageRepository,
                sessionProvider,
                accessService)
        {
            _LanguageRepository = LanguageRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "Language.Add";

        protected override string UpdateMedtodKey => "Language.Update";

        protected override string RemoveMedtodKey => "Language.Remove";

        protected override string ReadMedtodKey => "Language.Read";
        #endregion

        protected override void ModelValidate(LanguageDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(LanguageDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(LanguageDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(LanguageDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update



    }
}
