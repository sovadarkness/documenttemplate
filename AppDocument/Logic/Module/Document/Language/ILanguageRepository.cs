using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.SystemSettings
{
    public interface ILanguageRepository : IBaseCrudRepository<LanguageDto, int>
    {

    }
}
