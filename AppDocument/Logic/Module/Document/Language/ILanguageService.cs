using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.SystemSettings
{
    public interface ILanguageService: IBaseCrudService<LanguageDto, int>
    {
	
    }
}
