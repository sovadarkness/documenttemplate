using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.General
{
    public partial class s_PlaceholderGroupService : BaseProtectedCrudService<s_PlaceholderGroupDto, int>, Is_PlaceholderGroupService
    {
        private readonly Is_PlaceholderGroupRepository _s_PlaceholderGroupRepository;
        public s_PlaceholderGroupService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_PlaceholderGroupRepository s_PlaceholderGroupRepository) :
            base(s_PlaceholderGroupRepository,
                sessionProvider,
                accessService)
        {
            _s_PlaceholderGroupRepository = s_PlaceholderGroupRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "s_PlaceholderGroup.Add"; //ModuleAccessHelper.Documents.S_placeHolderGroup.CUD;

        protected override string UpdateMedtodKey => "s_PlaceholderGroup.Update"; //ModuleAccessHelper.Documents.S_placeHolderGroup.CUD;

        protected override string RemoveMedtodKey => "s_PlaceholderGroup.Remove"; //ModuleAccessHelper.Documents.S_placeHolderGroup.CUD;

        protected override string ReadMedtodKey => "s_PlaceholderGroup.Read"; // ModuleAccessHelper.Documents.S_placeHolderGroup.Read;
        #endregion

        protected override void ModelValidate(s_PlaceholderGroupDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_PlaceholderGroupDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(s_PlaceholderGroupDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_PlaceholderGroupDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update



    }
}
