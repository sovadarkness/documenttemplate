using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.General
{
    public class s_PlaceholderGroupDto : IDtoWithId<int>
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public int? queueNumber { get; set; }
			public int? idCustomSvgIcon { get; set; }
			public string idCustomSvgIconPreview { get; set; }
			public string iconColor { get; set; }
		public int id { get; set; }
    }
}
