using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface Is_PlaceholderGroupRepository : IBaseCrudRepository<s_PlaceholderGroupDto, int>
    {

    }
}
