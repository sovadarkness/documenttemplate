using Core.Services.BaseCrudService;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface Is_PlaceHolderGroupsMtmService: IBaseCrudService<s_PlaceHolderGroupsMtmDto,int>
    {
	public s_PlaceHolderGroupsMtmDto GetExact(int idPlaceHolder, int idTypePlaceholder);
	public List<s_PlaceHolderGroupsMtmDto> GetByidPlaceHolder(int idPlaceHolder);
	public List<s_PlaceHolderGroupsMtmDto> GetByidTypePlaceholder(int idTypePlaceholder);
    }
}
