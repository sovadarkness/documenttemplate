using Core.Abstraction;
using Core.Exceptions;
using Core.Services.BaseCrudService;
using Core.Validation;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq;

namespace AppDocument.General
{
    public partial class s_PlaceHolderGroupsMtmService : BaseProtectedCrudService<s_PlaceHolderGroupsMtmDto, int>, Is_PlaceHolderGroupsMtmService
    {
        private Is_PlaceHolderGroupsMtmRepository _s_PlaceHolderGroupsMtmRepository;

        public s_PlaceHolderGroupsMtmService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_PlaceHolderGroupsMtmRepository s_PlaceHolderGroupsMtmRepository) :
            base(s_PlaceHolderGroupsMtmRepository,
                sessionProvider,
                accessService)
        {
            _s_PlaceHolderGroupsMtmRepository = s_PlaceHolderGroupsMtmRepository;
        }


        #region Custom Add
        protected override int CustomAdd(s_PlaceHolderGroupsMtmDto dto)
        {
            return base.CustomAdd(dto);
        }
        protected override void ValidateAdd(s_PlaceHolderGroupsMtmDto dto, List<IExecutionError> errors)
        {
            base.ValidateAdd(dto, errors);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_PlaceHolderGroupsMtmDto dto)
        {
            return base.CustomUpdate(dto);
        }
        protected override void ValidateUpdate(s_PlaceHolderGroupsMtmDto dto, List<IExecutionError> errors)
        {
            base.ValidateUpdate(dto, errors);
        }
        #endregion Custom Update

        #region Custom Delete
        protected override void ValidateDelete(int key, List<IExecutionError> errors)
        {
            base.ValidateDelete(key, errors);
        }
        #endregion Custom Delete

        protected override void ModelValidate(s_PlaceHolderGroupsMtmDto dto, List<IExecutionError> errors)
        {
          
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_PlaceHolderGroupsMtmDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region MtM Methods

        public s_PlaceHolderGroupsMtmDto GetExact(int idPlaceHolder, int idTypePlaceholder)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_PlaceHolderGroupsMtmRepository.GetExact(idPlaceHolder, idTypePlaceholder);
        }

        public List<s_PlaceHolderGroupsMtmDto> GetByidPlaceHolder(int idPlaceHolder)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_PlaceHolderGroupsMtmRepository.GetByidPlaceHolder(idPlaceHolder);
        }


        public List<s_PlaceHolderGroupsMtmDto> GetByidTypePlaceholder(int idTypePlaceholder)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_PlaceHolderGroupsMtmRepository.GetByidTypePlaceholder(idTypePlaceholder);
        }


        #endregion MtM Methods

        #region Access keys
        protected override string AddMedtodKey => "s_PlaceHolderGroupsMtm.Add"; // ModuleAccessHelper.Documents.S_PlaceHolderGroupOfGroups.CUD;

        protected override string UpdateMedtodKey => "s_PlaceHolderGroupsMtm.Update"; //ModuleAccessHelper.Documents.S_PlaceHolderGroupOfGroups.CUD;

        protected override string RemoveMedtodKey => "s_PlaceHolderGroupsMtm.Remove"; //ModuleAccessHelper.Documents.S_PlaceHolderGroupOfGroups.CUD;

        protected override string ReadMedtodKey => "s_PlaceHolderGroupsMtm.Read"; // ModuleAccessHelper.Documents.S_PlaceHolderGroupOfGroups.Read;
        #endregion
    }
}
