using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface Is_PlaceHolderGroupsMtmRepository : IBaseCrudRepository<s_PlaceHolderGroupsMtmDto, int>
    {
        
	List<s_PlaceHolderGroupsMtmDto> GetByidPlaceHolder(int idPlaceHolder);
	List<s_PlaceHolderGroupsMtmDto> GetByidTypePlaceholder(int idTypePlaceholder);
    s_PlaceHolderGroupsMtmDto GetExact(int idPlaceHolder, int idTypePlaceholder);
    }
}
