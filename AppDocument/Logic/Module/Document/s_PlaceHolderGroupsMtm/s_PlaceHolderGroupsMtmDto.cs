using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Globalization;

namespace AppDocument.General
{
    public class s_PlaceHolderGroupsMtmDto : IDtoWithId<int>
    {
		public int id { get; set; }
	// start dictionary  Null Allow
        public int? idPlaceHolder { get; set; }    
        public string idPlaceHolderNavName { get; set; }
	//end dictionary  
	// start dictionary  Null Allow
        public int? idTypePlaceholder { get; set; }    
        public string idTypePlaceholderNavName { get; set; }
	//end dictionary  

    }
}
