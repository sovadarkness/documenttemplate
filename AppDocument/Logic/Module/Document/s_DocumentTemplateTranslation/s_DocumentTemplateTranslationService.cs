using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AppDocument.General;
using Core.Validation;
using AppDocument.SystemSettings;

namespace AppDocument.DocTemplate
{
    public partial class s_DocumentTemplateTranslationService : BaseProtectedCrudService<s_DocumentTemplateTranslationDto, int>, Is_DocumentTemplateTranslationService
    {
        private readonly Is_DocumentTemplateTranslationRepository _s_DocumentTemplateTranslationRepository;
        private readonly ILanguageRepository _LanguageRepository;
        private readonly Is_TemplateDocumentPlaceholderRepository _s_TemplateDocumentPlaceholderRepository;
        private readonly Is_DocumentTemplateRepository _s_DocumentTemplateRepository;
        private readonly Is_PlaceHolderTemplateRepository _s_PlaceHolderTemplateRepository;
        private readonly Is_QueriesDocumentTemplateRepository _s_QueriesDocumentTemplateRepository;
        public s_DocumentTemplateTranslationService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_DocumentTemplateTranslationRepository s_DocumentTemplateTranslationRepository,
            ILanguageRepository LanguageRepository,
            Is_TemplateDocumentPlaceholderRepository s_TemplateDocumentPlaceholderRepository,
            Is_PlaceHolderTemplateRepository s_PlaceHolderTemplateRepository,
            Is_QueriesDocumentTemplateRepository s_QueriesDocumentTemplateRepository,
            Is_DocumentTemplateRepository s_DocumentTemplateRepository 
        ) :
            base(s_DocumentTemplateTranslationRepository,
                sessionProvider,
                accessService)
        {
            _s_DocumentTemplateTranslationRepository = s_DocumentTemplateTranslationRepository;
            _LanguageRepository = LanguageRepository;
            _s_TemplateDocumentPlaceholderRepository = s_TemplateDocumentPlaceholderRepository;
            _s_PlaceHolderTemplateRepository = s_PlaceHolderTemplateRepository;
            _s_QueriesDocumentTemplateRepository = s_QueriesDocumentTemplateRepository;
            _s_DocumentTemplateRepository = s_DocumentTemplateRepository;
        }

        #region Access Keys
        //protected override string AddMedtodKey => ModuleAccessHelper.Documents.S_documentTemplateTranslation.CUD; TODO WTF

        //protected override string UpdateMedtodKey => ModuleAccessHelper.Documents.S_documentTemplateTranslation.CUD;

        //protected override string RemoveMedtodKey => ModuleAccessHelper.Documents.S_documentTemplateTranslation.CUD;

        //protected override string ReadMedtodKey => ModuleAccessHelper.Documents.S_documentTemplateTranslation.Read;
        protected override string AddMedtodKey => "s_DocumentTemplateTranslation.Add";

        protected override string UpdateMedtodKey => "s_DocumentTemplateTranslation.Update";

        protected override string RemoveMedtodKey => "s_DocumentTemplateTranslation.Remove";

        protected override string ReadMedtodKey => "s_DocumentTemplateTranslation.Read";
        #endregion

        protected override void ModelValidate(s_DocumentTemplateTranslationDto dto, List<IExecutionError> errors)
        {

            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_DocumentTemplateTranslationDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(s_DocumentTemplateTranslationDto dto)
        {
            var resultId = base.CustomAdd(dto);
            CreatePlaceHolders(dto, resultId);
            return resultId;
        }

        private void CreatePlaceHolders(s_DocumentTemplateTranslationDto dto, int resultId)
        {
            var matches = Regex.Matches(dto.template, @"(?<=\[\[)(.*?)(?=\]\])");
            var words = new List<string>();
            foreach (Match m in matches)
            {
                if (!words.Contains(m.Value))
                    words.Add(m.Value);
            }

            var placeholders = _s_PlaceHolderTemplateRepository.GetByNames(words);
            var newQueryIds = placeholders.Select(x => x.idQuery).Distinct().ToList();

            var oldQueries = _s_QueriesDocumentTemplateRepository.GetByidDocumentTemplate(resultId).ToList();

            var oldQueriesIds = oldQueries.Select(x => x.idQuery);
            foreach (var q in oldQueries)
            {
                if (!newQueryIds.Contains(q.idQuery))
                {
                    _s_QueriesDocumentTemplateRepository.Remove(q.id);
                }
            }

            var queriesToAdd = newQueryIds.Where(x => !oldQueriesIds.Contains(x)).ToList();
            foreach (var ta in queriesToAdd)
            {
                _s_QueriesDocumentTemplateRepository.Add(new s_QueriesDocumentTemplateDto
                { idDocumentTemplate = resultId, idQuery = ta });
            }

            var newPlaceHolders = placeholders.Select(x => x.id).ToList();
            var oldTemplates = _s_TemplateDocumentPlaceholderRepository.GetByidTemplateDocument(resultId).ToList();
            var oldPlaceholdersIds = oldTemplates.Select(x => x.idPlaceholder);
            foreach (var template in oldTemplates)
            {
                if (!newPlaceHolders.Contains(template.idPlaceholder))
                {
                    _s_TemplateDocumentPlaceholderRepository.Remove(template.id);
                }
            }

            var holdersToAdd = newPlaceHolders.Where(x => !oldPlaceholdersIds.Contains(x)).ToList();
            foreach (var h in holdersToAdd)
            {
                _s_TemplateDocumentPlaceholderRepository.Add(new s_TemplateDocumentPlaceholderDto()
                { idTemplateDocument = resultId, idPlaceholder = h });
            }
        }

        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_DocumentTemplateTranslationDto updateDto)
        {
            var resultId = base.CustomUpdate(updateDto);
            CreatePlaceHolders(updateDto, resultId);
            return resultId;
        }
        #endregion Custom Update


        public List<s_DocumentTemplateTranslationDto> GetByidDocumentTemplate(int id)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_DocumentTemplateTranslationRepository.GetByidDocumentTemplate(id);
        }
        public s_DocumentTemplateTranslationDto GetByidLanguageAndIdTemplate(int idLanguage, int idTemplate)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_DocumentTemplateTranslationRepository.GetByidLanguageAndIdTemplate(idLanguage, idTemplate);
        }

        public string GetFilledTemplate(int idDocumentTemplate, Dictionary<string, object> sqlParameters, string language = "ru")
        {
            var langs = _LanguageRepository.GetAll();
            var lang = new LanguageDto();
            if(language == "ky" || language == "ky-KG" || language == "Kg")
            {
                lang = langs.Where(x => x.code == "kyrgyz").FirstOrDefault();
            }
            else if(language == "ru" || language == "ru-RU" || language == "Ru")
            {
                lang = langs.Where(x => x.code == "russian").FirstOrDefault();
            }
            else
            {
                lang = langs.Where(x => x.isDefault == true).FirstOrDefault();
            }
            sqlParameters.Add("language", language);

            var docTrans = _s_DocumentTemplateTranslationRepository.GetByidLanguageAndIdTemplate(lang.id, idDocumentTemplate);
            if (docTrans == null) return "ERROR: translate not found";
            string html = docTrans.template;

            var placeholders = _s_TemplateDocumentPlaceholderRepository.GetPlaceholderByidTemplateDocument(docTrans.id);
            var filedHtml = _s_TemplateDocumentPlaceholderRepository.GetFilledHtml(html, placeholders, sqlParameters);
            return filedHtml;
        }


        public string GetFilledTemplateHtml(int idDocument, string html, Dictionary<string, object> sqlParameters)
        {
            //var doc = _s_DocumentTemplateTranslationRepository.GetOneByKey(idDocument);
            //string html = doc.template;

            var placeholders = _s_TemplateDocumentPlaceholderRepository.GetPlaceholderByidTemplateDocument(idDocument);
            var filedHtml = _s_TemplateDocumentPlaceholderRepository.GetFilledHtml(html, placeholders, sqlParameters);
            return filedHtml;
        }

        public s_DocumentTemplateTranslationDto GetDocumentForVisit(int document, Dictionary<string, object> sqlParameters)
        {
            //Todo lang
            var translate = _s_DocumentTemplateTranslationRepository.GetByidDocumentTemplate(document)
                .FirstOrDefault(x => x.idLanguageNavName == "Ru-ru" || x.idLanguageNavName == "�������");

            var placeholders = _s_TemplateDocumentPlaceholderRepository.GetPlaceholderByidTemplateDocument(translate.id);
            translate.template = _s_TemplateDocumentPlaceholderRepository.GetFilledHtml(translate.template, placeholders, sqlParameters);
            return translate;
        }

        public s_DocumentTemplateTranslationDto GetDocumentBalance(int document, Dictionary<string, object> sqlParameters)
        {
            var translate = _s_DocumentTemplateTranslationRepository.GetByidDocumentTemplate(document)
                .FirstOrDefault(x => x.idLanguageNavName == "Ru-ru" || x.idLanguageNavName == "�������");

            var placeholders = _s_TemplateDocumentPlaceholderRepository.GetPlaceholderByidTemplateDocument(translate.id);
            translate.template = _s_TemplateDocumentPlaceholderRepository.GetFilledHtml(translate.template, placeholders, sqlParameters);
            return translate;
        }
        public List<s_DocumentTemplateTranslationDto> GetSubscription()
        {

            return null;

        }
    }
}
