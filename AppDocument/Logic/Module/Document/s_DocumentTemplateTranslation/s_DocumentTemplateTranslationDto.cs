using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.DocTemplate
{
    public class s_DocumentTemplateTranslationDto : IDtoWithId<int>
    {
			public string template { get; set; }
		public int id { get; set; }
	// start dictionary  Not Null 
        public int idDocumentTemplate { get; set; }    
        public string idDocumentTemplateNavName { get; set; }
        public bool? useDiagnos { get; set; }
    //end dictionary  
    // start dictionary  Not Null 
        public int idLanguage { get; set; }    
        public string idLanguageNavName { get; set; }
        public int? CustomSubsribtion { get; set; }
        public bool? isSubscribed { get; set; }
	//end dictionary  
    }
}
