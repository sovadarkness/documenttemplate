using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.DocTemplate
{
    public interface Is_DocumentTemplateTranslationService : IBaseCrudService<s_DocumentTemplateTranslationDto, int>
    {
        List<s_DocumentTemplateTranslationDto> GetByidDocumentTemplate(int id);
        public s_DocumentTemplateTranslationDto GetByidLanguageAndIdTemplate(int idLanguage, int idTemplate);
        List<s_DocumentTemplateTranslationDto> GetSubscription();
        string GetFilledTemplate(int template, Dictionary<string, object> dictionary, string language);
        string GetFilledTemplateHtml(int template, string html, Dictionary<string, object> dictionary);
        s_DocumentTemplateTranslationDto GetDocumentForVisit(int document, Dictionary<string, object> dictionary);
        s_DocumentTemplateTranslationDto GetDocumentBalance(int document, Dictionary<string, object> dictionary);
    }
}
