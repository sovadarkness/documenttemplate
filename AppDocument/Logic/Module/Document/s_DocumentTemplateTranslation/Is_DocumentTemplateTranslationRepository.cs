using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface Is_DocumentTemplateTranslationRepository : IBaseCrudRepository<s_DocumentTemplateTranslationDto, int>
    {
	List<s_DocumentTemplateTranslationDto> GetByidDocumentTemplate(int id);
        s_DocumentTemplateTranslationDto GetByidLanguageAndIdTemplate(int idLanguage, int idTemplate);
    List<s_DocumentTemplateTranslationDto> GetSubscription();
    }
}
