using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface Is_DocumentTemplateRepository : IBaseCrudRepository<s_DocumentTemplateDto, int>
    {
        List<s_DocumentTemplateDto> GetAllTemplate();
    }
}
