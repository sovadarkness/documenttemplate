using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.General
{
    public class s_DocumentTemplateDto : IDtoWithId<int>
    {
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int id { get; set; }
        public int? idCustomSvgIcon { get; set; }
        public string idCustomSvgIconPreview { get; set; }
        public string iconColor { get; set; }
        public int? idDocumentTyplateType { get; set; }
        public string idDocumentTyplateTypeNavName { get; set; }
        public bool? useDiagnos { get; set; }
    }
}
