using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.General
{
    public partial class s_DocumentTemplateService : BaseProtectedCrudService<s_DocumentTemplateDto, int>, Is_DocumentTemplateService
    {
        private readonly Is_DocumentTemplateRepository _s_DocumentTemplateRepository;
        public s_DocumentTemplateService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_DocumentTemplateRepository s_DocumentTemplateRepository) :
            base(s_DocumentTemplateRepository,
                sessionProvider,
                accessService)
        {
            _s_DocumentTemplateRepository = s_DocumentTemplateRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "s_DocumentTemplate.Add"; // ModuleAccessHelper.Documents.S_DocumentTemplate.CUD;

        protected override string UpdateMedtodKey => "s_DocumentTemplate.Update"; // ModuleAccessHelper.Documents.S_DocumentTemplate.CUD;

        protected override string RemoveMedtodKey => "s_DocumentTemplate.Remove"; //ModuleAccessHelper.Documents.S_DocumentTemplate.CUD;

        protected override string ReadMedtodKey => "s_DocumentTemplate.Read"; // ModuleAccessHelper.Documents.S_DocumentTemplate.Read;
        #endregion

        protected override void ModelValidate(s_DocumentTemplateDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_DocumentTemplateDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(s_DocumentTemplateDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_DocumentTemplateDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }


        #endregion Custom Update

        public List<s_DocumentTemplateDto> GetAllTemplate()
        {
            return _s_DocumentTemplateRepository.GetAllTemplate();
        }

    }
}
