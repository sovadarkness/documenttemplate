using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.General
{
    public interface Is_DocumentTemplateService : IBaseCrudService<s_DocumentTemplateDto, int>
    {
        List<s_DocumentTemplateDto> GetAllTemplate();
    }
}
