using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.DocTemplate
{
    public partial class s_PlaceHolderTemplateService : BaseProtectedCrudService<s_PlaceHolderTemplateDto, int>, Is_PlaceHolderTemplateService
    {
        private readonly Is_PlaceHolderTemplateRepository _s_PlaceHolderTemplateRepository;
        public s_PlaceHolderTemplateService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_PlaceHolderTemplateRepository s_PlaceHolderTemplateRepository) :
            base(s_PlaceHolderTemplateRepository,
                sessionProvider,
                accessService)
        {
            _s_PlaceHolderTemplateRepository = s_PlaceHolderTemplateRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "s_PlaceHolderTemplate.Add"; //ModuleAccessHelper.Documents.S_PlaceHolderTemplate.CUD;

        protected override string UpdateMedtodKey => "s_PlaceHolderTemplate.Update"; //ModuleAccessHelper.Documents.S_PlaceHolderTemplate.CUD;

        protected override string RemoveMedtodKey => "s_PlaceHolderTemplate.Remove"; // ModuleAccessHelper.Documents.S_PlaceHolderTemplate.CUD;

        protected override string ReadMedtodKey => "s_PlaceHolderTemplate.Read"; //ModuleAccessHelper.Documents.S_PlaceHolderTemplate.Read;
        #endregion

        protected override void ModelValidate(s_PlaceHolderTemplateDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_PlaceHolderTemplateDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(s_PlaceHolderTemplateDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_PlaceHolderTemplateDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update


	public List<s_PlaceHolderTemplateDto> GetByidQuery(int id)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_PlaceHolderTemplateRepository.GetByidQuery(id);
        }

    }
}
