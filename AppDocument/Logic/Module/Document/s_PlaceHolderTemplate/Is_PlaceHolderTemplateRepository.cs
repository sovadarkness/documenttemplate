using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.DocTemplate
{
    public interface Is_PlaceHolderTemplateRepository : IBaseCrudRepository<s_PlaceHolderTemplateDto, int>
    {
        List<s_PlaceHolderTemplateDto> GetByidQuery(int id);
        List<s_PlaceHolderTemplateDto> GetByNames(IEnumerable<string> names);

    }
}
