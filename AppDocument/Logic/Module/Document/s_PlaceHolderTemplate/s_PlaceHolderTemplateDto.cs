using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using AppDocument.General;

namespace AppDocument.DocTemplate
{
    public class s_PlaceHolderTemplateDto : IDtoWithId<int>
    {
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public int id { get; set; }
        // start dictionary  Not Null 
        public int idQuery { get; set; }
        public string idQueryNavName { get; set; }
        public string idQueryCommand{ get; set; }
        //end dictionary  

    }
}
