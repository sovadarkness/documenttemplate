using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.DocTemplate
{
    public interface Is_PlaceHolderTemplateService: IBaseCrudService<s_PlaceHolderTemplateDto, int>
    {
	List<s_PlaceHolderTemplateDto> GetByidQuery(int id);
	
    }
}
