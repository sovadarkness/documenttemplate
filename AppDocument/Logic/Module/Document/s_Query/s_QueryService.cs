using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.General
{
    public partial class s_QueryService : BaseProtectedCrudService<s_QueryDto, int>, Is_QueryService
    {
        private readonly Is_QueryRepository _s_QueryRepository;
        public s_QueryService(ISessionProvider sessionProvider,
            IAccessService accessService,
            Is_QueryRepository s_QueryRepository) :
            base(s_QueryRepository,
                sessionProvider, accessService)
        {
            _s_QueryRepository = s_QueryRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "s_Query.Add";

        protected override string UpdateMedtodKey => "s_Query.Update";

        protected override string RemoveMedtodKey => "s_Query.Remove";

        protected override string ReadMedtodKey => "s_Query.Read";
        #endregion

        protected override void ModelValidate(s_QueryDto dto, List<IExecutionError> errors)
        {
	//CheckEmptyTextField(event.target.value, nameErrors);
           
            base.ModelValidate(dto, errors);
        }

        protected override void DataValidate(s_QueryDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(s_QueryDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(s_QueryDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update


	public List<s_QueryDto> GetByidQueryType(int id)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_QueryRepository.GetByidQueryType(id);
        }
	public List<s_QueryDto> GetByidDBMS(int id)
        {
            //_accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _s_QueryRepository.GetByidDBMS(id);
        }

    }
}
