using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.General
{
    public interface Is_QueryService: IBaseCrudService<s_QueryDto, int>
    {
	List<s_QueryDto> GetByidQueryType(int id);
	List<s_QueryDto> GetByidDBMS(int id);
	
    }
}
