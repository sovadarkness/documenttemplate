using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface Is_QueryRepository : IBaseCrudRepository<s_QueryDto, int>
    {
	List<s_QueryDto> GetByidQueryType(int id);
	List<s_QueryDto> GetByidDBMS(int id);

    }
}
