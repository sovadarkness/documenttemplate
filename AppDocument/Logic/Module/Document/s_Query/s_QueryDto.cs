using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.General
{
    public class s_QueryDto : IDtoWithId<int>
    {
			public string name { get; set; }
			public string description { get; set; }
			public string code { get; set; }
			public string query { get; set; }
		public int id { get; set; }
	// start dictionary  Not Null 
        public int idQueryType { get; set; }    
        public string idQueryTypeNavName { get; set; }
        public string idQueryTypeNavCode{ get; set; }
	//end dictionary  
	// start dictionary  Not Null 
        public int idDBMS { get; set; }    
        public string idDBMSNavName { get; set; }
	//end dictionary  
    }
}
