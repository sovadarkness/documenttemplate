using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;
using System;
using AppDocument.Access;

namespace AppDocument.General
{
    public partial class CommonSettingsService : BaseProtectedCrudService<CommonSettingsDto, int>, ICommonSettingsService
    {
        private readonly ICommonSettingsRepository _CommonSettingsRepository;
        public CommonSettingsService(ISessionProvider sessionProvider,
            IAccessService accessService,
            ICommonSettingsRepository CommonSettingsRepository):
            base(CommonSettingsRepository,
                sessionProvider,
                accessService)
        {
            _CommonSettingsRepository = CommonSettingsRepository;
        }

        public CommonSettingsDto GetOneByCode(string code)
        {
            return _CommonSettingsRepository.GetOneByCode(code);
        }
        #region Access Keys
        protected override string AddMedtodKey => "CommonSettings.Add";
        protected override string UpdateMedtodKey => "CommonSettings.Update";
        protected override string RemoveMedtodKey => "CommonSettings.Remove";
        protected override string ReadMedtodKey => "CommonSettings.Read";
        #endregion Access Keys

        #region Validation
        protected override void ModelValidate(CommonSettingsDto dto, List<IExecutionError> errors)
        {
            base.ModelValidate(dto, errors);
        }
        protected override void DataValidate(CommonSettingsDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }
        #endregion Validation

        #region Custom Add
        protected override int CustomAdd(CommonSettingsDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(CommonSettingsDto updateDto)
        {
            return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update

        #region Constraints
        
        #endregion Constraints
    }
}