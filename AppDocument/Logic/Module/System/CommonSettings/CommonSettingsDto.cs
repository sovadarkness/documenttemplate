using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.General
{
    public class CommonSettingsDto : IDtoWithId<int>
    {
        public int id { get; set; }
		public string name { get; set; }
		public string nameky { get; set; }
		public string value { get; set; }
		public string valueky { get; set; }
		public string code { get; set; }
		public string description { get; set; }
		
    }
}