using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.General
{
    public interface ICommonSettingsService: IBaseCrudService<CommonSettingsDto, int>
    {
	    public CommonSettingsDto GetOneByCode(string code);
    }
}