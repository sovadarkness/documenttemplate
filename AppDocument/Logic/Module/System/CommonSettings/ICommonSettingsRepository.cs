using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppDocument.General
{
    public interface ICommonSettingsRepository : IBaseCrudRepository<CommonSettingsDto, int>
    {
        public CommonSettingsDto GetOneByCode(string code);
    }
}
