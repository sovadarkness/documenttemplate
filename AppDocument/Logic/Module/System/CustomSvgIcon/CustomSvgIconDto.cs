using Core.Abstraction;
using AppDocument.Base;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AppDocument.General
{
    public class CustomSvgIconDto : IDtoWithId<int>
    {
			public string name { get; set; }
			public string svgPath { get; set; }
			public string usedTables { get; set; }
		public int id { get; set; }
    }
}
