using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.General
{
    public interface ICustomSvgIconRepository : IBaseCrudRepository<CustomSvgIconDto, int>
    {
        List<CustomSvgIconDto> GetByTableName(string tableName);
    }
}
