using System.Collections.Generic;
using Core.Services.BaseCrudService;

namespace AppDocument.General
{
    public interface ICustomSvgIconService : IBaseCrudService<CustomSvgIconDto, int>
    {
        List<CustomSvgIconDto> GetByTableName(string tableName);
    }
}
