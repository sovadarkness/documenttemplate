using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using Core.Validation;

namespace AppDocument.General
{
    public partial class CustomSvgIconService : BaseProtectedCrudService<CustomSvgIconDto, int>, ICustomSvgIconService
    {
        private readonly ICustomSvgIconRepository _CustomSvgIconRepository;
        public CustomSvgIconService(ISessionProvider sessionProvider,
            IAccessService accessService,
            ICustomSvgIconRepository CustomSvgIconRepository) :
            base(CustomSvgIconRepository,
                sessionProvider,
                accessService)
        {
            _CustomSvgIconRepository = CustomSvgIconRepository;
        }

        #region Access Keys
        protected override string AddMedtodKey => "CustomSvgIcon.Add";

        protected override string UpdateMedtodKey => "CustomSvgIcon.Update";

        protected override string RemoveMedtodKey => "CustomSvgIcon.Remove";

        protected override string ReadMedtodKey => "CustomSvgIcon.Read";
        #endregion

        protected override void ModelValidate(CustomSvgIconDto dto, List<IExecutionError> errors)
        {
           
            base.ModelValidate(dto, errors);
        }

        public List<CustomSvgIconDto> GetByTableName(string tableName)
        {
            return _CustomSvgIconRepository.GetByTableName(tableName);
        }

        protected override void DataValidate(CustomSvgIconDto dto, List<IExecutionError> errors)
        {
            base.DataValidate(dto, errors);
        }

        #region Custom Add
        protected override int CustomAdd(CustomSvgIconDto dto)
        {
          return base.CustomAdd(dto);
        }
        #endregion Custom Add

        #region Custom Update
        protected override int CustomUpdate(CustomSvgIconDto updateDto)
        {
           return base.CustomUpdate(updateDto);
        }
        #endregion Custom Update



    }
}
