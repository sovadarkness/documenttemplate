﻿using Core.Abstraction;
using AppDocument.Base;
using System;

namespace AppDocument.SystemSettings
{
    public class SystemSettingDto : IDtoWithId<int>
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int QueueNumber { get; set; }
        public string Value { get; set; }
    }
}
