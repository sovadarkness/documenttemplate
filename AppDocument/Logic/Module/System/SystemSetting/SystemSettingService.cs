﻿using Core.Abstraction;
using Core.Services.BaseCrudService;
using AppDocument.Base;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;
using AppDocument.Base.DtoWrapperErrors;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Linq;
namespace AppDocument.SystemSettings
{
    public partial class SystemSettingService : BaseProtectedCrudService<SystemSettingDto, int>, ISystemSettingService
    {

        private readonly ISystemSettingRepository _systemSettingRepository;
        public SystemSettingService(ISessionProvider sessionProvider,
            IAccessService accessService,
            ISystemSettingRepository SystemSettingRepository) :
            base(SystemSettingRepository, sessionProvider,
                accessService
                )
        {
            _systemSettingRepository = SystemSettingRepository;
        }

        protected override string AddMedtodKey => "SystemSetting.Add";

        protected override string UpdateMedtodKey => "SystemSetting.Update";

        protected override string RemoveMedtodKey => "SystemSetting.Remove";

        protected override string ReadMedtodKey => "SystemSetting.Read";

        public void SetLogo(string filePath)
        {
            _systemSettingRepository.SetLogo(filePath);
        }
        
        public string GetLogo()
        {
            return _systemSettingRepository.GetLogo();
        }
        public void DeleteLogo()
        {
            _systemSettingRepository.DeleteLogo();
        }

        public int GetBonusProcent()
        {
            return _systemSettingRepository.GetBonusProcent();
        }

        public int GetMainProcent()
        {
            return _systemSettingRepository.GetMainProcent();
        }
}
}
