﻿using Core.Services.BaseCrudService;
using AppDocument.Base;
using System.Collections.Generic;

namespace AppDocument.SystemSettings
{
    public interface ISystemSettingRepository : IBaseCrudRepository<SystemSettingDto, int>
    {
        void SetLogo(string filePath);
        string GetLogo();
        void DeleteLogo();
        int GetBonusProcent();
        int GetMainProcent();

    }
}
