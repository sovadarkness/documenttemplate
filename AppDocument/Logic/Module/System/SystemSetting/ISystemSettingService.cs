﻿using Core.Services.BaseCrudService;

namespace AppDocument.SystemSettings
{
    public interface ISystemSettingService : IBaseCrudService<
        SystemSettingDto,
        int>
    {
        void SetLogo(string filePath);
        string GetLogo();
        void DeleteLogo();
        int GetBonusProcent();
        int GetMainProcent();
    }
}