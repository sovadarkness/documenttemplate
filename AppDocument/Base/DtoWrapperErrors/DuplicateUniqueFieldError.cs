﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;

namespace AppDocument.Base.DtoWrapperErrors
{
    internal class DuplicateUniqueFieldError: IExecutionError
    {
        public string FieldName { get; private set; }
        public object Value { get; private set; }

        public int ErrorCode => (int)ValidationErrors.DuplicateUniqueField;

        public string ErrorType => ErrorTypes.Validation;

        public DuplicateUniqueFieldError(string fieldName, object value)
        {
            FieldName = fieldName;
            Value = value;
        }
    }
}