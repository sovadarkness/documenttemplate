﻿using Microsoft.Extensions.DependencyInjection;
using Core.Abstraction;
using AppDocument.General;
using AppDocument.SystemSettings;
using AppDocument.Access;
using AppDocument.DocTemplate;

namespace AppDocument
{
    public static class DiServices
    {
        public static void AddServices(IServiceCollection services)
        {
            services.AddScoped<IAccessService, AccessService>();
            services.AddScoped<ICommonSettingsService, CommonSettingsService>();
            services.AddScoped<ISystemSettingService, SystemSettingService>();
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<ICustomSvgIconService, CustomSvgIconService>();
            services.AddScoped<Is_DocumentTemplateTranslationService, s_DocumentTemplateTranslationService>();
            services.AddScoped<Is_QueriesDocumentTemplateService, s_QueriesDocumentTemplateService>();
            services.AddScoped<Is_TemplateDocumentPlaceholderService, s_TemplateDocumentPlaceholderService>();
            services.AddScoped<Is_DocumentTemplateService, s_DocumentTemplateService>();
            services.AddScoped<Is_PlaceHolderTemplateService, s_PlaceHolderTemplateService>();
            services.AddScoped<Is_PlaceHolderGroupsMtmService, s_PlaceHolderGroupsMtmService>();
            services.AddScoped<Is_PlaceholderGroupService, s_PlaceholderGroupService>();
            services.AddScoped<IroDocumentTemplateCategoryService, roDocumentTemplateCategoryService>();
            services.AddScoped<Is_QueryService, s_QueryService>();
        }
    }
}
