﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoApp.Logger
{
    public interface ILogger
    {
        void Trace(string message);
        void Debug(string message);
        void Info(string message);
        void Warning(string message);
        void Error(string message);
        void Fatal(string message);
    }
}
