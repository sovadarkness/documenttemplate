using System.Net.Http;
using System.Net.Http.Headers;
using DALDocument.EF.Context;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using PLDocument;

namespace BackendLogin.IntegrationTests
{
    [TestFixture]
    public class BaseTest
    {
        protected HttpClient httpClient;
        protected ApplicationContext dbContext;

        [SetUp]
        public void SetUp()
        {
            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            dbContext = webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            httpClient = webHost.CreateClient();
            var token = Auth.Login("admin", "Pa$$word123").Result;
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
        }
    }
}