using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Core.Providers;
using DALDocument.EF.Context;
using DALDocument.EF.Entities;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using PLDocument;

namespace BackendLogin.IntegrationTests
{
    [TestFixture]
    public class BaseMemoryTest
    {
        protected ApplicationContext dbContext;
        protected WebApplicationFactory<Startup> webHost;

        [SetUp]
        public void SetUp()
        {
            webHost = new WebApplicationFactory<Startup>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var dbContextDescriptor = services.FirstOrDefault(d =>
                        d.ServiceType == typeof(DbContextOptions<ApplicationContext>));

                    services.Remove(dbContextDescriptor);

                    services.AddDbContext<ApplicationContext>(options =>
                    {
                        options.UseInMemoryDatabase("Test_db");
                    });

                    //services.AddScoped<IDateTimeProvider, DateTimeProvider>();
                });
            });

            dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            SystemSetting defLanguage = new SystemSetting { code = "DefaultLanguage", value = "1" };

            // todo
            //if (dbContext.SystemSettings.FirstOrDefault(x => x.code == defLanguage.code) == null)
            //{
            //    dbContext.SystemSettings.Add(defLanguage);
            //    dbContext.SaveChanges();
            //}


        }

        public async Task<HttpClient> loginWithUser(string userName, string password)
        {
            var httpClient = webHost.CreateClient();
            var token = await Auth.Login(userName, password);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            return httpClient;
        }
    }
}