﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Org.BouncyCastle.Bcpg;

namespace BackendLogin.IntegrationTests
{
    public static class Auth
    {
        static string IdentityUrl => "http://194.87.102.173:4203";

        public static async Task<string> Login(string user, string password)
        {
            var loginData = new Dictionary<string, string>()
            {
                {"username", user},
                {"password", password},
                {"grant_type", "password"},
                {"client_id", "mis-backend"}
            };
            var formData = new FormUrlEncodedContent(loginData);
            //formData.Headers.Clear();
            //formData.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.PostAsync($"{IdentityUrl}/connect/token", formData);
            var contentResponse = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ResponseIdentity>(contentResponse);
            return result.access_token;
        }

        private class ResponseIdentity
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string scope { get; set; }
            public string token_type { get; set; }
        }
    }
}
