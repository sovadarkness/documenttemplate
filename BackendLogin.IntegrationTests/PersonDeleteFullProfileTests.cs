using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DemoApp.Hrm;
using DemoEfRepo.EF.Context;
using DemoEfRepo.EF.Entities;
using DocumentFormat.OpenXml.Drawing.Charts;
using iTextSharp.text;
using LayerTransmitter.Wrapper;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using NUnit.Framework;
using ReactApplication;
using SixLabors.ImageSharp;

namespace BackendLogin.IntegrationTests
{
    //[TestFixture]
    public class PersonDeleteFullProfileTests
    {
        private int idPerson;

        //[Test]
        public async Task _1_SearchPerson_SendUserId_ShouldReturnOk()
        {
            // Arrange
            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var personDB = dbContext.People.FirstOrDefault(x =>
                x.firstName == "Jon" && x.lastName == "Doe" && x.userName == "Jon_user");

            // Act

            HttpResponseMessage response = await httpClient.GetAsync("api/Person/GetOneById?id=" + personDB.id);

            // Assert

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var contentResp = await response.Content.ReadAsStringAsync();
            var person = JsonConvert.DeserializeObject<DtoWrapper<PersonDto>>(contentResp);
            idPerson = person.Item.Id;
        }

        //[Test]
        public async Task _2_DeletePersonContact_DeleteContact_ShouldReturnCount()
        {
            // Arrange
            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var personContacts = dbContext.PersonContacts.Where(x =>
                x.idPerson == idPerson).ToList();

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/PersonContact/GetAll");
            foreach (var item in personContacts)
            {
                HttpResponseMessage responseDelete = await httpClient.PostAsync("/api/PersonContact/Delete", new StringContent(item.id.ToString()));
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/PersonContact/GetAll");

            // Assert

            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var contactBefore = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var contactAfter = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentAfter);
            int recordCount = contactBefore.Item.Count - contactAfter.Item.Count;
            Assert.AreEqual(recordCount, personContacts.Count);
        }

        //[Test]
        public async Task _3_DeleteTestTypeContact_DeleteTypeContact_ShouldReturnCount()
        {
            // Arrange

            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var typeContacts = dbContext.TypeContacts.Where(x =>
                x.name == "TestTypeContactType" && x.code == "TestTypeContactType").ToList();

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/TypeContact/GetAll");
            foreach (var item in typeContacts)
            {
                HttpResponseMessage responseDelete = await httpClient.PostAsync("/api/TypeContact/Delete", new StringContent(item.id.ToString()));
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/TypeContact/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var typeContactListBefore = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var typeContactListAfter = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentAfter);
            int recordCount = typeContactListBefore.Item.Count - typeContactListAfter.Item.Count;
            Assert.AreEqual(recordCount, typeContacts.Count);
        }

        //[Test]
        public async Task _4_DeletePersonJobHishtorys_DeleteJobHistorys_ShouldReturnCount()
        {
            // Arrange
            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var jobList = dbContext.JobHistories.Where(x =>
                x.idPerson == idPerson).ToList();

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/JobHistory/GetByIdPerson?idPerson=" + idPerson);
            foreach (var item in jobList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/JobHistory/Delete", new StringContent(item.id.ToString()));
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/JobHistory/GetByIdPerson?idPerson=" + idPerson);

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var jobsBefore = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var jobsAfter = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentAfter);
            int recordCount = jobsBefore.Item.Count - jobsAfter.Item.Count;
            Assert.AreEqual(recordCount, jobList.Count);
        }

        //[Test]
        public async Task _5_DeleteDocumentType_DeleteRecord_ShouldReturnCount()
        {
            // Arrange

            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var documentTypeList = dbContext.DocumentTypes.Where(x =>
                x.name == "TestDocumentType" && x.code == "TestDocumentType").ToList();

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/DocumentType/GetAll");
            foreach (var item in documentTypeList)
            {
                HttpResponseMessage responseDelete = await httpClient.PostAsync("/api/DocumentType/Delete", new StringContent(item.id.ToString()));
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/DocumentType/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var documentTypeBefore = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var documentTypeAfter = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentAfter);
            int recordCount = documentTypeBefore.Item.Count - documentTypeAfter.Item.Count;
            Assert.AreEqual(recordCount, documentTypeList.Count);
        }

        //[Test]
        public async Task _6_DeletePost_DeleteRecord_ShouldReturnCount()
        {
            // Arrange

            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var postList = dbContext.Posts.Where(x =>
                x.name == "TestPost" && x.code == "TestPostRecord").ToList();

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/Post/GetAll");
            foreach (var item in postList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/Post/AddOrUpdate", new StringContent(item.id.ToString()));
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/Post/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var postsBefore = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var postsAfter = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentAfter);
            int recordCount = postsBefore.Item.Count - postsAfter.Item.Count;
            Assert.AreEqual(recordCount, postList.Count);
        }

        //[Test]
        public async Task _7_DeletePerson_DeleteRecord_ShouldReturnOK()
        {
            // Arrange

            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            // Act

            HttpResponseMessage responseDelete = await httpClient.PostAsync("/api/Person/Delete", new StringContent(idPerson.ToString()));
            
            // Assert

            Assert.AreEqual(HttpStatusCode.OK, responseDelete.StatusCode);
        }

        //[Test]
        public async Task _8_DeleteTestSex_DeleteRecord_ShouldReturnCount()
        {
            // Arrange

            WebApplicationFactory<Startup> webHost =
                new WebApplicationFactory<Startup>().WithWebHostBuilder(builder => { });

            ApplicationContext dbContext =
                webHost.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            HttpClient httpClient = webHost.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", AuthToken.admin);

            var sexList = dbContext.Sexes.Where(x =>
                x.name == "TestMale" || x.name == "TestWoman").ToList();

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/Sex/GetAll");
            foreach (var item in sexList)
            {
                HttpResponseMessage responseDelete = await httpClient.PostAsync("/api/Sex/Delete", new StringContent(item.id.ToString()));
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/Sex/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var sexsBefore = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var sexsAfter = JsonConvert.DeserializeObject<DtoWrapper<List<SexDto>>>(contentAfter);
            int recordCount = sexsBefore.Item.Count - sexsAfter.Item.Count;
            Assert.AreEqual(recordCount, sexList.Count);
        }
    }
}