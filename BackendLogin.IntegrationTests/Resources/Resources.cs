﻿using System;
using Microsoft.VisualBasic;

namespace BackendLogin.IntegrationTests
{
    public static class Resources
    {
        public static string sampleFile => @$"{Environment.CurrentDirectory}\Resources\File\img_avatar.png";
    }
}