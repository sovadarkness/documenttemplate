using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AppDocument.State;
using BackendLogin.IntegrationTests;
using DALDocument.EF.Entities;
using DocumentFormat.OpenXml.Drawing.Charts;
using LayerTransmitter.Wrapper;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;

namespace BackendLogin.Tests
{
    [TestFixture]
    public class SpecialityControllerTests : BaseMemoryTest
    {

        //[Test, Order(1)]
        //public async Task CheckStatus()
        //{

        //    // Act

        //    HttpResponseMessage response = await httpClient.GetAsync("api/Speciality/getall");

        //    // Assert

        //    Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //    ;
        //}

        //[Test, Order(2)]
        //public async Task Add()
        //{
        //    // Arrange

        //    List<StringContent> specialities = new List<StringContent>
        //    {
        //        new StringContent(JsonConvert.SerializeObject(
        //            new {id = 0,
        //                name = "�������������������",
        //                description = "�������������������",
        //                code = "Otorhinolaryngology"}), Encoding.UTF8, "application/json"),
        //        new StringContent(JsonConvert.SerializeObject(
        //            new {id = 0,
        //                name = "����������-�����������",
        //                description = "����������-�����������",
        //                code = "Gynecology"}), Encoding.UTF8, "application/json"),
        //        new StringContent(JsonConvert.SerializeObject(
        //            new {id = 0,
        //                name = "���������",
        //                description = "���������",
        //                code = "Pediatrics"}), Encoding.UTF8, "application/json"),
        //        new StringContent(JsonConvert.SerializeObject(
        //            new {id = 0,
        //                name = "����� �������",
        //                description = "����� �������",
        //                code = "Therapy"}), Encoding.UTF8, "application/json"),
        //        new StringContent(JsonConvert.SerializeObject(
        //            new {id = 0,
        //                name = "�������������",
        //                description = "�������������",
        //                code = "Traumatology"}), Encoding.UTF8, "application/json"),
        //        new StringContent(JsonConvert.SerializeObject(
        //            new {id = 0,
        //                name = "���������",
        //                description = "���������",
        //                code = "Neuralogy"}), Encoding.UTF8, "application/json")
        //    };

        //    // Act
        //    HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/Speciality/GetAll");
        //    foreach (var item in specialities)
        //    {
        //        HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/Speciality/AddOrUpdate", item);
        //    }
        //    HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/Speciality/GetAll");

        //    // Assert
        //    var contentBefore = await responseBefore.Content.ReadAsStringAsync();
        //    var before = JsonConvert.DeserializeObject<List<SpecialityDto>>(contentBefore);
        //    var contentAfter = await responseAfter.Content.ReadAsStringAsync();
        //    var after = JsonConvert.DeserializeObject<List<SpecialityDto>>(contentAfter);
        //    int recordCount = after.Count - before.Count;
        //    Assert.AreEqual(recordCount, specialities.Count);
        //}

        //[Test, Order(3)]
        //public async Task UpdateTestTypeMessage_SendRecord_ShouldReturnOk_CheckId()
        //{
        //    // Arrange

        //    List<StringContent> SpecialityList = new List<StringContent>
        //        {
        //            new StringContent(JsonConvert.SerializeObject(
        //                new {id = 0,
        //                    name = "TestSpeciality1",
        //                    description = "TestSpecialityRecord",
        //                    code = "Telepathy"}), Encoding.UTF8, "application/json"),
        //            new StringContent(JsonConvert.SerializeObject(
        //                new {id = 0,
        //                    name = "TestSpeciality2",
        //                    description = "TestSpecialityRecord",
        //                    code = "SignLanguage"}), Encoding.UTF8, "application/json")
        //        };

        //    foreach (var item in SpecialityList)
        //    {
        //        HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/Speciality/AddOrUpdate", item);
        //    }

        //    var idSpeciality = dbContext.Specialities.FirstOrDefault(x => x.code == "Telepathy");

        //    var data = JsonConvert.SerializeObject(new
        //    {
        //        id = idSpeciality.id,
        //        name = "TestSpeciality3",
        //        description = "TestSpecialityRecord",
        //        code = "Telepathy"
        //    });
        //    var newData = new StringContent(data, Encoding.UTF8, "application/json");
        //    // Act

        //    HttpResponseMessage responseUpdate = await httpClient.PostAsync("/api/Speciality/AddOrUpdate", newData);

        //    // Assert
        //    Assert.AreEqual(HttpStatusCode.OK, responseUpdate.StatusCode);

        //    var checkId = dbContext.Specialities.FirstOrDefault(x => x.code == "Telepathy" && x.name == "TestSpeciality3");

        //    Assert.AreEqual(idSpeciality.id, checkId.id);
        //}

        //[Test, Order(4)]
        //public async Task CreateTestSpeciality_GetCreated_DeleteCreated_ShouldReturnOk()
        //{

        //    var data = JsonConvert.SerializeObject(new
        //    {
        //        id = 0,
        //        name = "Long_Unique_Name_For_Test_Record",
        //        description = "TestSpecialityRecordByAleksei",
        //        code = "Code"
        //    });
        //    var newData = new StringContent(data, Encoding.UTF8, "application/json");
        //    // Act

        //    HttpResponseMessage responseUpdate = await httpClient.PostAsync("/api/Speciality/AddOrUpdate", newData);

        //    Assert.AreEqual(HttpStatusCode.OK, responseUpdate.StatusCode);

        //    var idSpeciality = dbContext.Specialitys.FirstOrDefault(x => x.name == "Long_Unique_Name_For_Test_Record" && x.description == "TestSpecialityRecordByAleksei");

        //    var delete = JsonConvert.SerializeObject(new
        //    {
        //        id = idSpeciality.id,
        //    });
        //    var deleteData = new StringContent(delete, Encoding.UTF8, "application/json");

        //    HttpResponseMessage responseDelete = await httpClient.PostAsync("/api/Speciality/Delete", deleteData);

        //    Assert.AreEqual(HttpStatusCode.OK, responseUpdate.StatusCode);
        //}
    }
}