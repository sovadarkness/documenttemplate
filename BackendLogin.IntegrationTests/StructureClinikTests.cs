using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AppDocument.StructureClinik;
using BackendLogin.IntegrationTests;
using DALDocument.EF.Context;
using DALDocument.EF.Entities;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using PLDocument;

namespace StructureClinik.IntegrationTest
{
    [TestFixture]
    public class StructureClinikTests : BaseMemoryTest
    {
        private int StryctureTypeId_Clinic = 0;

        private int StructureId_Clinic = 0;


        private int ClinicLegalStatusId = 0;

        [Test]
        public async Task Scenario()
        {
            //�����������
            var user = await loginWithUser("admin", "Pa$$word123");

            ////���������� ���� �������������
            //var structureType�linic = new StructureTypeDto()
            //{
            //    name = "�������",
            //    description = "������� - ",
            //    code = "Clinic"
            //};
            //StryctureTypeId_Clinic = await AddStructureType(user, structureType�linic);
            //Assert.IsTrue(StryctureTypeId_Clinic > 0);




            //���������� ������������ �������
            //�������� �������
            //�������� �������

            //var structure_clinic = new StringContent(JsonConvert.SerializeObject(new
            //{
            //    id = 0,
            //    name = "���� ��������-����������",
            //    shortName = "��������-����������",
            //    description = "��������-���������� �������",
            //    licenseNumber = "564654669862363",
            //    siteUrl = "404.com",
            //    requisites = "[���������]",
            //    idType = ClinicTypeId,
            //    idLegalState = ClinicLegalStatusId
            //}), Encoding.UTF8, "application/json");

            //ClinicTypeId = await AddStructureType(structureType_clinic);

            //var clinic = new StructureDto()
            //{
            //    id = 0,
            //    name = "�������������������",
            //};

            //await AddClinic(clinic);

        }

        /*
        public async Task<int> AddStructureType(HttpClient user, StructureTypeDto structureType)
        {
            //Arrange
            var item = new StringContent(JsonConvert.SerializeObject(structureType));

            // Act
            var response = await user.PostAsync("/api/StructureType/AddOrUpdate", item);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var contentResponse = await response.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<StructureTypeDto>(contentResponse);
            return responseObj.id;
        }

        

        //���������� ���� �������������
        [Test, Order(1)]
        public async Task CheckCountStructureType_AddRecord_ShouldReturnOk()
        {
            //Arrange
            List<StringContent> structureTypeList = new List<StringContent>
            {
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "�������",
                        description = "������� - ",
                        code = "Clinic"}), Encoding.UTF8, "application/json"),
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "������",
                        description = "������ - ",
                        code = "Branch"}), Encoding.UTF8, "application/json"),
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "���������",
                        description = "��������� - ",
                        code = "Department"}), Encoding.UTF8, "application/json"),
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "�������",
                        description = "������� - ",
                        code = "Cabinet"}), Encoding.UTF8, "application/json")
            };

            // Act
            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/StructureType/GetAll");
            foreach (var item in structureTypeList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/StructureType/AddOrUpdate", item);
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/StructureType/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var before = JsonConvert.DeserializeObject<List<StructureTypeDto>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var after = JsonConvert.DeserializeObject<List<StructureTypeDto>>(contentAfter);
            int recordCount = after.Count - before.Count;
            Assert.AreEqual(recordCount, structureTypeList.Count);
        }

        //���������� ������������ �������
        [Test, Order(2)]
        public async Task CheckCountLegalStatus_AddRecord_ShouldReturnOk()
        {
            //Arrange
            List<StringContent> legalStatusList = new List<StringContent>
            {
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "��",
                        description = "�������������� ���������������",
                        code = "Individual"}), Encoding.UTF8, "application/json"),
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "����",
                        description = "�������� � ������������ ����������������",
                        code = "LLC"}), Encoding.UTF8, "application/json")
            };

            // Act

            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/LegalStatus/GetAll");
            foreach (var item in legalStatusList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/LegalStatus/AddOrUpdate", item);
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/LegalStatus/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var before = JsonConvert.DeserializeObject<List<LegalStatusDto>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var after = JsonConvert.DeserializeObject<List<LegalStatusDto>>(contentAfter);
            int recordCount = after.Count - before.Count;
            Assert.AreEqual(recordCount, legalStatusList.Count);
        }

        //���������� ���� ��������
        [Test, Order(3)]
        public async Task CheckCountContactType_AddRecord_ShouldReturnOk()
        {
            //Arrange
            List<StringContent> �ontactTypeList = new List<StringContent>
            {
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "�������",
                        description = "",
                        code = "Pager"}), Encoding.UTF8, "application/json"),
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "�������",
                        description = "",
                        code = "Telephone"}), Encoding.UTF8, "application/json"),
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "�����",
                        description = "",
                        code = "Mail"}), Encoding.UTF8, "application/json"),
            };

            // Act
            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/ContactType/GetAll");
            foreach (var item in �ontactTypeList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/ContactType/AddOrUpdate", item);
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/ContactType/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var before = JsonConvert.DeserializeObject<List<ContactTypeDto>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var after = JsonConvert.DeserializeObject<List<ContactTypeDto>>(contentAfter);
            int recordCount = after.Count - before.Count;
            Assert.AreEqual(recordCount, �ontactTypeList.Count);
        }

        //�������� �������
        [Test, Order(4)]
        public async Task CheckAddClinic_AddRecord_ShouldReturnOk()
        {
            //Arrange
            HttpResponseMessage responseStructureType = await httpClient.GetAsync("/api/StructureType/GetAll");
            var contentStructureType = await responseStructureType.Content.ReadAsStringAsync();
            var listStructureType = JsonConvert.DeserializeObject<List<StructureTypeDto>>(contentStructureType);
            int idStructureType = listStructureType.FirstOrDefault(x => x.name == "�������").id;

            HttpResponseMessage responseLegalStatusList = await httpClient.GetAsync("/api/LegalStatus/GetAll");
            var contentLegalStatusListe = await responseLegalStatusList.Content.ReadAsStringAsync();
            var listLegalStatusList = JsonConvert.DeserializeObject<List<LegalStatusDto>>(contentLegalStatusListe);
            int idLegalStatusList = listLegalStatusList.FirstOrDefault(x => x.name == "����").id;

            List<StringContent> �ontactTypeList = new List<StringContent>
            {
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        name = "���� ��������-����������",
                        shortName = "��������-����������",
                        description = "��������-���������� �������",
                        licenseNumber = "564654669862363",
                        siteUrl = "404.com",
                        requisites = "[���������]",
                        idType = idStructureType,
                        idLegalState = idLegalStatusList
                        }), Encoding.UTF8, "application/json"),
            };

            // Act
            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/Structure/GetAll");
            foreach (var item in �ontactTypeList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/Structure/AddOrUpdate", item);
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/Structure/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var before = JsonConvert.DeserializeObject<List<StructureDto>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var after = JsonConvert.DeserializeObject<List<StructureDto>>(contentAfter);
            int recordCount = after.Count - before.Count;
            Assert.AreEqual(recordCount, �ontactTypeList.Count);
        }

        //�������� ��������� �������

        [Test, Order(5)]
        public async Task CheckAddBranchClinic_AddRecord_ShouldReturnOk()
        {
            //Arrange
            HttpResponseMessage responseStructureType = await httpClient.GetAsync("/api/StructureType/GetAll");
            var contentStructureType = await responseStructureType.Content.ReadAsStringAsync();
            var listStructureType = JsonConvert.DeserializeObject<List<StructureTypeDto>>(contentStructureType);
            int idStructureType = listStructureType.FirstOrDefault(x => x.name == "������").id;

            HttpResponseMessage responseStructureList = await httpClient.GetAsync("/api/Structure/GetAll");
            var contentStructure = await responseStructureList.Content.ReadAsStringAsync();
            var listStructureList = JsonConvert.DeserializeObject<List<StructureDto>>(contentStructure);
            int idStructureList = listStructureList.FirstOrDefault(x => x.name == "���� ��������-����������").id;

            HttpResponseMessage responseLegalStatusList = await httpClient.GetAsync("/api/LegalStatus/GetAll");
            var contentLegalStatusListe = await responseLegalStatusList.Content.ReadAsStringAsync();
            var listLegalStatusList = JsonConvert.DeserializeObject<List<LegalStatusDto>>(contentLegalStatusListe);
            int idLegalStatusList = listLegalStatusList.FirstOrDefault(x => x.name == "����").id;

            List<StringContent> �ontactTypeList = new List<StringContent>
            {
                new StringContent(JsonConvert.SerializeObject(
                    new {id = 0,
                        idParent = 0,
                        name = "���� ��������-����������",
                        shortName = "��������-����������",
                        description = "��������-���������� �������",
                        licenseNumber = "564654669862363",
                        siteUrl = "404.com",
                        requisites = "[���������]",
                        idType = idStructureType,
                        idLegalState = idLegalStatusList
                        }), Encoding.UTF8, "application/json"),
            };

            // Act
            HttpResponseMessage responseBefore = await httpClient.GetAsync("/api/Structure/GetAll");
            foreach (var item in �ontactTypeList)
            {
                HttpResponseMessage responseAdd = await httpClient.PostAsync("/api/Structure/AddOrUpdate", item);
            }
            HttpResponseMessage responseAfter = await httpClient.GetAsync("/api/Structure/GetAll");

            // Assert
            var contentBefore = await responseBefore.Content.ReadAsStringAsync();
            var before = JsonConvert.DeserializeObject<List<StructureDto>>(contentBefore);
            var contentAfter = await responseAfter.Content.ReadAsStringAsync();
            var after = JsonConvert.DeserializeObject<List<StructureDto>>(contentAfter);
            int recordCount = after.Count - before.Count;
            Assert.AreEqual(recordCount, �ontactTypeList.Count);
        }
        */
    }
}