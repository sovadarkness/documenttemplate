using NUnit.Framework;
using Moq;
using AppDocument.Access;
using System;
using System.Collections.Generic;
using System.Linq;
using Core.Exceptions.Access;

namespace AccessTests
{
    public class AccessServiceTest
    {
        private Mock<IEmployeeRoleService> mEmployeeRoleService;
        private Mock<IFunctionAccessService> mFunctionAccessService;

        [SetUp]
        public void Setup()
        {
            mEmployeeRoleService = new Mock<IEmployeeRoleService>();
            mFunctionAccessService = new Mock<IFunctionAccessService>();

            PrepareTestData();
        }

        private Dictionary<Guid, List<EmployeeRoleDto>> AllUsersWithRoles;
        private int GetRolesForUserCounter;

        private Dictionary<string, List<int>> AllFunctionWithRoles;
        private int AllFunctionWithRolesConter;

        [Test]
        public void CheckAccess_Ok()
        {
            //Arrange
            var testUserGuid = Guid.Parse("eefcf89d-41a8-4510-adb5-4d640b8182fd");
            var roleIds = AllUsersWithRoles[testUserGuid].Select(x => x.idRole).ToList();
            var testFunctionCode = "testFunction";
            var testUserName = "testUser";

            mEmployeeRoleService
                .Setup(repo => repo.GetRolesForUser(testUserGuid))
                .Returns(() => GetRolesForUser(testUserGuid));

            mFunctionAccessService
                .Setup(repo => repo.IsRolesHasAccess(testFunctionCode, roleIds))
                .Returns(() => GetIsRolesHasAccess(testFunctionCode, roleIds));

            var accessService = new AccessService(mEmployeeRoleService.Object, mFunctionAccessService.Object);

            //Act
            accessService.CheckAccess(testUserGuid.ToString(), testUserName, testFunctionCode);

            //Assert
            Assert.Pass();
        }

        [Test]
        public void CheckAccess_NoAccess()
        {
            //Arrange
            var testUserGuid = Guid.Parse("8218f546-dd04-4d1f-8907-9f6b1882f6bf");
            var testFunctionCode = "testFunction";
            var testUserName = "testUser";
            var roleIds = AllUsersWithRoles[testUserGuid].Select(x => x.idRole).ToList();

            mEmployeeRoleService
                .Setup(repo => repo.GetRolesForUser(testUserGuid))
                .Returns(() => GetRolesForUser(testUserGuid));

            mFunctionAccessService
                .Setup(repo => repo.IsRolesHasAccess(testFunctionCode, roleIds))
                .Returns(() => GetIsRolesHasAccess(testFunctionCode, roleIds));

            var accessService = new AccessService(mEmployeeRoleService.Object, mFunctionAccessService.Object);

            //preAct
            TestDelegate del = () => accessService.CheckAccess(testUserGuid.ToString(), testUserName, testFunctionCode);

            //Assert
            Assert.Throws<AccessException>(del);

        }

        [Test]
        public void CheckAccess_Call()
        {
            //Arrange
            var testUserGuid = Guid.Parse("eefcf89d-41a8-4510-adb5-4d640b8182fd");
            var testFunctionCode = "testFunction";
            var testUserName = "testUser";
            var roleIds = AllUsersWithRoles[testUserGuid].Select(x => x.idRole).ToList();

            GetRolesForUserCounter = 0;
            AllFunctionWithRolesConter = 0;

            mEmployeeRoleService
                .Setup(repo => repo.GetRolesForUser(testUserGuid))
                .Returns(() => GetRolesForUser(testUserGuid));

            mFunctionAccessService
                .Setup(repo => repo.IsRolesHasAccess("testFunction", roleIds))
                .Returns(() => GetIsRolesHasAccess("testFunction", roleIds));

            var accessService = new AccessService(mEmployeeRoleService.Object, mFunctionAccessService.Object);

            //Act
            accessService.CheckAccess(testUserGuid.ToString(), testUserName, testFunctionCode);

            //Assert
            Assert.AreEqual(1, GetRolesForUserCounter);
            Assert.AreEqual(1, AllFunctionWithRolesConter);
        }


        private List<EmployeeRoleDto> GetRolesForUser(Guid userId)
        {
            GetRolesForUserCounter++;
            return AllUsersWithRoles[userId];
        }

        private bool GetIsRolesHasAccess(string functionCode, List<int> roleIds)
        {
            AllFunctionWithRolesConter++;
            var functionRoles = AllFunctionWithRoles[functionCode];

            if (functionRoles.Count == 0)
                return false;

            if (functionRoles.Count != roleIds.Count)
                return false;

            for (var i = 0; i < functionRoles.Count; i++)
            {
                if (functionRoles[i] != roleIds[i])
                    return false;
            }
            return true;
        }

        private void PrepareTestData()
        {
            AllUsersWithRoles = new Dictionary<Guid, List<EmployeeRoleDto>>();
            AllUsersWithRoles.Add(Guid.Parse("eefcf89d-41a8-4510-adb5-4d640b8182fd"), new List<EmployeeRoleDto>()
            {
                new EmployeeRoleDto()
                {
                    idEmployeeHistory = 1,
                    idRole = 1
                },
                new EmployeeRoleDto()
                {
                    idEmployeeHistory = 2,
                    idRole = 2
                },
            });

            AllUsersWithRoles.Add(Guid.Parse("8218f546-dd04-4d1f-8907-9f6b1882f6bf"), new List<EmployeeRoleDto>());

            AllFunctionWithRoles = new Dictionary<string, List<int>>();
            AllFunctionWithRoles.Add("testFunction", new List<int> { 1, 2 });
        }
    }
}