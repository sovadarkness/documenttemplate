﻿
//using Communication;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.TestHost;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.DependencyInjection.Extensions;
//using Microsoft.Extensions.Hosting;
//using Moq;
//using Newtonsoft.Json;
//using NUnit.Framework;
//using PLDocument;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Text;
//using System.Threading.Tasks;

//namespace Billing.IntegrationTests.FeatureTests.InsuranceProvider
//{
//    class s_QueryType_Tests : TestBase
//    {
//        public override bool NeedRestoreDataFromScripts => true;

//        [Test]
//        public async Task AddEntity_ToDb_Throw_RestApi()
//        {
//            //var mock = new Mock<Is_QueryTypeRepository>();
//            using var host = new HostBuilder()
//               .ConfigureWebHost(webBuilder =>
//               {
//                   webBuilder
//                       .UseTestServer()
//                       .UseStartup<Startup>()
//                       .UseSetting("ConnectionStrings:DefaultConnection", TestFixture.GetConnectionString())
//                       //ConfigureTestServices must be bellow UseStartup
//                       .ConfigureTestServices(services =>
//                       {
//                           //services.Replace(ServiceDescriptor.Scoped<Is_QueryTypeRepository>(_ => mock.Object));
//                       });
//               })
//               .Build();
//            await host.StartAsync();

//            //mock.Setup(_ => _.Add(It.IsAny<s_QueryTypeDto>())).Returns(1);
//            //mock.Setup(_ => _.GetSameItems(It.IsAny<s_QueryTypeDto>())).Returns(new List<s_QueryTypeDto>());

//            var _client = host.GetTestServer().CreateClient();

//            var createResponse = await _client.PostAsync("/api/s_QueryType/AddOrUpdate", new StringContent(JsonConvert.SerializeObject(new s_QueryTypeModel
//            {
//                name = "newAddedQueryType",
//            }), Encoding.UTF8, "application/json"));

//            createResponse.EnsureSuccessStatusCode();

//            var addedIdStr = await createResponse.Content.ReadAsStringAsync();
//            var returenedId = int.Parse(addedIdStr);

//            var db = TestFixture.GetDbContext();
//            var queryType = db.s_QueryTypes.OrderByDescending(x => x.id).FirstOrDefault();
//            var exptectedId = queryType.id;

//            Assert.AreEqual(exptectedId, returenedId);
//            Assert.AreEqual("newAddedQueryType", queryType.name);
//        }
//    }
//}
