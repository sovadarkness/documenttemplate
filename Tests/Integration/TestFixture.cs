namespace Billing.IntegrationTests
{
    using Billing.IntegrationTests.TestUtilities;
    using DALDocument.EF.Context;
    using MediatR;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Npgsql;
    using NUnit.Framework;
    using PLDocument;
    using Respawn;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    [SetUpFixture]
    public class TestFixture
    {
        private static IConfigurationRoot _configuration;
        private static IWebHostEnvironment _env;
        private static IServiceScopeFactory _scopeFactory;
        private static Checkpoint _checkpoint;

        private string _dockerContainerId;
        private string _dockerSqlPort;

        [OneTimeSetUp]
        public async Task RunBeforeAnyTests()
        {
            (_dockerContainerId, _dockerSqlPort) = await DockerSqlDatabaseUtilities.EnsureDockerStartedAndGetContainerIdAndPortAsync();
            var dockerConnectionString = DockerSqlDatabaseUtilities.GetSqlConnectionString(_dockerSqlPort);

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddInMemoryCollection(new Dictionary<string, string>
                    {
                        { "UseInMemoryDatabase", "false" },
                        { "ConnectionStrings:DefaultConnection", dockerConnectionString } 
                    })
                .AddEnvironmentVariables();

            _configuration = builder.Build();
            _env = Mock.Of<IWebHostEnvironment>();

            var startup = new Startup(_configuration, _env);

            var services = new ServiceCollection();

            services.AddLogging();

            startup.ConfigureServices(services);

            _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();





            _checkpoint = new Checkpoint
            {
                TablesToIgnore = new[] { "__EFMigrationsHistory" },
                SchemasToExclude = new[] { "information_schema", "pg_subscription", "pg_catalog", "pg_toast" },
                DbAdapter = DbAdapter.Postgres
            };

            EnsureDatabase();
        }

        private static void EnsureDatabase()
        {
            //using var scope = _scopeFactory.CreateScope();
            //var str = _configuration.GetConnectionString("DefaultConnection");
            //var context = new ApplicationContext(str);

            //context.Database.Migrate();

            //using var scope = _scopeFactory.CreateScope();

            //var context = scope.ServiceProvider.GetService<ApplicationContext>();

            //context.Database.Migrate();
        }


        public static string GetConnectionString()
        {
            return _configuration.GetConnectionString("DefaultConnection");
        }
        public static ApplicationContext GetDbContext()
        {
            throw new NotImplementedException();
            //var str = _configuration.GetConnectionString("DefaultConnection");
            ////var context = new ApplicationContext(str); //TODO WTF
            //var context = new ApplicationContext();
            //return context;
        }

        public static async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            using var scope = _scopeFactory.CreateScope();

            var mediator = scope.ServiceProvider.GetService<ISender>();

            return await mediator.Send(request);
        }

        public static async Task ResetState(bool needRestore)
        {
            var str = _configuration.GetConnectionString("DefaultConnection");
            using (var conn = new NpgsqlConnection(str))
            {
                await conn.OpenAsync();
                await _checkpoint.Reset(conn);
            }

            if (!needRestore) return;

            try //TODO refactor
            {
                var path = Directory.GetParent(ProjectSourcePath.Value);
                var sqlPath = Path.Combine(path.Parent.Parent.Parent.FullName, "Scripts");
                var updatePath = Path.Combine(sqlPath, "updates");
                NpgsqlConnection _connPg = new NpgsqlConnection(str);
                _connPg.Open();

                var updates = Directory.GetFiles(updatePath).OrderBy(x => x);
                foreach (var update in updates)
                {
                    FileInfo updateF = new FileInfo(update);
                    string uScript = updateF.OpenText().ReadToEnd();
                    var update_cmd = new NpgsqlCommand(uScript, _connPg);
                    update_cmd.ExecuteNonQuery();
                }

                _connPg.Close();
            }
            catch (Exception ex)
            {

            }
        }

        [OneTimeTearDown]
        public Task RunAfterAnyTests()
        {
            return Task.CompletedTask;
        }
    }
}