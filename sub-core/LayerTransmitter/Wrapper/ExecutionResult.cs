﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LayerTransmitter.Wrapper
{
    public enum ExecutionResult
    {
        Ok,
        Warning,
        Error,
        Exception
    }
}
