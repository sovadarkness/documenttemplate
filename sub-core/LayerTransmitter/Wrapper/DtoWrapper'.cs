﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LayerTransmitter.Wrapper
{
    public class DtoWrapper<Dto> : DtoWrapperBase
    {
        private DtoWrapper()
        {
        }
        public Dto Item { get; set; }

        public static DtoWrapper<Dto> Ok(Dto item)
        {
            var wrapper = new DtoWrapper<Dto>
            {
                ExecutionResult = ExecutionResult.Ok,
                Item = item
            };
            return wrapper;
        }

        public static DtoWrapper<Dto> Warning(Dto item, List<IExecutionWarning> warnings)
        {
            var wrapper = new DtoWrapper<Dto>
            {
                ExecutionResult = ExecutionResult.Warning,
                Item = item,
                Warnings = warnings
            };
            return wrapper;
        }

        public static DtoWrapper<Dto> Exception(IExecutionException exception)
        {
            var wrapper = new DtoWrapper<Dto>
            {
                ExecutionResult = ExecutionResult.Exception,
                ExecutionException = exception,
            };
            return wrapper;
        }
        
        public static DtoWrapper<Dto> Error(List<IExecutionError> errors)
        {
            var wrapper = new DtoWrapper<Dto>
            {
                ExecutionResult = ExecutionResult.Error,
                Errors = errors,
            };
            return wrapper;
        }

        public void AppendWarnings(List<IExecutionWarning> warnings)
        {
            Warnings.AddRange(warnings);
        }
    }
}
