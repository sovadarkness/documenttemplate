﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LayerTransmitter.Wrapper
{
    public abstract class DtoWrapperBase
    {
        protected DtoWrapperBase()
        {

        }
        public IExecutionException ExecutionException { get; set; }
        public ExecutionResult ExecutionResult { get; set; }
        public List<IExecutionError> Errors { get; set; }
        public List<IExecutionWarning> Warnings { get; set; }

    }
}
