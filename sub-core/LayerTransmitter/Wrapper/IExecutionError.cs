﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LayerTransmitter.Wrapper
{
    public interface IExecutionError
    {
        public int ErrorCode { get; }
        public string ErrorType { get; }
    }
}
