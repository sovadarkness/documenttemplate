﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LayerTransmitter.Wrapper
{
    public class UnknownException : IExecutionException
    {
        public Exception Exception { get; private set; }
        public UnknownException(Exception exception)
        {
            Exception = exception;
        }
    }
}
