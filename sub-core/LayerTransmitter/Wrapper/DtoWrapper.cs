﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LayerTransmitter.Wrapper
{
    public class DtoWrapper: DtoWrapperBase
    {
        public static DtoWrapper Ok()
        {
            var wrapper = new DtoWrapper
            {
                ExecutionResult = ExecutionResult.Ok
            };
            return wrapper;
        }

        public static DtoWrapper Warning(List<IExecutionWarning> warnings)
        {
            var wrapper = new DtoWrapper
            {
                ExecutionResult = ExecutionResult.Warning,
                Warnings = warnings
            };
            return wrapper;
        }
        public static DtoWrapper Error(List<IExecutionError> errors)
        {
            var wrapper = new DtoWrapper
            {
                ExecutionResult = ExecutionResult.Error,
                Errors = errors
            };
            return wrapper;
        }

        public static DtoWrapper Exception(IExecutionException exception)
        {
            var wrapper = new DtoWrapper
            {
                ExecutionResult = ExecutionResult.Exception,
                ExecutionException = exception
            };
            return wrapper;
        }

        public void AppendWarnings(List<IExecutionWarning> warnings)
        {
            Warnings.AddRange(warnings);
        }
    }
}
