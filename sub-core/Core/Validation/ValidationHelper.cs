﻿using Core.Validation.Errors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Validation
{
    public static class ValidationHelper
    {
        public static void IsEqualOrGreater(int minValue, int value, List<IExecutionError> errors)
        {
            if (value < minValue)
            {
                errors.Add(new GreaterOrEqualError(minValue, value));
            }
        }

        public static void IsEqualOrLess<T>(T maxValue, T value, List<IExecutionError> errors)
        {
            //if(value < minValue)
            //{
            //    errors.Add(new GreaterOrEqualError(minValue, value));
            //}
        }
        public static void IsGreater<T>(T minValue, T value, List<IExecutionError> errors)
        {
            //if(value < minValue)
            //{
            //    errors.Add(new GreaterOrEqualError(minValue, value));
            //}
        }

        public static void IsLess<T>(T maxValue, T value, List<IExecutionError> errors)
        {
            //if(value < minValue)
            //{
            //    errors.Add(new GreaterOrEqualError(minValue, value));
            //}
        }

        public static void IsNewIdentificator<TKeyType>(TKeyType value, List<IExecutionError> errors)
        {
            var typeDict = new Dictionary<Type, int>
            {
                {typeof(int),0},
                {typeof(long),1},
                {typeof(string),2},
                {typeof(Guid),3}
            };

            switch (typeDict[typeof(TKeyType)])
            {
                case 0:
                    if ((int)(object)value < 1)
                        errors.Add(new IsNewIdentificatorError<TKeyType>(value));
                    break;
                case 1:
                    if ((long)(object)value < 1)
                        errors.Add(new IsNewIdentificatorError<TKeyType>(value));
                    break;
                case 2:
                    if (string.IsNullOrWhiteSpace((string)(object)value))
                        errors.Add(new IsNewIdentificatorError<TKeyType>(value));
                    break;
                case 3:
                    if ((Guid)(object)value == Guid.Empty)
                        errors.Add(new IsNewIdentificatorError<TKeyType>(value));
                    break;
                default:
                    break;
            }
        }

        public static void MinStringLength(int minLength, string value, List<IExecutionError> errors)
        {
            if (string.IsNullOrEmpty(value) || value.Length < minLength)
            {
                errors.Add(new MinStringLengthError(minLength, value));
            }
        }
        public static void MaxStringLength(int maxLength, string value, List<IExecutionError> errors)
        {
            //if(string.IsNullOrEmpty(value) || value.Length < minLength)
            //{
            //    errors.Add(new MinStringLengthError(minLength, value));
            //}
        }
        public static void IsOnlyDigits(string value, List<IExecutionError> errors)
        {
            //if(string.IsNullOrEmpty(value) || value.Length < minLength)
            //{
            //    errors.Add(new MinStringLengthError(minLength, value));
            //}
        }
        public static void IsOnlyText(string value, List<IExecutionError> errors)
        {
            //if(string.IsNullOrEmpty(value) || value.Length < minLength)
            //{
            //    errors.Add(new MinStringLengthError(minLength, value));
            //}
        }
        public static void IsPhone(string value, List<IExecutionError> errors)
        {
            //if(string.IsNullOrEmpty(value) || value.Length < minLength)
            //{
            //    errors.Add(new MinStringLengthError(minLength, value));
            //}
        }

        public static void IsEmail(string value, List<IExecutionError> errors)
        {

        }

        public static void IsEqual<T>(T value1, T value2, List<IExecutionError> errors)
        {

        }

        public static void CheckEmptyTextField(string dtoName, List<IExecutionError> errors)
        {
        }

        public static void CheckEmptyLookup(int? value, List<IExecutionError> errors)
        {
        }
    }
}
