﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Validation.Errors
{
    public class IsNewIdentificatorError<T> : IExecutionError
    {
        public T Value { get; }
        public IsNewIdentificatorError(T value)
        {
            Value = value;
        }

        public int ErrorCode => 1;

        public string ErrorType => ErrorTypes.Validation;
    }
}
