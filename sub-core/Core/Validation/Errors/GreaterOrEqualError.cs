﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Validation.Errors
{
    public class GreaterOrEqualError : IExecutionError
    {
        public int MinValue { get; }
        public int Value { get; }
        public GreaterOrEqualError(int minValue, int value)
        {
            MinValue = minValue;
            Value = value;
        }

        public int ErrorCode => (int)ValidationErrors.DataLogicError;

        public string ErrorType => ErrorTypes.Validation;
    }
}
