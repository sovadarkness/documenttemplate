﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Validation.Errors
{
    public class MinStringLengthError : IExecutionError
    {
        public int MinValue { get; }
        public string Value { get; }
        public MinStringLengthError(int minValue, string value)
        {
            MinValue = minValue;
            Value = value;
        }

        public int ErrorCode => 1;

        public string ErrorType => ErrorTypes.Validation;
    }
}
