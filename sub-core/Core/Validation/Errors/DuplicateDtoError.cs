﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;

namespace Core.Validation.Errors
{
    internal class DuplicateDtoError<T>: IExecutionError
    {
        public T Value { get; private set; }
        public int ErrorCode => (int)ValidationErrors.DuplicateDto;
        public string CustomErrorCode { get; set; }
        public string ErrorType => ErrorTypes.Validation;

        public DuplicateDtoError(T value)
        {
            Value = value;
        }

        public DuplicateDtoError(T value, string code)
        {
            Value = value;
            CustomErrorCode = code;
        }
    }
}