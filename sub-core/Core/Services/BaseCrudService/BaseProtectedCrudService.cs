﻿using Core.Abstraction;
using Core.DtoWrapperErrors;
using Core.Exceptions;
using Core.Exceptions.Access;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Core.Validation;
using Core.Validation.Errors;

namespace Core.Services.BaseCrudService
{
    public abstract class BaseProtectedCrudService<DomainDto, KeyType> : IBaseCrudService<DomainDto, KeyType>
        where DomainDto : class, IDtoWithId<KeyType>
    {
        protected readonly
            IBaseCrudRepository<DomainDto, KeyType> _baseCrudRepository;
        protected readonly ISessionProvider _sessionProvider;
        protected readonly IAccessService _accessService;
        public BaseProtectedCrudService(
                IBaseCrudRepository<DomainDto, KeyType> baseCrudRepository,
                    ISessionProvider sessionProvider,
                    IAccessService accessService)
        {
            _baseCrudRepository = baseCrudRepository;
            _sessionProvider = sessionProvider;
            _accessService = accessService;
        }

        protected abstract string AddMedtodKey { get; }
        protected abstract string UpdateMedtodKey { get; }
        protected abstract string RemoveMedtodKey { get; }
        protected abstract string ReadMedtodKey { get; }

        #region Add
        //public virtual A Add<A, B>(B b)
        //    where B : class, IDtoWithId<A>
        //    where A : KeyType
        //{
        //    return (A)Add(b as DomainDto);
        //}

        public KeyType Add(DomainDto dto)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, AddMedtodKey);

            var errors = new List<IExecutionError>();
            ValidateAdd(dto, errors);

            if (errors != null && errors.Any())
            {
                throw new ValidationException(errors);
            }

            BeforeAddTrigger(dto);
            var result = CustomAdd(dto);
            AfterAddTrigger(dto, result);

            return result;
        }

        protected virtual void ValidateAdd(DomainDto dto, List<IExecutionError> errors)
        {
            ModelValidate(dto, errors);
            DataValidate(dto, errors);
        }

        protected virtual void ModelValidate(DomainDto dto, List<IExecutionError> errors)
        {
        }

        protected virtual void DataValidate(DomainDto dto, List<IExecutionError> errors)
        {
            var oldPersons = _baseCrudRepository.GetSameItems(dto);
            if (oldPersons.Any(x => !x.id.Equals(dto.id)))
            {
                var error = new DuplicateDtoError<DomainDto>(dto);
                errors.Add(error);
            }
        }


        protected virtual void BeforeAddTrigger(DomainDto dto)
        {
        }

        protected virtual KeyType CustomAdd(DomainDto dto)
        {
            return _baseCrudRepository.Add(dto);
        }
        protected virtual void AfterAddTrigger(DomainDto dto, KeyType result)
        {
        }
        #endregion Add

        #region Update
        //public A Update<A, B>(B updateDto)
        //    where B : class, IDtoWithId<A>
        //    where A: KeyType
        //{
        //    return (A)Update(updateDto as DomainDto);
        //}
        public KeyType Update(DomainDto dto)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, UpdateMedtodKey);

            var errors = new List<IExecutionError>();
            ValidateUpdate(dto, errors);

            if (errors != null && errors.Any())
            {
                throw new ValidationException(errors);
            }

            BeforeUpdateTrigger(dto);
            var result = CustomUpdate(dto);
            AfterUpdateTrigger(dto, result);

            return result;
        }

        protected virtual void ValidateUpdate(DomainDto dto, List<IExecutionError> errors)
        {
            ValidationHelper.IsNewIdentificator<KeyType>(dto.id, errors);
            ModelValidate(dto, errors);
            DataValidate(dto, errors);
        }

        protected virtual void BeforeUpdateTrigger(DomainDto dto)
        {
        }

        protected virtual KeyType CustomUpdate(DomainDto dto)
        {
            return _baseCrudRepository.Update(dto);
        }
        protected virtual void AfterUpdateTrigger(DomainDto dto, KeyType result)
        {
        }

        #endregion Update

        #region Get All
        //public List<A> GetAll<A>()
        //{
        //    return GetAll<DomainDto>() as List<A>;
        //}

        public virtual List<DomainDto> GetAll()
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);
            return _baseCrudRepository.GetAll();
        }

        #endregion Get All

        #region Get One By Key
        //public object GetOneById<A>(A idEntity) where A : struct
        //{
        //    return GetOneByKey((KeyType)((object)idEntity));
        //}
        public virtual DomainDto GetOneByKey(KeyType key)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, ReadMedtodKey);

            var result = _baseCrudRepository.GetOneByKey(key);

            if (result == null)
            {
                var notFoundError = new NotFoundError();
                throw new ValidationException(notFoundError);
            }

            return result;
        }

        #endregion Get One By Key

        #region Delete
        public void Remove(KeyType key)
        {
            _accessService.CheckAccess(_sessionProvider.UserId, _sessionProvider.UserName, RemoveMedtodKey);

            var errors = new List<IExecutionError>();
            ValidateDelete(key, errors);

            if (errors != null && errors.Any())
            {
                throw new ValidationException(errors);
            }

            BeforeDeleteTrigger(key);
            CustomDelete(key);
            AfterDeleteTrigger(key);
        }

        protected virtual void ValidateDelete(KeyType key, List<IExecutionError> errors) 
        {
            if (!_baseCrudRepository.CheckCanBeRemoved(key))
            {
                errors.Add(new RelationError(typeof(DomainDto).Name, key));
            }
        }

        protected virtual void BeforeDeleteTrigger(KeyType dto)
        {
        }

        protected virtual void CustomDelete(KeyType key)
        {
            _baseCrudRepository.Remove(key);
        }
        protected virtual void AfterDeleteTrigger(KeyType key)
        {
        }
        #endregion Delete

    }
}