﻿using Core.Abstraction;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace Core.Services.BaseCrudService
{
    public abstract class BasePublicCrudService<DomainDto, KeyType> : IBaseCrudService<DomainDto, KeyType>
        where DomainDto : IDtoWithId<KeyType>
    {
        private readonly IBaseCrudRepository<DomainDto, KeyType> _baseCrudRepository;
        public BasePublicCrudService(IBaseCrudRepository<DomainDto, KeyType> baseCrudRepository)
        {
            _baseCrudRepository = baseCrudRepository;
        }
        public KeyType Add(DomainDto addDto)
        {
            return _baseCrudRepository.Add(addDto);
        }

        //public DtoWrapper<A> Add<A, B>(B addDto) where B : class, IDtoWithId<A>
        //{
        //    return Add(addDto as AddDto) as DtoWrapper<A>;
        //}

        public List<DomainDto> GetAll()
        {
            return _baseCrudRepository.GetAll();
        }

        public DtoWrapper<List<A>> GetAll<A>()
        {
            return GetAll<GetDto>() as DtoWrapper<List<A>>;
        }
        public object GetOneById<A>(A idEntity) where A : struct
        {
            return GetOneByKey((KeyType)((object)idEntity));
        }
        public DtoWrapper<GetDto> GetOneByKey(KeyType key)
        {
            return _baseCrudRepository.GetOneByKey(key);
        }

        public DtoWrapper Remove(KeyType key)
        {
            return _baseCrudRepository.Remove(key);
        }

        public DtoWrapper<KeyType> Update(UpdateDto updateDto)
        {
            return _baseCrudRepository.Update(updateDto);
        }
        public DtoWrapper<A> Update<A, B>(B updateDto)
        {
            return Update(updateDto as UpdateDto) as DtoWrapper<A>;
        }
    }
}
