﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime Now => DateTime.Now;

        public DateTime Today => DateTime.Today;

        public int GetAgeForNow(DateTime dob)
        {
            var now = Now;

            var yearDiff = now.Year - dob.Year;
            if(now.Month < dob.Month)
                yearDiff--;

            if(now.Month == dob.Month && now.Day < dob.Day)
                yearDiff--;

            return yearDiff;
        }
    }
}
