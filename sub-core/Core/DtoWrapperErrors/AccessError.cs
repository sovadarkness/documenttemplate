﻿using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DtoWrapperErrors
{
    public class AccessError : IExecutionError
    {
        public AccessError(string userName, string medtodKey)
        {
            UserName = userName;
            MedtodKey = medtodKey;
        }

        public int ErrorCode => (int)AccessErrors.NoAccess;

        public string ErrorType => ErrorTypes.Access;

        public string UserName { get; }
        public string MedtodKey { get; }
    }
}
