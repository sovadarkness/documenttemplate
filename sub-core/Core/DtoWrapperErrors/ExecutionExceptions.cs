﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DtoWrapperErrors
{
    public enum ExecutionExceptions
    {
        DatabaseError = 1,
        DeleteError = 2
    }
}
