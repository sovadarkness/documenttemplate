﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DtoWrapperErrors
{
    public static class ErrorTypes
    {
        public static string Access => "ACC";
        public static string Data => "DAT";
        public static string Validation => "VLD";
        public static string Execution => "EXE";
    }
}
