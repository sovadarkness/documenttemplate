﻿using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DtoWrapperErrors
{
    public enum DataErrors
    {
        NotFoundByKey = 1,
        RelationError = 2
    }
}
