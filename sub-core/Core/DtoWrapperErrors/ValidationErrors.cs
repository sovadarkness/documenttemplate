﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DtoWrapperErrors
{
    public enum ValidationErrors
    {
        DuplicateUniqueField = 1,
        DuplicatePerson = 2,
        DuplicateDto = 3,
        DataLogicError = 4,
        //DateLogicError = 5,
    }
}
