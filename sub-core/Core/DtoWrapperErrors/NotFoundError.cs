﻿using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DtoWrapperErrors
{
    public class NotFoundError : IExecutionError
    {
        public int ErrorCode => (int)DataErrors.NotFoundByKey;
        public string ErrorType => ErrorTypes.Data;
    }

    public class RelationError : IExecutionError
    {
        public string Type { get; }
        public object Key { get; }
        public int ErrorCode => (int)DataErrors.RelationError;
        public string ErrorType => ErrorTypes.Data;

        public RelationError(string type, object key)
        {
            Type = type;
            Key = key;
        }
    }
}
