﻿using Core.DtoWrapperErrors;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Exceptions.Access
{
    public class AccessException: Exception
    {
        public AccessException(AccessError accessError)
        {
            AccessError = accessError;
        }

        public string Type => "AccessException";
        public AccessError AccessError { get; }
    }
}
