﻿using Core.DtoWrapperErrors;
using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Exceptions
{
    public class ValidationException : Exception
    {
        public string Type => "ValidationException";

        public List<IExecutionError> Errors { get; }

        public ValidationException(List<IExecutionError> errors)
        {
            Errors = errors;
        }

        public ValidationException(IExecutionError error)
        {
            Errors = new List<IExecutionError> { error };
        }
    }
}
