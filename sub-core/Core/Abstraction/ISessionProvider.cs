﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Core.Abstraction
{
    public interface ISessionProvider
    {
        public string UserName { get; }
        public string UserId { get; }
        public string Token { get; }
        public int CurrentStructureId { get; }
        public void Initialize(IEnumerable<Claim> claims, string token);
    }
}
