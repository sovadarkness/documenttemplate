﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Providers
{
    public interface IDateTimeProvider
    {
        DateTime Now { get; }
        DateTime Today { get; }

        int GetAgeForNow(DateTime dob);
    }
}
