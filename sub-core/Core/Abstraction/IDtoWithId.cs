﻿namespace Core.Abstraction
{
    public interface IDtoWithId<KeyType>
    {
        KeyType id { get; set; }
    }
}
