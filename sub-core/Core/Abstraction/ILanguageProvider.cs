﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Abstraction
{
    public interface ILanguageProvider
    {
        string SelectedLanguage { get; }
        int LanguageId { get; }

        public void Initialize(string langCode);
    }
}
