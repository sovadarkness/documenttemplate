﻿using Core.Abstraction;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace Core.Services.BaseCrudService
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="DomainDto"></typeparam>
    /// <typeparam name="KeyType"></typeparam>
    public interface IBaseCrudService<DomainDto, KeyType> //: IBaseCrudService
        where DomainDto : IDtoWithId<KeyType>
    {
        KeyType Add(DomainDto addDto);
        List<DomainDto> GetAll();
        DomainDto GetOneByKey(KeyType key);
        void Remove(KeyType key);
        KeyType Update(DomainDto updateDto);
    }

    public interface IBaseCrudService
    {
        KeyType Add<KeyType, AddDto>(AddDto addDto)
            where AddDto : class, IDtoWithId<KeyType>;
        KeyType Update<KeyType, UpdateDto>(UpdateDto updateDto)
                    where UpdateDto : class, IDtoWithId<KeyType>;
        object GetOneById<KeyType>(KeyType idEntity) where KeyType : struct;
    }
}
