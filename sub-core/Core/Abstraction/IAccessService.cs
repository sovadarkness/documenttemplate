﻿using LayerTransmitter.Wrapper;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Core.Abstraction
{
    public interface IAccessService
    {
        public void CheckAccess(string userId, string userName, string functionCode);
    }
}
