﻿using Core.Abstraction;
using LayerTransmitter.Wrapper;
using System.Collections.Generic;

namespace Core.Services.BaseCrudService
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="DomainDto"></typeparam>
    /// <typeparam name="KeyType"></typeparam>
    public interface IBaseCrudRepository<DomainDto, KeyType>
        where DomainDto : IDtoWithId<KeyType>
    {
        KeyType Add(DomainDto addPost);
        List<DomainDto> GetAll();
        DomainDto GetOneByKey(KeyType id);
        bool CheckCanBeRemoved(KeyType key);
        void Remove(KeyType key);
        KeyType Update(DomainDto updatePost);
        List<DomainDto> GetSameItems(DomainDto dto);
    }
}
